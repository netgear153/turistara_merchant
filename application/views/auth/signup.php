<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Turistara</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="<?php echo base_url();?>assets/img/icon.png" rel="icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="<?php echo base_url();?>assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="<?php echo base_url();?>assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/lib/animate/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

</head>

<body>

 <!--==========================
  Header
  ============================-->
  <header id="header">

    <div id="topbar">
      <div class="container">
        <div class="social-links">
          <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
          <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
          <a href="#" class="google"><i class="fa fa-google"></i></a>
          <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
        </div>
      </div>
    </div>

    <div class="container">

      <div class="logo float-left">
        <a href="#intro" class="scrollto"><img src="<?php echo base_url();?>assets/img/turistara/logo-dark.png" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li><a href="<?php echo base_url(); ?>Home">Home</a></li>
          <li><a href="<?php echo base_url(); ?>login">Login</a></li>
        </ul>
      </nav><!-- .main-nav -->
  
    </div>
  </header><!-- #header -->

    <section id="intro" class="clearfix">



  <!--==========================
      Pricing Section
    ============================-->
    <section id="login" class="wow fadeInUp">
      <div class="login-info">

        <!-- log in form --> 
        <form method="post" action="<?php echo base_url(); ?>Signup/validation">


         <div class="card">
          <div class="card-header">
                <h3><span class="currency"></span>Sign up</h3>
          </div>
          <div class="login-block">

          <select id="signup" name="signup_type">
            <option value="user">User</option>
            <option value="admin">Admin</option>
            <option value="business_owner">Business Owner</option>
          </select>

          <br><br>

          <div class="form-group">
            <input class="login-form" name="username" type="text" placeholder="Username">
            <span class="text-danger"><?php echo form_error('username'); ?></span>
          </div>
  
          <div class="form-group">
            <input class="login-form" name="email" type="email" placeholder="E-mail">
            <span class="text-danger"><?php echo form_error('email'); ?></span>
          </div>

          <div class="form-group">
            <input class="login-form" name="password" type="password"  placeholder="Password">
            <span class="text-danger"><?php echo form_error('password'); ?></span>
          </div>

          <div class="form-group">
            <input class="login-form" name="confirm_password" type="password"  placeholder="Confirm Password">
            <span class="text-danger"><?php echo form_error('confirm_password'); ?></span>
          </div>

          <div class="form-group">
            <input type="checkbox" id="accept-terms">
            <label for="accept-terms">I agree to the <a href="#0">Terms</a></label>
          </div>

          <div class="text-center">
          <input type="submit" name="register" value="Register" class="btn btn-info" />

          </div>

        <br>

        </div>

        </div>
      
        </div>
      </form>



        <!-- <a href="#0" class="cd-close-form">Close</a> -->
      </div> <!-- cd-login -->
      </section>
  




</body>

  <!-- JavaScript Libraries -->
  <script src="<?php echo base_url();?>assets/lib/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/jquery/jquery-migrate.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/easing/easing.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/mobile-nav/mobile-nav.js"></script>
  <script src="<?php echo base_url();?>assets/lib/wow/wow.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/waypoints/waypoints.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/counterup/counterup.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/isotope/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/lightbox/js/lightbox.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="<?php echo base_url();?>assets/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="<?php echo base_url();?>assets/js/main.js"></script>

