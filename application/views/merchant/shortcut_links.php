<li class="breadcrumb-item">
   <a href="<?php echo base_url();?>business/dashboard"> <i class="feather icon-home"></i> 
   </a>
</li>
<?php
if ($hasmenu == 'home') {
?>
<li class="breadcrumb-item"><a href="<?php echo base_url();?>business/profile">Business Profile</a></li>
<?php
}
?>
<?php
if ($hasmenu == 'order' AND $submenu == 'transaction') {
?>
<li class="breadcrumb-item">Orders</li>
<li class="breadcrumb-item"><a href="<?php echo base_url();?>business/order">Transactions</a></li>
<?php
}
?>
<?php
if ($hasmenu == 'order' AND $submenu == 'pending') {
?>
<li class="breadcrumb-item">Orders</li>
<li class="breadcrumb-item"><a href="<?php echo base_url();?>business/pending">Pendings</a></li>
<?php
}
?>
<?php
if ($hasmenu == 'order' AND $submenu == 'confirmed') {
?>
<li class="breadcrumb-item">Orders</li>
<li class="breadcrumb-item"><a href="<?php echo base_url();?>business/confirmed">Confirmed</a></li>
<?php
}
?>
<?php
if ($hasmenu == 'order' AND $submenu == 'preparing') {
?>
<li class="breadcrumb-item">Orders</li>
<li class="breadcrumb-item"><a href="<?php echo base_url();?>business/preparing">Preparing</a></li>
<?php
}
?>
<?php
if ($hasmenu == 'order' AND $submenu == 'deliver') {
?>
<li class="breadcrumb-item">Orders</li>
<li class="breadcrumb-item"><a href="<?php echo base_url();?>business/deliver">Deliver</a></li>
<?php
}
?>
<?php
if ($hasmenu == 'order' AND $submenu == 'completed') {
?>
<li class="breadcrumb-item">Orders</li>
<li class="breadcrumb-item"><a href="<?php echo base_url();?>business/completed">Completed</a></li>
<?php
}
?>
<?php
if ($hasmenu == 'order' AND $submenu == 'cancelled') {
?>
<li class="breadcrumb-item">Orders</li>
<li class="breadcrumb-item"><a href="<?php echo base_url();?>business/cancelled">Cancelled</a></li>
<?php
}
?>
<?php
if ($hasmenu == 'products and services' AND $submenu == 'rooms') {
?>
<li class="breadcrumb-item">Products and Services</li>
<li class="breadcrumb-item"><a href="<?php echo base_url();?>business/rooms">Rooms</a></li>
<?php
} ?>
<?php
if ($hasmenu == 'products and services' AND $submenu == 'foods') {
?>
<li class="breadcrumb-item">Products and Services</li>
<li class="breadcrumb-item"><a href="<?php echo base_url();?>business/foods">Foods</a></li>
<?php
} ?>
