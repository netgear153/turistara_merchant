<!DOCTYPE html>
<html lang="en">

<head>
    <title>Transaction - Order </title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">

    <link rel="icon" href="<?php echo base_url();?>assets/img/turistara/icon.png" type="image/x-icon">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/themify-icons/themify-icons.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/icofont/css/icofont.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/feather/css/feather.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/extensions/responsive/css/responsive.dataTables.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/ekko-lightbox.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/sweetalert.css">
    <link href="<?php echo site_url(); ?>assets/css/toggle.css" rel="stylesheet">


    <style type="text/css">
      .toolbar {
    float:left;
}

    </style>

</head>

<body>

<?php include 'navbar.php'; ?> 
<?php include 'sidebar.php'; ?> 
 
<div class="pcoded-content">
   <div class="pcoded-inner-content">
      <div class="main-body">
         <div class="page-wrapper">
            <div class="page-header">
               <div class="row align-items-end">
                  <div class="col-lg-8">
                     <div class="page-header-title">
                        <div class="d-inline">
                           <?php foreach($result_order_business as $row) { ?>
                            <h4><?php echo $row->business_name;  ?></h4>
                            <span><?php echo $row->business_type;  ?> - <?php echo $row->category;  ?></span>
                            <?php } ?>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-4">
                     <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                           <?php include 'shortcut_links.php'; ?>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <div class="page-body">
               <div class="card">
                  <div class="card-block">
                     <?php include 'transaction_datatable.php'; ?> 
                  </div>
                  
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="dataModal" class="modal fade"  role="dialog" >
   <div class="modal-dialog" style="  
      position: relative;
      display: table; 
      overflow-y: auto;    
      overflow-x: auto;
      width: auto;
      min-width: 300px;   ">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Order Details</h4>
         </div>
         <div class="modal-body" id="employee_detail">
            <?php include 'modal_view_order.php'; ?> 
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-success" id="confirm_order">Confirm Order</button>
            <button type="button" class="btn btn-danger" id="cancel_transaction" onclick="cancel_transaction();" >Cancel Transaction</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>



 


    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/bootstrap/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/modernizr/js/css-scrollbars.js"></script>

    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/js/jszip.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/js/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/extensions/responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next/js/i18next.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>

    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/extensions/responsive/js/responsive-custom.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/js/pcoded.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/js/vartical-layout.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/assets/js/script.js"></script>

    <script src="<?php  echo site_url();?>assets/js/ekko-lightbox.min.js"></script> 

    <script src="<?php  echo site_url();?>assets/js/sweetalert.min.js"></script>

    <script src="<?php  echo site_url();?>assets/js/check_image.js"></script>

    <script src="<?php  echo site_url();?>assets/js/adminty_design_code.js"></script>


<script>

  var site_url = '<?php echo site_url(); ?>';

  //------- GLOBAL VARIABLE--------------//

  //----VARIABLE FOR DATATABLE-----//

  var transaction_type = "order";
  var business_type = "Foods & Restaurants";
  var status = "";

  //----VARIABLE FOR TRANSACTION-----//



  var global_transaction_id = "" ;
  var global_payment_id = "";
  var global_user_id = "";
  var global_amount = parseFloat("") || 0;
  var global_delivery_fee = parseFloat("") || 0;
  var global_amount_with_fee = parseFloat("") || 0;

  //this is for storing old order amount
  var global_quantity_amount = {};

  //this is for updating order
  var global_order_id = [];
  var global_cancelled_order = {};
  var global_updated_quantity = {};
  var global_updated_amount = {};
  
  //this is for validation for delivery fee
  var global_delivery_setting = "" ;

//-------- WHEN THE USER INPUTS DELIVERY FEE --------
$("#delivery_fee").on('touchend keyup', function() {

  var delivery_fee = parseFloat($(this).val()) || 0;

  //------ UPADATES GLOBAL VARIABLE FOR TRANSACTION----//
  global_delivery_fee = delivery_fee;
  global_amount_with_fee = parseFloat(global_amount) + global_delivery_fee;

  $('#amount_with_fee').text(global_amount_with_fee);

});


//-----------WHEN THE ORDER IS CONFIRMED--------------//
$('#confirm_order').click(function() {

    var updated_quantity = Object.values(global_updated_quantity);
    var updated_order_amount = Object.values(global_updated_amount);
    var cancelled_order = Object.values(global_cancelled_order);

    var message = $('#message').val() || '';
    var order_status = 'Cancelled';
    var transaction_status = 'pending';

//----------CHECKS IF THE DELIVERY FEE IS EMPTY---------------//
if( global_delivery_setting != 'free'){
    if ( global_delivery_fee == 0 ) {

        $('#error_delivery_fee').text('please input fee');
        return false;
    }
  };

//----------CHECKS IF THE MESSAGE IS EMPTY---------------//
    if (message == '') {
        $('#error_message').text('Please leave a message.');
        return false;
    }


//----------CHECKS IF THE AMOUNT IS NOT EMPTY ELSE RUN cancel_transaction() function---------------//
    if (amount != 0) {
    
        $('#dataModal').modal('hide');


        $.ajax({
            url: site_url + "Business/confirm_order",
            type: "POST",
            data: {
                'order_id': global_order_id,
                'quantity': updated_quantity,
                'order_amount': updated_order_amount,
                'user_id': global_user_id,
                'transaction_id': global_transaction_id,
                'amount': global_amount,
                'delivery_fee': global_delivery_fee,
                'amount_with_fee': global_amount_with_fee,
                'payment_id': global_payment_id,
                'cancelled_order': cancelled_order,
                'transaction_status': transaction_status,
                'message': message
            },
            dataType: 'json',
            success: function(data) {

                Swal.fire({
                    title: "Success!",
                    text: "The order is sent to the user for confirmation.",
                    type: "success"
                }).then(function() {
                    window.location = site_url + 'Business/pending';
                });

            },

            error: function(request, status, error) {
                alert(request.responseText);

                Swal.fire({
                    title: "Error!",
                    text: "The order could not be confirm. Please try again.",
                    type: "error"
                }).then(function() {
                    window.location = site_url + 'Business/order';
                });

            }

        });

    } else {

        cancel_trasaction();

    }


});

//------- WHEN THE ORDER IS CANCELLED
function cancel_order(input) {

    var order_id = $(input).val();

    var price = parseFloat(global_updated_amount[order_id]);

    var el = $(input);
    if (el.text() == el.data("text-swap")) {
        el.text(el.data("text-original"));

        global_amount = parseFloat(global_amount) + price;
        global_amount_with_fee = global_amount + parseFloat(global_delivery_fee);

        global_cancelled_order[order_id] = '';

       
        $('input[data-value="'+order_id+'"]').prop("readonly",false);
        $('#amount').text(global_amount);
        $('#amount_with_fee').text(global_amount_with_fee);

    } else {

        el.data("text-original", el.text());
        el.text(el.data("text-swap"));

        global_cancelled_order[order_id] = 'cancelled';

        global_amount = parseFloat(global_amount) - price;
        global_amount_with_fee = global_amount + parseFloat(global_delivery_fee);

        $('input[data-value="'+order_id+'"]').prop("readonly",true);
        $('#amount').text(global_amount);
        $('#amount_with_fee').text(global_amount_with_fee);

    }

}

function order_quantity(input) {

  var element = $(input);

  //gets the the quantity val() and order_id
  var original_quantity = element.attr("max");
  var quantity = parseFloat(element.val());
  var order_id = element.attr("data-value");


  //gets the value of amount in the global object
  var object_amount = global_quantity_amount[order_id];
  var original_amount = parseFloat(object_amount);

  //This result will show the per food price...
  var per_food_amount = original_amount / original_quantity;

  //and then multiplied through quantity variable....
  var updated_amount = quantity * per_food_amount;

  //This variable are used to check if the quantity is increase or decreasing
  var old_quantity = element.attr("data-old-quantity");;
  var check_quantity = quantity > old_quantity;

  
  if(check_quantity){ //check if quantity is increasing....  
 

  global_amount = parseFloat(global_amount) + per_food_amount;
  global_amount_with_fee = global_amount + parseFloat(global_delivery_fee);

  
  } else{ //else quantity is decreasing....

  global_amount = parseFloat(global_amount) - per_food_amount;
  global_amount_with_fee =  global_amount + parseFloat(global_delivery_fee);

  }

  //updates global_updated_amount (for sending and updating data)
  global_updated_amount[order_id] = updated_amount;

  //updates global_updated_amount (for sending and updating data)
  global_updated_quantity[order_id] = quantity;

  $('#amount').text(global_amount);
  $('#amount_with_fee').text(global_amount_with_fee);
  $('#amount_'+order_id).text(updated_amount);
  element.attr("data-old-quantity", quantity);
}

</script>

<script type="text/javascript">
  

</script>

<!--  ----Global Transaction Table----- -->
<script src="<?php  echo site_url();?>assets/js/transaction_datable.js"></script>

<script src="<?php  echo site_url();?>assets/js/view_order_transaction_details.js"> </script>   

<script src="<?php  echo site_url();?>assets/js/cancel_transaction.js"></script>

<script src="<?php  echo site_url();?>assets/js/sidebar.js"></script>

</body>

</html>