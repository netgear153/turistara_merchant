<div class="card">
               <div class="card-body">
                  <h5 class="card-title ">Customer Details</h5>
                  <br>
                  <div class="row">
                     <div class="col-lg-12 col-xl-6">
                        <div class="table-responsive">
                           <table class="table table-sm">
                              <tbody>
                                 <tr>
                                    <th scope="row">First Name</th>
                                    <td><span id="first_name"></td>
                                 </tr>
                                 <tr>
                                    <th scope="row">username</th>
                                    <td><span id="username"></td>
                                 </tr>
                                 <tr>
                                    <th scope="row">address</th>
                                    <td><span id="address"></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>

                     <div class="col-lg-12 col-xl-6">
                        <div class="table-responsive">
                           <table class="table table-sm">
                              <tbody>
                                 <tr>
                                    <th scope="row">Last Name</th>
                                    <td><span id="last_name"></span></td>
                                 </tr>
                                 <tr>
                                    <th scope="row">Email</th>
                                    <td><span id="email"></span></td>
                                 </tr>
                                 <tr>
                                    <th scope="row">Contact Number</th>
                                    <td><span id="contact_number"></span></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
             <div class="card">
               <div class="card-body">
                  <h5 class="card-title ">Food Details</h5>
                  <br>
                  <div class="table-responsive">
                     <table id="table_for_orders" class="table table-hover responsive ">
                        <thead>
                           <tr>
                              <th>Image</th>
                              <th>food Name</th>
                              <th>Stock</th>
                              <th>Quantity</th>
                              <th>Amount</th>
                              <th id="remove_cancel_thead"></th>
                           </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                     </table>
                  </div>
                  <div class="row">
                     <div class="col-lg-12 col-xl-6">
                   
                        <div class="table-responsive">
                           <table class="table table-sm">
                              <tbody>
                                 <tr>
                                    <th scope="row">Payment ID</th>
                                    <td><span id="payment_id"></td>
                                 </tr>
                                 <tr>
                                    <th scope="row">Date</th>
                                    <td><span id="date_of_payment"></td>
                                 </tr>
                                  <tr>
                                    <th scope="row">Payment Methods</th>
                                    <td><span id="payment_method"></span></td>
                                 </tr>
                                 <tr>
                                    <th scope="row">Status</th>
                                    <td><span id="payment_status"></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>

                     <div class="col-lg-12 col-xl-5">
                           <table class="table table-sm text-center">
                              <tbody>
                                 <tr>
                                    <th scope="row">Total Orders:</th>
                                    <td>&#8369;<span id="amount"></span></td>
                                 </tr>
                                 <tr>
                                    <th scope="row">Delivery Fee: </th>
                                    <td><input type="number" id="delivery_fee" name="delivery_fee" class="form-control pamount" placeholder="Fee" step="0.01" style="text-align: right;">
                                    <small><span class="text-danger text-left" id="error_delivery_fee"></span></small></td>
                                 </tr>
                                  <tr>
                                    <th scope="row">Total Amount with Fee:</th>
                                    <td>&#8369;<span id="amount_with_fee"></td>
                                 </tr>
                              </tbody>
                           </table>
                     </div>
                     <div class="col-lg-12 col-xl-1"></div>
                  </div>
               </div>
            </div>
           
            <div class="card">
               <div class="card-body">
                  <div class="row">
                     <div class="col-lg-12 col-xl-6">
                        <h1 class="text-left">Customer's Note</h1>
                        <textarea id="note" name="note" style="height: 150px;
                           width: 98%;
                           padding:1%;
                           "readonly></textarea>
                        <br>
                     </div>
                     <div class="col-lg-12 col-xl-6">
                      <h1>Message</h1>
                  <textarea id="message" name="message" placeholder="Always leave a message to the customers, &#x0a;-e.g 'All your requested orders are available'. &#x0a;&#x0a;If you cancelled an order &#x0a;-e.g 'Our grilled chicken is currently unavailable. Were very sorry.'" style="height: 150px;
                           width: 98%;
                           padding:1%;
                           "></textarea>
                  <span class= "text-danger" id="error_message"></span>
                       
                     </div>
                  </div>
               </div>
            </div>