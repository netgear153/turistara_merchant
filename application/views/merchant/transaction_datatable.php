<div class="table-responsive">
	<table id="dataTable" class="table table-striped table-bbookinged nowrap">
		<thead>
			<tr>
				<th>Transaction ID</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Date of Transaction</th>
				<th>Payment_method</th>
				<th id="remove_status">Status</th>
				<th></th>
			</tr>
		</thead>
	</table>
</div>