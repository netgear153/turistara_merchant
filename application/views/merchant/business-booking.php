<!DOCTYPE html>
<html lang="en">

<head>
    <title>Transaction - booking </title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">

    <link rel="icon" href="<?php echo base_url();?>assets/img/turistara/icon.png" type="image/x-icon">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/themify-icons/themify-icons.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/icofont/css/icofont.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/feather/css/feather.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/extensions/responsive/css/responsive.dataTables.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/ekko-lightbox.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/sweetalert.css">

</head>

<body>

<?php include 'navbar.php'; ?> 
 <?php include 'sidebar.php'; ?> 
 
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">

                            <div class="main-body">
                                <div class="page-wrapper">

                                    <div class="page-header">
                                        <div class="row align-items-end">
                                            <div class="col-lg-8">
                                                <div class="page-header-title">
                                                    <div class="d-inline">
                                                        <?php foreach($result_accommodation_business as $row) { ?>
                                                        <h4><?php echo $row->business_name;  ?></h4>
                                                        <span><?php echo $row->description;  ?></span>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="page-header-breadcrumb">
                                                    <ul class="breadcrumb-title">
                                                        <li class="breadcrumb-item">
                                                            <a href="index.html"> <i class="feather icon-home"></i> </a>
                                                        </li>
                                                        <li class="breadcrumb-item"><a href="#!">Transactions</a>
                                                        </li>
                                                        <li class="breadcrumb-item"><a href="#!">bookings</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="page-body">

                                        <div class="card">
                                            <div class="card-block">
                                               <div class="table-responsive">
                                                 
                                               
                                                    <table id="dataTable" class="table table-striped table-bbookinged nowrap">
                                                         <thead>
                                                            <tr>
                                                                <th>Transaction ID</th>
                                                                <th>First Name</th>
                                                                <th>Last Name</th>
                                                                <th>Date of Transaction</th>
                                                                <th></th>
                                                            </tr>
                                                          </thead>
                                                    </table>
                                                
                                        
                                            </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div id="styleSelector">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


 <div id="dataModal" class="modal fade">  
      <div class="modal-dialog" style="  
    position: relative;
    display: table; 
    overflow-y: auto;    
    overflow-x: auto;
    width: auto;
    min-width: 300px;   ">  

           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">booking Details</h4>  
                </div>  
                <div class="modal-body" id="employee_detail">  
                  <div class="card-header">
                  <h3>Customer Details</h3>
                <p><strong>Name: </strong> <span id="customer_name"></span> </p>
                <p><strong>Date of transaction:  </strong> <span id="date_of_transaction"></span> </p>
                <p><strong>Contact Number:  </strong> <span id="contact_number"></span> </p>
                <p><strong>Payment Status:  </strong> <span id="payment_status"></span> </p>
                </div> 

                <div class="card-block">
                  <div class="table-responsive">
                    <table id="dataTable_for_bookings" class="table table-striped table-hover responsive ">
                      <thead>
                          <tr>
                              <th>Image</th>
                              <th>Room Type</th>
                              <th>Per Night</th>
                          </tr>                            
                      </thead>
                    </table>
                    <h4 style="float:right;"> Total = &#8369; <span id="amount"> </h4>
                   </div>
                </div>

              </div>
                <div class="modal-footer">  
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                </div>  
           </div>  
      </div>  
 </div> 


    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/bootstrap/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/modernizr/js/css-scrollbars.js"></script>

    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/js/jszip.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/js/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/extensions/responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next/js/i18next.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>

    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/extensions/responsive/js/responsive-custom.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/js/pcoded.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/js/vartical-layout.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/assets/js/script.js"></script>

    <script src="<?php  echo site_url();?>assets/js/ekko-lightbox.min.js"></script> 

    <script src="<?php  echo site_url();?>assets/js/sweetalert.min.js"></script>

    <script src="<?php  echo site_url();?>assets/js/check_image.js"></script>

    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-23581568-13');
    </script>

    <script>

  var site_url = '<?php echo site_url(); ?>';

$.ajax({
    url: site_url + "business/fetch_booking_transaction",
    method: 'POST',
    dataType: 'json',
    success: function(response) {

        $('#dataTable').dataTable({
            responsive: true,
            data: response.data,
            columns: [{
                    'data': 'transaction_id',
                    'sortable': true
                },
                {
                    'data': 'first_name',
                    'sortable': true
                },
                {
                    'data': 'last_name',
                    'sortable': true
                },
                {
                    'data': 'date_of_transaction',
                    'sortable': true
                },
                {
                    'data': 'transaction_id',
                    'render': function(transaction_id) {
                        return '<div class="text-center"> <button type="button" name="view_booking" id="' + transaction_id + '" onclick="view_booking(' + transaction_id + ');" class="btn btn-primary waves-effect m-r-20 f-w-600 view_booking">View</button></div>';

                    },
                    'sortable': false
                }

            ]
        });
    },
    error: function(error) {
        console.log(error)
    }
});

</script>

<script>
  

function view_booking(transaction_id){

        $('#dataModal').modal("show");

        // ajax for customer details and payment
        $.ajax({
                    url: site_url+"Business/fetch_customer_and_payment",
                    type: "POST",
                    data: {'transaction_id': transaction_id},
                    dataType: 'json',
                    success: function(data) {

                      $('#customer_name').text(data.first_name);
                      $('#date_of_transaction').text(data.date_of_transaction);
                      $('#contact_number').text(data.contact_number);
                      $('#payment_status').text(data.payment_status);
                      $('#amount').text(data.amount);
                       
                    }
                });

        // ajax for bookings
        $.ajax({
                    url: site_url+"Business/fetch_booking",
                    type: "POST",
                    data: {'transaction_id': transaction_id},
                    dataType: 'json',
                    success: function(response) {

                      $('#dataTable_for_bookings').dataTable( {
                        data: response.data,
                        searching: false,
                        paging: false,
                        destroy: true,
                        bLengthChange: false,
                        iDisplayLength: 15,
                        columns: [
                            {
                              'sWidth': "130px",
                              'className': "pro-list-img",
                              'data' : 'display_image',
                               render : function(display_image) {
                                    if (display_image != ''){
                                    return  ' <img src="'+site_url+'images/room/'+display_image+'" class="img-fluid" alt="tbl" >';
                                    } else{
                                    return  '<label class="text-danger">'+'No Image'+'</label>';
                                    }
                               
                                },
                              'sortable': true
                            },
                            { data: {
                              "width": "40px", 
                              "targets": 1, 
                              'className': "room_type",
                              'room_type' : 'room_type',
                              'display_image' : 'display_image'},
                              render : function(data, type, full) {

                                return  '<h5>'+data.room_type+'</h5>'+'<span style="display:block;text-overflow: ellipsis;width:500px;overflow: hidden; white-space: nowrap;">'+data.description+'</span>';                                         
                               
                                },
                              'sortable': true
                            },
                            { 
                                'data' : 'per_night',
                                render : function(per_night) {
                                    return  '&#8369;' + per_night;
                               
                                },
                                'sortable': true}                   
                                 ]
                    });
           
                            }
                });
         
}

</script>
<script src="<?php  echo site_url();?>assets/js/sidebar.js"></script>

</body>

</html>