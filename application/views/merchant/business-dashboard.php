<!DOCTYPE html>
<html lang="en">

<head>
    <title>Dashboard</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">

    <link rel="icon" href="<?php echo base_url();?>assets/img/turistara/icon.png" type="image/x-icon">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/adminty/files/assets/pages/chart/radial/css/radial.css" type="text/css" media="all">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/feather/css/feather.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/css/jquery.mCustomScrollbar.css">
</head>

<body>

   
     <?php include 'navbar.php'; ?> 
     <?php include 'sidebar.php'; ?> 


<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <div class="col-lg-12 col-xl-6">
                            <div class="row">
                                <div class="col-xl-6 col-md-12">
                                    <div class="card bg-c-yellow text-white">
                                        <div class="card-block">
                                            <div class="row align-items-center">
                                                <div class="col">
                                                    <p class="m-b-5">Customer</p>
                                                    <h4 class="m-b-0" id="customer_num"></h4>
                                                </div>
                                                <div class="col col-auto text-right">
                                                    <i class="feather icon-user f-50 text-c-yellow"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-md-12">
                                    <div class="card bg-c-green text-white">
                                        <div class="card-block">
                                            <div class="row align-items-center">
                                                <div class="col">
                                                    <p class="m-b-5">Transaction</p>
                                                    <h4 class="m-b-0" id="transaction_num"></h4>
                                                </div>
                                                <div class="col col-auto text-right">
                                                    <i class="feather icon-credit-card f-50 text-c-green"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-xl-6 col-md-12">
                                    <div class="card bg-c-pink text-white">
                                        <div class="card-block">
                                            <div class="row align-items-center">
                                                <div class="col">
                                                    <p class="m-b-5">Number Of Orders</p>
                                                    <h4 class="m-b-0" id="order_num"></h4>
                                                </div>
                                                <div class="col col-auto text-right">
                                                    <i class="feather icon-book f-50 text-c-pink"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-md-1">
                                    <div class="card bg-c-blue text-white">
                                        <div class="card-block">
                                            <div class="row align-items-center">
                                                <div class="col">
                                                    <p class="m-b-5">Total Orders</p>
                                                    <h4 class="m-b-0">&#8369;<span id="order_payment"></span></h4>
                                                </div>
                                                <div class="col col-auto text-right">
                                                    <i class="feather icon-shopping-cart f-50 text-c-blue"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-xl-6">
                            <div class="col-xl-12 col-md-12">
                                <div class="card feed-card">
                                    <div class="card-header">
                                        <h5>Feeds</h5>
                                    </div>
                                    <div class="card-block">
                                        <div id="feeds"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   
                </div>
            </div>
        </div>
    </div>
</div>






    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/bootstrap/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/modernizr/js/css-scrollbars.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/chart.js/js/Chart.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/assets/pages/google-maps/gmaps.js"></script>

    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/widget/gauge/gauge.min.js "></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/widget/amchart/amcharts.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/widget/amchart/serial.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/widget/amchart/gauge.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/widget/amchart/pie.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/widget/amchart/light.js"></script>

    <script src="<?php echo base_url();?>assets/adminty/files/assets/js/pcoded.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/js/vartical-layout.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/assets/pages/dashboard/crm-dashboard.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/assets/js/script.js"></script>
    <script src="<?php  echo site_url();?>assets/js/adminty_design_code.js"></script>

    <script>
        

    window.site_url = '<?php echo site_url(); ?>';

        $.ajax({
        url: site_url + "Business/fetch_dashboard",
        type: "POST",
        dataType: 'json',
        success: function(data){

            $('#customer_num').text(data.customer_num);
            $('#transaction_num').text(data.transaction_num);
            $('#order_num').text(data.order_num);
            $('#order_payment').text(data.order_payment);
        
        }, error: function(data){

            console.log(data);
        }
    });
        

    //---------------FEEDS REQUESTS----------------

    var business_type       = "Foods & Restaurants";
    var transaction_type    = "order";

        $.ajax({
        url: site_url + "Business/fetch_transaction",
        type: "POST",
        data: {'status': 'Pending', 'business_type': 'Foods & Restaurants' , 'transaction_type': 'order'},
        dataType: 'json',
        success: function(data){

            var count = data.count;
            var feeds = $('#feeds');

            if( count > 1){
                    var order = 'orders';
                } else {
                    var order = 'order';
                }

            if( count != 0 ){
                

            feeds.append('<div class="row m-b-30">'+
                         '<div class="col-auto p-r-0">'+
                         '<a href="<?php echo base_url();?>business/pending" ><i class="feather icon-bell bg-simple-c-blue feed-icon"></i></a>'+
                         '</div>'+
                         '<div class="col">'+
                         '<h6 class="m-b-5">You have '+data.count+' pending '+order+' waiting for user confirmation. <span class="text-muted f-right f-13"></span></h6>'+
                         '</div>'+
                         '</div>');
        }

        
        }, error: function(data){

            console.log(data);
        }
    });


        $.ajax({
        url: site_url + "Business/fetch_transaction",
        type: "POST",
        data: {'status': 'Confirmed', 'business_type': 'Foods & Restaurants' , 'transaction_type': 'order'},
        dataType: 'json',
        success: function(data){

            var count = data.count;
            var feeds = $('#feeds');

            if( count > 1){
                    var order = 'orders';
                } else {
                    var order = 'order';
                }

            if( count != 0 ){
          
            feeds.append('<div class="row m-b-30">'+
                         '<div class="col-auto p-r-0">'+
                         '<a href="<?php echo base_url();?>business/confirmed" ><i class="feather icon-bell bg-simple-c-blue feed-icon"></i></a>'+
                         '</div>'+
                         '<div class="col">'+
                         '<h6 class="m-b-5">You have '+data.count+' confirmed '+order+' ready to prepare. <span class="text-muted f-right f-13"></span></h6>'+
                         '</div>'+
                         '</div>');
        }

        
        }, error: function(data){

            console.log(data);
        }
    });


        $.ajax({
        url: site_url + "Business/fetch_transaction",
        type: "POST",
        data: {'status': '', 'business_type': 'Foods & Restaurants' , 'transaction_type': 'order'},
        dataType: 'json',
        success: function(data){

            var count = data.count;
            var feeds = $('#feeds');

            if( count > 1){
                    var order = 'orders';
                } else {
                    var order = 'order';
                }

            if( count != 0){

            feeds.append('<div class="row m-b-30">'+
                         '<div class="col-auto p-r-0">'+
                         '<a href="<?php echo base_url();?>business/order" ><i class="feather icon-shopping-cart bg-simple-c-pink feed-icon"></i></a>'+
                         '</div>'+
                         '<div class="col">'+
                         '<h6 class="m-b-5">New '+data.count+' '+order+' received <span class="text-muted f-right f-13"></span></h6>'+
                         '</div>'+
                         '</div>');

            }
        
        }, error: function(data){

            console.log(data);
        }
    });


        $.ajax({
        url: site_url + "Business/fetch_transaction",
        type: "POST",
        data: {'status': 'Completed', 'business_type': 'Foods & Restaurants' , 'transaction_type': 'order'},
        dataType: 'json',
        success: function(data){

            var count = data.count;
            var feeds = $('#feeds');

            if( count > 1){
                    var order = 'orders';
                } else {
                    var order = 'order';
                }

            if( count != 0){

            feeds.append('<div class="row m-b-30">'+
                         '<div class="col-auto p-r-0">'+
                         '<a href="<?php echo base_url();?>business/completed" ><i class="feather icon-file-text bg-simple-c-green feed-icon"></i></a>'+
                         '</div>'+
                         '<div class="col">'+
                         '<h6 class="m-b-5">You have '+data.count+' completed '+order+'. <span class="text-muted f-right f-13"></span></h6>'+
                         '</div>'+
                         '</div>');

            }
        
        }, error: function(data){

            console.log(data);
        }
    });

</script>

<script type="text/javascript">
    
// shows "THERE NO TASK"

window.onload=function() { // when page has loaded
  setTimeout(function() { 

    if ( $("#feeds").text().length == 0 ) {

    var length = $("#feeds").html().length;

    console.log(length);

   $("#feeds").html('<span style="color:#A8A8A8">There are no task</span>');
}


  },3000); // insert after 10 seconds
}

</script>



    <script src="<?php  echo site_url();?>assets/js/sidebar.js"></script>
</body>

</html>