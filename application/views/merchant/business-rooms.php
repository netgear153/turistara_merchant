<!DOCTYPE html>
<html lang="en">

<head>
    <title>Room list</title>

    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->

        <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">

    <link rel="icon" href="<?php echo base_url();?>assets/img/turistara/icon.png" type="image/x-icon">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/font-awesome/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-bar-rating/css/fontawesome-stars.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/themify-icons/themify-icons.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/css/buttons.dataTables.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/extensions/responsive/css/responsive.dataTables.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/icofont/css/icofont.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/feather/css/feather.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/ekko-lightbox.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/sweetalert.css">

    <style type="text/css">
        
    td img {
    max-width:100%;
    height:auto;
    }

    .add_food {
    float:left;
    }

    .per_hour {
    display: flex;
      justify-content: center;
      align-items: center; ">
    }

    </style>
</head>

<body>

 <?php include 'navbar.php'; ?> 
 <?php include 'sidebar.php'; ?> 

<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-header">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <div class="d-inline">
                                    <?php foreach($result_accommodation_business as $row) { ?>
                                    <h4><?php echo $row->business_name;  ?></h4>
                                    <span><?php echo $row->business_type;  ?> - <?php echo $row->category;  ?></span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class="breadcrumb-title">
                                    <?php include 'shortcut_links.php'; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Room List</h5>
                                </div>
                                <div class="card-block">
                                    <div class="table-responsive">
                                        <table id="dataTable" class="table table-striped nowrap">
                                            <thead>
                                                <tr>
                                                    <th>Image</th>
                                                    <th>Room Details</th>
                                                    <th>Max Guest</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                        </table>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="mymodal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form  method="post"  action="<?php  echo site_url('Business/add_room');?>" enctype="multipart/form-data">
                                    <div class="modal-header">
                                        <h3 class="f-26">Add Room</h3>
                                    </div>
                                    
                                    <div class="modal-body">
                                        <div class="card">
                                            <div class="card-body text-center " >
                                                <span class="text-muted text-center mt-5" id="no_image">No image.</span>
                                                <img alt="No Image" id="image-field" width="400px" height="300px" class="responsive" accept = "image/jpg,image/png/,image/jpeg"  >
                                                
                                            </div>
                                        </div>
                                        
                                        <div class="input-group">
                                            <div class="mx-auto input-group " >
                                                <div id="outer" style="margin: auto;">
                                                    <input type="file" id="display_image" name="display_image" class="custom-file-input" onchange= "previewImage(this)" >
                                                    <label class="input-group-addon btn btn-primary" for="display_image">Choose Image</label>
                                                </div>
                                            </div>
                                        </div>

                                    <div class="row">
                                        <div class="col-sm-6" >
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icofont icofont-bed"></i></span>
                                            <input id="room_type" name="name" class="form-control pname" placeholder="Room Name" required>
                                            <input type="hidden" id="image_path" name="image_path" value="room">
                                        </div>
                                        </div>

                                        <div class="col-sm-6" >
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icofont icofont-people"></i></span>
                                            <input type="number" id="max_guest" name="max_guest" class="form-control pname" placeholder="Maximum Guest" step="0.01" required>
                                        </div>
                                        </div>
                                    </div>


                                    <!-- NEED FOR CONFIRMATION OF DONJOSE -->

                                   <!--  <div class="row">
                                            <div class="col-sm-12" >
                                                <?php if(!empty($result_room_rate)){ ?>
                                                <label>Room Type: &nbsp; <?php echo $result_room_rate[0]->room_rate_type; ?></label>
                                                <input type="hidden" name="room_rate_type" value="<?php echo $result_room_rate[0]->room_rate_type; ?>">

                                                
                                                 <?php foreach  ($result_room_rate as $room_rate) { ?>

                                                    <div class="input-group per_hour">
                                                        <h5>Per&nbsp;</h5>
                                                        <input type="number" name="rate[]" class="" placeholder="" style="text-align: center; " pattern="/^-?\d+\.?\d*$/" value="<?php echo $room_rate->rate; ?>" readonly required> &nbsp;<h5>hours&nbsp;</h5>&nbsp;:&nbsp;&nbsp;&nbsp;
                                                        <span class="input-group-addon"><i class="icofont icofont-cur-peso"></i></span>
                                                        <input type="text" name="price[]" placeholder="0.00" step="0.01" style="text-align: center; width:  100px;" required>
                                                    </div>
                                                    <?php }}?>
                                            </div>

                                        </div>
                                      <?php  if(empty($result_room_rate)){ ?> -->


                                        <div class="input-group">
                                            <label>Room Type: &nbsp;</label>
                                            <select id="room_rate_type" name="room_rate_type" required>
                                              <option selected disabled>Select Room Rate</option>
                                              <option value="per night">Per Night</option>
                                              <option value="per hour">Per Hour</option>
                                            </select>                                           
                                        </div>
                                        

                                        <div id="per_night_input">
                                            <div class="input-group">
                                            <span class="input-group-addon"><i class="icofont icofont-cur-peso"></i></span>
                                            <input id="rate[]" name="rate[]" value="night" hidden>
                                             <input type="number" name="price[]" class="form-control" placeholder="Price per night" placeholder="0.00" step="0.01">                                            
                                            </div>
                                        </div>

                                        <div id="per_hour_input">
                                             <div class="input-group">
                                            <button class="btn btn-primary waves-effect m-r-20 f-w-600 d-inline-block add_field_button" >Add More</button>
                                             </div>
                                            
                                            <div class="per_hour_append">
                                                <div class="input-group per_hour"> 
                                                <h5>Per&nbsp;</h5>
                                                <input type="number" name="rate[]" style="text-align: center; width:  50px;" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" min="1" max="24"> &nbsp;<h5>hours&nbsp;</h5>&nbsp;:&nbsp;&nbsp;&nbsp;
                                                <span class="input-group-addon"><i class="icofont icofont-cur-peso"></i></span>
                                                <input type="text" name="price[]" placeholder="0.00" step="0.01" style="text-align: center; width:  100px;"><a href="#" style="color: transparent; background-color: transparent; border-color: transparent; cursor: default;" disabled>Remove</a>
                                                </div>
                                            </div>
                                        </div>
                                        
                                       <!--  <?php } ?> -->
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                            <textarea class="form-control station-important" placeholder="Description of the Room" name="description" id="description" rows="4" cols="50"></textarea>
                                        </div>

                                        <div class="input-group">
                                                        <select name="room_status" id="room_status" class="form-control form-control-primary" required>
                                                            <option disabled value="" selected hidden>---- Select Room Status ----</option>
                                                            <option value="Available">Available</option>
                                                            <option value="Not Available">Not Available</option>
                                                        </select>
                                        </div>

                                    </div>
                                    <div class= "modal-footer">
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary waves-effect m-r-20 f-w-600 d-inline-block save_btn">Save</button>
                                            <button type="button" class="btn btn-primary waves-effect m-r-20 f-w-600 md-close d-inline-block close_btn" data-dismiss="modal" >Close</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/bootstrap/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/modernizr/js/css-scrollbars.js"></script>

    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/js/jszip.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/js/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/extensions/responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next/js/i18next.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>

    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/extensions/responsive/js/responsive-custom.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/js/pcoded.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/js/vartical-layout.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/assets/js/script.js"></script>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>

    <script src="<?php  echo site_url();?>assets/js/ekko-lightbox.min.js"></script> 

    <script src="<?php  echo site_url();?>assets/js/sweetalert.min.js"></script> 

    <script src="<?php  echo site_url();?>assets/js/check_image.js"></script>

    <script src="<?php  echo site_url();?>assets/js/adminty_design_code.js"></script>

    <script>  

        var site_url = '<?php echo site_url(); ?>';

        $('#image-field').hide();

        function delete_room(room_id) {

        Swal.fire({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                  if (result.value) {

                    $.ajax({
                    url: site_url+"Business/delete_room/",
                    type: "POST",
                    data: {'room_id': room_id},
                    dataType: 'text',
                    success: function (response) {
                       
                         Swal.fire({
                                title: "Deleted!",
                                text: "Food item has been deleted.",
                                type: "success"
                            }).then(function() {
                                window.location = site_url+'Business/rooms';
                            });

                    },
                      error: function (response) {
                         
                          Swal.fire({
                                title: "Error!",
                                text: "Food item couldn't be deleted! Please try again.",
                                type: "error"
                            }).then(function() {
                                window.location = site_url+'Business/rooms';
                            });

                    }
                });


   
                  }
                });
        }


  $.ajax({
            url: site_url+"business/fetch_room",
            method: 'POST',
            dataType: 'json',
            success: function(response) {

            $('#dataTable').dataTable( {
                data: response.data,
                bLengthChange: false,
                iDisplayLength: 15,
                bAutoWidth: false,
                sScrollY: "100%",
                dom: 'l<"add_room">frtip',
                initComplete: function(){
                $("div.add_room").html('<button type="button" class="btn btn-primary waves-effect waves-light f-right d-inline-block md-trigger" data-modal="modal-13" data-toggle="modal" data-target="#mymodal" style="float: left;"> <i class="icofont icofont-plus m-r-5"></i> Add Room </button>');           
                },

                aoColumns: [
                    {
                      'sWidth': "130px",
                      'className': "pro-list-img",
                      'data' : 'display_image',
                       render : function(display_image) {
                            if (display_image != null){
                            return  ' <a data-toggle="lightbox" href="'+site_url+'images/room/'+display_image+'" ><img src="'+site_url+'images/room/'+display_image+'" class="img-fluid" alt="tbl" ><a/>';
                            } else{
                            return  '<label class="text-danger">'+'No Image'+'</label>';
                            }
                       
                        },
                      'sortable': true
                    },
                    { data: {
                      "fixedColumns": "true",
                      "sWidth": '500px',  
                      "targets": 1, 
                      'className': "room_type"},
                      render : function(data, type, full, meta) {

                        var room_type = data.room_type;
                        var description = data.description;
                        var room_id = data.room_id;

                        var currentCell = $("#dataTable").DataTable().cells({"row":meta.row, "column":meta.col}).nodes(0);

                        $.ajax({
                            url: site_url+"business/fetch_room_rate",
                            type: "POST",
                            data: {'room_id': data.room_id},
                            dataType: 'json'
                        }).done(function (data) {


                            room_rate_value = [];

                            $.each(data, function(index, value) {                            
                                room_rate_value.push('<li id="' + value.room_rate_id + '">' +'per '+value.rate + ': &#8369;' + value.price + '</li>');
                            });

                            var result = room_rate_value.join('');

                             $(currentCell).html('<h5>'+room_type+'</h5>'+'<span style="display:block;text-overflow: ellipsis; width:600px; overflow: hidden; white-space: nowrap;">'+description+'</span>'+result);


                             $($.fn.dataTable.tables(true)).DataTable().columns.adjust();

                        });

                        //This is for the sake of searching....
                        return data.room_type+data.description+data.room_rate_type;
                          
                        },
                      'sortable': true
                    },
                    {  
                      "targets": 3,
                      "className": "text-center",
                      "width": "4%",
                      'data' : 'max_guest',
                      'sortable': false,
                       render : function(max_guest) {
                            return  '<label>'+max_guest+'</label>';                       
                        }
                    }
                    ,
                    {  
                      "className": "dt-center",
                      'data' : 'room_status',
                      'sortable': false,
                       render : function(room_status) {
                            if (room_status == 'Available'){
                            return  '<label class="text-success">'+room_status+'</label>';
                            } else{
                            return  '<label class="text-danger">'+room_status+'</label>';
                            }
                       
                        }
                    }
                    ,
                    {
                      'className': "action-icon",  
                      'data' : 'room_id',
                      'render' : function(room_id) {

                        return  '<a href="'+site_url+'Business/edit_rooms/'+room_id+'" class="m-r-15 text-muted" data-toggle="tooltip" data-placement="top" data-original-title="Edit">'+'<i class="icofont icofont-ui-edit"></i></a>'+
                            '<a onclick="delete_room('+room_id+');" class="m-r-15 text-muted" data-toggle="tooltip" data-placement="top" data-original-title="Delete">'+'<i class="icofont icofont-ui-delete"></i></a>';
                    },
                     'sortable': false}
                    ,

                    ]
            });
          }
          , error: function (error) {
                console.log(error)
                }
        });

</script>

<script type="text/javascript">
    $(document).ready(function(){
    var max_fields      = 4; //maximum input boxes allowed
    var wrapper         = $(".per_hour_append"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="input-group per_hour">'+
                               '<h5>Per&nbsp;</h5>'+
                                '<input type="number" name="rate[]" style="text-align: center; width:  50px;" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" min="1" max="24" required>&nbsp;<h5>hours&nbsp;</h5>&nbsp;:&nbsp;&nbsp;&nbsp;'+
                                '<span class="input-group-addon"><i class="icofont icofont-cur-peso"></i></span>'+
                                '<input type="text" name="price[]" placeholder="0.00" step="0.01" style="text-align: center; width:  100px;" required>'+
                              '<a href="#" class="remove_field">&nbsp;Remove</a></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })


//---------- CODE FOR SELECT ROOM RATE TYPE --------//

$('#per_night_input').hide();
$('#per_night_input input').prop("disabled", "disabled");


$('#per_hour_input').hide();
$('#per_hour_input input').prop("disabled", "disabled");


var global_select = "" || 0;

$('#room_rate_type').on('change', function() {


    $(global_select).hide();
    $(global_select + ' input').prop("disabled", "disabled");

    var room_rate_type = $(this).val();

    if (room_rate_type == "per night") {

        room_rate_type = "#per_night_input";

        $(room_rate_type).show();
       var sample = $(room_rate_type + " input").removeAttr("disabled").attr("required", "required");
       console.log(sample);

    } else {

        room_rate_type = "#per_hour_input";

        $(room_rate_type).show();
        $(room_rate_type + " input").removeAttr("disabled").attr("required", "required");

    }

    global_select = room_rate_type;

});


});

</script>

<script src="<?php  echo site_url();?>assets/js/sidebar.js"></script>

</body>

</html>