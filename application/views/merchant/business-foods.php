<!DOCTYPE html>
<html lang="en">

<head>
    <title>Product list</title>

    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">

    <link rel="icon" href="<?php echo base_url();?>assets/img/turistara/icon.png" type="image/x-icon">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/font-awesome/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-bar-rating/css/fontawesome-stars.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/themify-icons/themify-icons.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/css/buttons.dataTables.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/extensions/responsive/css/responsive.dataTables.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/icofont/css/icofont.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/feather/css/feather.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/ekko-lightbox.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/sweetalert.css">
    <script src="<?php  echo site_url();?>assets/js/sweetalert.min.js"></script>

    <style type="text/css">
        
    td img {
    max-width:100%;
    height:auto;}

    .add_food {
    float:left;}

    </style>
</head>

<body>

  <?php include 'navbar.php'; ?> 
  <?php include 'sidebar.php'; ?> 

  <!--  ALERT IF LOGO IS SUCCESFULLY UPDATED -->
<?php if ($this->session->flashdata('success')): ?>
    <script>
    Swal.fire({
    title: "Success!",
    text: "<?php echo $this->session->flashdata('success'); ?>",
    type: "success",
    timer: 8000,
    });
    </script>
<?php endif; ?>

<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-header">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <div class="d-inline">
                                    <?php foreach($result_food_business as $row) { ?>
                                    <h4><?php echo $row->business_name;  ?></h4>
                                    <span><?php echo $row->business_type;  ?> - <?php echo $row->category;  ?></span>
                                    <?php } ?>  
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class="breadcrumb-title">
                                    <?php include 'shortcut_links.php'; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Menu List</h5>
                                </div>
                                <div class="card-block">
                                    <div class="table-responsive">
                                        <table id="dataTable" class="table table-hover nowrap">
                                            <thead>
                                                <tr>
                                                    <th>Image</th>
                                                    <th>food Name</th>
                                                    <th>Stock</th>
                                                    <th>Amount</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="mymodal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form method="post" action="<?php  echo site_url('Business/add_food');?>" enctype="multipart/form-data">
                                <div class="modal-header">
                                    <h3 class="f-26">Add Product</h3>
                                </div>
                                <div class="modal-body">
                                    <div class="card">
                                        <div class="card-body text-center ">
                                            <span class="text-muted text-center mt-5" id="no_image">No image.</span>
                                            <img alt="No Image" id="image-field" width="400px" height="300px" class="responsive"
                                                accept="image/jpg,image/png/,image/jpeg">
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <div class="mx-auto input-group ">
                                            <div id="outer" style="margin: auto;">
                                                <input type="file" id="display_image" name="display_image" class="custom-file-input"
                                                    onchange="previewImage(this)">
                                                <input type="hidden" id="image_path" name="image_path" value="food">
                                                <label class="input-group-addon btn btn-primary" for="display_image">Choose Image</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icofont icofont-fast-food"></i></span>
                                        <input id="food_name" name="name" class="form-control pname" placeholder="Product Name" required>
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icofont icofont-cur-peso"></i></span>
                                        <input type="number" id="price" name="price" class="form-control pamount" placeholder="Amount in Peso" step="0.01"
                                            required>
                                    </div>
                                    <div class="input-group">
                                        <textarea class="form-control station-important" placeholder="Description of the Product" name="description"
                                            id="description" rows="4" cols="50"></textarea>
                                    </div>
                                    <div class="input-group">
                                        <select name="stock" id="description" class="form-control form-control-primary" required>
                                            <option disabled value="" class="text-center" selected hidden>---- Select Stock ----</option>
                                            <option value="In Stock">In Stock</option>
                                            <option value="Out of Stock">Out of Stock</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary waves-effect m-r-20 f-w-600 d-inline-block save_btn">Save</button>
                                        <button type="button" class="btn btn-primary waves-effect m-r-20 f-w-600 md-close d-inline-block close_btn"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/bootstrap/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/modernizr/js/css-scrollbars.js"></script>

    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/js/jszip.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/js/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/extensions/responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next/js/i18next.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>

    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/extensions/responsive/js/responsive-custom.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/js/pcoded.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/js/vartical-layout.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/assets/js/script.js"></script>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>

    <script src="<?php  echo site_url();?>assets/js/ekko-lightbox.min.js"></script> 

    <script src="<?php  echo site_url();?>assets/js/check_image.js"></script>

    <script src="<?php  echo site_url();?>assets/js/adminty_design_code.js"></script>

    <script>

        $('#image-field').hide();

        var site_url = '<?php echo site_url(); ?>';

        function delete_food(food_id) {

        Swal.fire({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                  if (result.value) {

                    $.ajax({
                    url: site_url+"Business/delete_food/",
                    type: "POST",
                    data: {'food_id': food_id},
                    dataType: 'text',
                    success: function (response) {
                       
                         Swal.fire({
                                title: "Deleted!",
                                text: "Food item has been deleted.",
                                type: "success"
                            }).then(function() {
                               window.location = site_url+'Business/foods';
                            });

                    },
                      error: function (response) {
                         
                          Swal.fire({
                                title: "Error!",
                                text: "Food item couldn't be deleted! Please try again.",
                                type: "error"
                            }).then(function() {
                              window.location = site_url+'Business/foods';
                            });

                    }
                });


   
                  }
                });
        }

   



  $.ajax({
            url: site_url+"business/fetch_food",
            method: 'POST',
            dataType: 'json',
            success: function(response) {

            $('#dataTable').dataTable( {
                
                data: response.data,
                bLengthChange: false,
                iDisplayLength: 15,
                sScrollY: "100%",  
                dom: 'l<"add_food">frtip',
                initComplete: function(){
                $("div.add_food").html('<button type="button" class="btn btn-primary waves-effect waves-light f-right d-inline-block md-trigger" data-modal="modal-13" data-toggle="modal" data-target="#mymodal" style="float: right;"> <i class="icofont icofont-plus m-r-5"></i> Add Product </button>');           
                },

                columns: [
                    {
                      'sWidth': "130px",
                      'className': "pro-list-img",
                      'data' : 'display_image',
                       render : function(display_image) {
                            if (display_image != null || ''){
                            return  ' <a data-toggle="lightbox" href="'+site_url+'images/food/'+display_image+'" ><img src="'+site_url+'images/food/'+display_image+'" class="img-fluid" alt="tbl" ></a>';
                            } else{
                            return  '<label class="text-danger">'+'No Image'+'</label>';
                            }
                       
                        },
                      'sortable': true
                    },
                    { data: {
                      "width": "40px", 
                      "targets": 1, 
                      'className': "food_name",
                      'food_name' : 'food_name',
                      'display_image' : 'display_image'},
                      render : function(data, type, full) {

                        return  '<h5>'+data.food_name+'</h5>'+'<span style="display:block;text-overflow: ellipsis;width:500px;overflow: hidden; white-space: nowrap;">'+data.description+'</span>';
                               
                                    
                       
                        },
                      'sortable': true
                    },
                    { 
                        'data' : 'stock',
                        render : function(stock) {
                            if (stock == 'In Stock'){
                            return  '<label class="text-success">'+stock+'</label>';
                            } else{
                            return  '<label class="text-danger">'+stock+'</label>';
                            }
                       
                        },
                        'sortable': true
                    }
                    ,
                    { 
                        'data' : 'price',
                        render : function(price) {
                            return  '&#8369;' + price;
                       
                        },
                        'sortable': true}                   
                    ,
                    {
                      'className': "action-icon",  
                      'data' : 'food_id',
                      'render' : function(food_id) {

                        return  '<a href="'+site_url+'Business/edit_product/'+food_id+'" class="m-r-15 text-muted" data-toggle="tooltip" data-placement="top" data-original-title="Edit">'+'<i class="icofont icofont-ui-edit"></i></a>'+
                            '<a onclick="delete_food('+food_id+');" class="m-r-15 text-muted" data-toggle="tooltip" data-placement="top" data-original-title="Delete">'+'<i class="icofont icofont-ui-delete"></i></a>';
                    },
                     'sortable': false}
                    ,


                         ]
            });
          }
          , error: function (error) {
                console.log(error)
                }
        });

</script>

<script src="<?php  echo site_url();?>assets/js/sidebar.js"></script>

</body>

</html>