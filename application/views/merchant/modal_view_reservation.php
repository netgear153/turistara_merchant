<style type="text/css">

table { 
  table-layout: fixed;
  width: 100%
}

.expiry_time_interval {

   text-align: right; 
   width:  40px; 

}

</style>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title ">Customer Details</h5>
                        <br>
                        <div class="row">
                            <div class="col-lg-12 col-xl-6">
                                <div class="table-responsive">
                                    <table class="table table-sm">
                                        <tbody>
                                            <tr>
                                                <th scope="row">First Name</th>
                                                <td><span id="first_name"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">username</th>
                                                <td><span id="username"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">address</th>
                                                <td><span id="address"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-12 col-xl-6">
                                <div class="table-responsive">
                                    <table class="table w-auto">
                                        <tbody>
                                            <tr>
                                                <th scope="row">Last Name</th>
                                                <td><span id="last_name"></span></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Email</th>
                                                <td><span id="email"></span></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Contact Number</th>
                                                <td><span id="contact_number"></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title ">Reservation Details</h5>
                        <br>
                        <div class="row">
                            <div class="col-lg-12 col-xl-6">
                                    <table class="table table-sm">
                                        <tbody>
                                            <tr>
                                                <th scope="row">Check In</th>
                                                <td><span id="check_in_date"></span></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Guests</th>
                                                <td><span id="guest"></span></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Nights</th>
                                                <td><span id="night"></span></td>
                                            </tr>
                                            <tr class="remove_time_interval">
                                                <th scope="row">Expiry Time Interval</th>
                                                <td>
                                                 <input type="number" id="hours_interval" class="expiry_time_interval" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" min="0" max="99" placeholder="00">:
                                                 <input type="number" id="minutes_interval" class="expiry_time_interval" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" min="0" max="99" placeholder="00">:
                                                 <input type="number" id="seconds_interval" class="expiry_time_interval" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" min="0" max="99" placeholder="00">

                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                            </div>
                            <div class="col-lg-12 col-xl-6">
                                    <table class="table table-sm">
                                        <tbody>
                                            <tr>
                                                <th scope="row">Check Out</th>
                                                <td><span id="check_out_date"></span></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Adult</th>
                                                <td><span id="num_of_adult"></span></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Children</th>
                                                <td><span id="num_of_children"></span></td>
                                            </tr>
                                             <tr>
                                                <th scope="row">Children Ages</th>
                                                <td><div class="form-group"><textarea id="children_age" rows="3" cols="19" readonly style="    border: none;
                                                    overflow: auto;
                                                    outline: none;
                                                    -webkit-box-shadow: none;
                                                    -moz-box-shadow: none;
                                                    box-shadow: none;
                                                    display:block;"></textarea></div></td>
                                            </tr>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title ">Room Details</h5>
                        <br>
                        <div class="row">
                            <div class="col-lg-12 col-xl-6 text-center">
                                <img id="display_image" class="img-fluid text-center" alt="tbl" width="250px" height="250px">
                            </div>
                            <div class="col-lg-12 col-xl-6">
                                <div class="table-responsive">
                                    <table class="table table-sm" id="room_rate">
                                        <tbody>
                                            <tr>
                                                <th scope="row">Room Type</th>
                                                <td><span id="room_type"></span></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Max Guest</th>
                                                <td><span id="max_guest"></span></td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <br>
                        <h5>Description</h5>
                        <div class="row" style="margin: 5px;">
                        <textarea rows="5" cols="100%" wrap="hard" id="description"  style="border: none"> </textarea>
                        </div>
                    </div>
                </div>