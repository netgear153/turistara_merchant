<!DOCTYPE html>
<html lang="en">

<head>
    <title>Edit Product</title>

    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="description" content="#">
<meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
<meta name="author" content="#">
<link rel="icon" href="<?php echo base_url();?>assets/img/turistara/icon.png" type="image/x-icon">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-bar-rating/css/bars-1to10.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-bar-rating/css/bars-horizontal.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-bar-rating/css/bars-movie.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-bar-rating/css/bars-pill.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-bar-rating/css/bars-reversed.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-bar-rating/css/bars-square.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-bar-rating/css/css-stars.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/themify-icons/themify-icons.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/icofont/css/icofont.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/feather/css/feather.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/image_hover_effect.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/sweetalert.css">

</head>

<body>

<?php include 'navbar.php'; ?>
<?php include 'sidebar.php'; ?>

<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-header">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <div class="d-inline">
                                    <h4>Edit Product</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class="breadcrumb-title">
                                    <?php include 'shortcut_links.php'; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Edit Room</h5>
                                </div>
                                <div class="card-block">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="product-edit">
                                                <ul class="nav nav-tabs nav-justified md-tabs " role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" data-toggle="tab" href="#edit_room" role="tab">
                                                            <div class="f-20">
                                                                <i class="icofont icofont-edit"></i>
                                                            </div>
                                                            Edit Room</a>
                                                      
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#gallery" role="tab">
                                                            <div class="f-20">
                                                                <i class="icofont icofont-ui-image"></i>
                                                            </div>
                                                            Pictures</a>
                                                      
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="edit_room" role="tabpanel">
                                                        <form method="post" class="md-float-material card-block" action="<?php  echo site_url('Business/edit_room');?>" enctype="multipart/form-data">
                                                            <div class="row">
                                                                <div class="mx-auto card">
                                                                    <div class="card-body">
                                                                        <?php if($result_room->display_image !='')
                                                                        { ?>
                                                                            <img alt="No Image" id="image-field" width="100%" height="100%" class="responsive" accept="image/jpg,image/png/,image/jpeg" src="<?php  echo site_url();?>images/room/<?php echo $result_room->display_image;  ?>">
                                                                            <?php } else{ ?>
                                                                                <span class="text-muted text-center mt-3" id="no_image">No image.</span>
                                                                                <img alt="No Image" id="image-field" width="100%" height="100%" class="responsive" accept="image/jpg,image/png/,image/jpeg" src="<?php  echo site_url();?>images/room/<?php echo $result_room->display_image;  ?>">
                                                                                <?php } ?>

                                                                    </div>
                                                                </div>

                                                                <div class="mx-auto input-group ">
                                                                    <div id="outer" style="margin: auto;">

                                                                        <input type="hidden" id="old_display_image" name="old_display_image" class="form-control pname" placeholder="Product Name" value="<?php echo $result_room->display_image; ?>">
                                                                        <input type="file" id="display_image" name="display_image" class="custom-file-input" onchange="previewImage(this)" hidden>
                                                                        <label class="input-group-addon btn btn-primary" for="display_image">Choose Image</label>

                                                                        <input type="hidden" id="image_path" name="image_path" value="room">
                                                                        <input type="hidden" id="room_id" name="room_id" value="<?php echo $result_room->room_id; ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xl-6">
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <label> Room Type/Name</label>
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                                                                <input id="room_type" name="name" class="form-control pname" placeholder="Room Type/Name" value="<?php echo $result_room->room_type; ?>" required>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <label>Maximum Guest</label>
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon"><i class="icofont icofont-people"></i></span>
                                                                                <input type="number" id="max_guest" name="max_guest" class="form-control pname" placeholder="Maximum Guest" step="0.01" value="<?php echo $result_room->max_guest; ?>" required>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <label>Room Rate: <?php echo $result_room->room_rate_type; ?></label>
                                                                            <?php foreach ($result_room_rate as $room_rate) { ?>
                                                                                <div class="input-group">
                                                                                    <span class="input-group-addon"><i class="icofont icofont-price"></i></span>
                                                                                    <input hidden name="room_rate_id[]" value="<?php echo $room_rate->room_rate_id ?>">
                                                                                    <input type="text" name="rate[]" class="form-control rate" placeholder="e.g 12 hours" style="text-align: center;" value="<?php echo $room_rate->rate ?>" required readonly>&nbsp;:&nbsp;&nbsp;&nbsp;
                                                                                    <span class="input-group-addon"><i class="icofont icofont-cur-peso"></i></span>
                                                                                    <input type="text" name="price[]" class="form-control" placeholder="0.00" step="0.01" value="<?php echo $room_rate->price ?>" required>
                                                                                </div>
                                                                                <?php }?>

                                                                        </div>

                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <label>Room Status</label>
                                                                            <select name="room_status" id="description" class="form-control form-control-primary" required>
                                                                                <option hidden>---- Select Stock ----</option>
                                                                                <option value="Available" <?php if($result_room->room_status == "Available" ){ echo "selected"; } ?>>Available
                                                                                </option>
                                                                                <option value="Not Available" <?php if($result_room->room_status == "Not Available" ){ echo "selected"; } ?>>Not Available</option>
                                                                            </select>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-xl-6">
                                                                    <div class="col-sm-12">
                                                                        <label>Description</label>
                                                                        <div class="input-group">
                                                                            <textarea class="form-control station-important" placeholder="Description of the Product" name="description" id="description" rows="9" cols="50">
                                                                                <?php echo $result_room->description; ?>
                                                                            </textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="text-center m-t-20">
                                                                <button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Save
                                                                </button>
                                                                <button type="button" class="btn btn-warning waves-effect waves-light" onclick="discard_room();">Discard
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </form>
                                                </div>

                                                <div class="tab-pane" id="gallery" role="tabpanel">
                                                    <div class="md-float-material card-block">
                                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#upload_image">Add Image</button>
                                                        <div class="card-block">
                                                            <div class="container" style="min-height: 30vh;">
                                                                <div class="row text-center text-lg-left">

                                                                    <?php if(!empty($result_room_image)){ ?>
                                                                        <?php foreach($result_room_image as $row) { ?>
                                                                            <div class="col-lg-3 col-md-4 col-6">
                                                                                <div class="hovereffect" >
                                                                                    <img src="<?php echo base_url();?>images/room/<?php echo $row->image_name; ?>" alt="user-img" class="img-thumbnail" >
                                                                                    <div class="overlay">
                                                                                        <h2><?php echo $row->upload_date; ?></h2>
                                                                                        <a class="info" href="<?php echo base_url();?>images/room/<?php echo $row->image_name; ?>" data-toggle="lightbox" data-gallery="gallery">View</a>
                                                                                        <a class="info delete_image" href="<?php echo base_url();?>business/delete_room_image/<?php echo $row->room_image_id; ?>/room/<?php  echo $result_room->room_id;  ?>">Delete</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <?php }} else{ ?>
                                                                </div>
                                                                <h1 style="text-align: center;" class="text-muted">---Please upload rooms image ---</h1>
                                                                <?php } ?>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<!-- Modal for Upload Image -->

<div class="modal fade" id="upload_image">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3>add image</h3>
            </div>
            <form  method="post"  action="<?php  echo site_url('Business/add_room_image');?>" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body text-center " >
                            <span class="text-muted text-center mt-5" id="no_image">No image.</span>
                            <img alt="No Image" id="image-field-gallery" width="400px" height="300px" class="responsive" accept = "image/jpg,image/png/,image/jpeg"  >
                            
                        </div>
                    </div>
                    <div class="input-group">
                        <div class="mx-auto input-group " >
                            <div id="outer" style="margin: auto;">
                                <input type="file" id="display_image_gallery" name="display_image" class="custom-file-input" onchange= "previewImageGallery(this)" >
                                <input type="hidden" id="image_path" name="image_path" value="room" >
                                <input type="hidden" name="upload_date" value="<?php  echo date('m-d-Y');   ?>" >
                                <input type="hidden" name="room_id" value="<?php  echo $result_room->room_id;  ?>" >
                                <input type="hidden" name="name" value="<?php  echo $result_room->room_type;  ?>" >
                                <label class="input-group-addon btn btn-primary" for="display_image_gallery">Choose Image</label>
                            </div>
                        </div>
                    </div>
                    <div class= "modal-footer">
                        <button type="submit" id="add-business-btn" class="btn btn-success" >Upload</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </form>
                </div>
            </div>
        </div>

<!-- End of Modal -->




<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/modernizr/js/modernizr.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/modernizr/js/css-scrollbars.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-bar-rating/js/jquery.barrating.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/assets/pages/rating/rating.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next/js/i18next.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
<script src="<?php echo base_url();?>assets/adminty/files/assets/js/pcoded.min.js"></script>
<script src="<?php echo base_url();?>assets/adminty/files/assets/js/vartical-layout.min.js"></script>
<script src="<?php echo base_url();?>assets/adminty/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/assets/js/script.js"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script> var site_url = '<?php echo site_url(); ?>';  </script>
<script src="<?php  echo site_url();?>assets/js/sweetalert.min.js"></script>
<script src="<?php echo site_url();?>assets/js/ekko-lightbox.min.js"></script>
<script src="<?php echo site_url();?>assets/js/adminty_design_code.js"></script>
<script src="<?php echo site_url();?>assets/js/check_image.js"></script>
<script src="<?php echo site_url();?>assets/js/check_image_for_gallery.js"></script>
<script src="<?php echo site_url();?>assets/js/display_image_hide.js"></script>
<script src="<?php echo site_url();?>assets/js/show_image_lightbox.js"></script>
<script src="<?php echo site_url();?>assets/js/delete_image.js"></script>
<script src="<?php echo site_url();?>assets/js/sidebar.js"></script>

<script>

function discard_room() {

    Swal.fire({
        title: 'Are you sure to discard it?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, discard it!'
    }).then((result) => {
        if (result.value) {
            window.location = site_url + 'Business/rooms';
        }
    });
}


</script>

</body>
</html>