<!DOCTYPE html>
<html lang="en">
    
<head>
    <title>Business Profile</title>
    <!--[if lt IE 10]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <link rel="icon" href="<?php echo base_url();?>assets/img/turistara/icon.png" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/bootstrap-daterangepicker/css/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/datedropper/css/datedropper.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/themify-icons/themify-icons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/icofont/css/icofont.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/feather/css/feather.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/ekko-lightbox.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/image_hover_effect.css">
    <link href="<?php echo site_url(); ?>assets/css/toggle.css" rel="stylesheet">
    <script src="<?php  echo site_url();?>assets/js/sweetalert.min.js"></script>

    <style type="text/css">

        .table_business_info { 
      table-layout: fixed;
      width: 100%
    }

     .business_info {

        /* css-3 */
        white-space: -o-pre-wrap; 
        word-wrap: break-word;
        white-space: pre-wrap; 
        white-space: -moz-pre-wrap; 
        white-space: -pre-wrap; 

    }

    </style>
</head>

<body>

<?php include 'navbar.php'; ?>
<?php include 'sidebar.php'; ?>

   <!--  ALERT IF LOGO IS SUCCESFULLY UPDATED -->
<?php if ($this->session->flashdata('success')): ?>
    <script>
    Swal.fire({
    title: "Success!",
    text: "<?php echo $this->session->flashdata('success'); ?>",
    type: "success",
    timer: 8000,
    });
    </script>
<?php endif; ?>

<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-header">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <div class="d-inline">
                                    <h4>Business Profile</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class="breadcrumb-title">
                                    <?php include 'shortcut_links.php'; ?>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cover-profile">
                                <div class="profile-bg-img">
                                    <img class="profile-bg-img img-fluid" src="<?php echo base_url();?>assets/adminty/files/assets/images/user-profile/bg-img1.jpg" alt="bg-img">
                                    <div class="card-block user-info">
                                        <div class="col-md-12">
                                            <div class="media-left">
                                                <a href="#" class="profile-image md-trigger" data-modal="modal-13" data-toggle="modal" data-target="#mymodal">
                                                    <?php if( $result_business->display_image !=''){?>
                                                    <img width="750px" height="145px" src="<?php echo base_url();?>images/business/<?php echo $result_business->display_image; ?>" alt="user-img">
                                                </a>
                                                <?php } else{?>
                                                <img width="750px" height="145px" src="<?php echo base_url();?>images/business/default.png" alt="user-img">
                                                <?php } ?>
                                            </div>
                                            <div class="media-body row">
                                                <div class="col-lg-12">
                                                    <div class="user-title">
                                                        <h2><?php echo $result_business->business_name;  ?></h2>
                                                        <span class="text-white"><?php echo $result_business->business_type; ?> - <?php echo $result_business->category; ?></span>
                                                    </div>
                                                </div>
                                                <div></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="tab-header card">
                                <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Business Info</a>
                                    <div class="slide"></div>
                                    </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#gallery" role="tab">Businesss Gallery</a>
                                    <div class="slide"></div>
                                    </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#review" role="tab">Reviews</a>
                                    <div class="slide"></div>
                                    </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#owner_info" role="tab">Owner Info</a>
                                    <div class="slide"></div>
                                    </li>
                                </ul>
                            </div>

                <!-- TAB FOR BUSINESS INFO -->
<div class="tab-content">
    <div class="tab-pane active" id="personal" role="tabpanel">
        <div class="card">
            <div class="card-header">
                <h5 class="card-header-text">About Business</h5>
                <button id="edit-btn" type="button" class="btn btn-sm btn-primary waves-effect waves-light f-right"> <i class="icofont icofont-edit"></i>
                </button>
            </div>
            <div class="card-block">
                <div class="view-info">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="general-info">
                                <div class="row">
                                    <div class="col-lg-12 col-xl-6">
                                        <div class="table-responsive">
                                            <table class="table table_business_info">
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">Business Name</th>
                                                        <td class="business_info"><?php echo $result_business->business_name;  ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Category</th>
                                                        <td class="business_info"><?php echo $result_business->business_type; ?> - <?php echo $result_business->category; ?></td>
                                                    </tr>                                                
                                                    <tr>
                                                        <th scope="row">Cellphone Number</th>
                                                        <td class="business_info"><?php echo $result_business->phone_number; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Telephone Number</th>
                                                        <td class="business_info"><?php echo $result_business->tel_number; ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                    </div>
                                </div>
                                    <div class="col-lg-12 col-xl-6">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <th scope="row" >Date Created</th>
                                                        <td class="business_info"><?php echo $result_business->date_created; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Email</th>
                                                        <td class="business_info"><?php echo $result_business->email; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Subscription</th>
                                                        <td class="business_info"><?php echo $result_business->subscription; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Address</th>
                                                        <td class="business_info"><?php echo $result_business->business_address; ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                            <!-- TAB FOR BUSINESS INFO (EDIT) -->
                <div class="edit-info">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="general-info">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <form  method="post"  action="<?php  echo site_url('Business/edit_business');?>" enctype="multipart/form-data">
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="input-group"> <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                                            <input type="text" class="form-control" placeholder="Business Name" value="<?php echo $result_business->business_name;  ?>" name="business_name">
                                                            <input type="hidden" name="business_id" value="<?php echo $result_business->business_id;  ?>">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="input-group"> <span class="input-group-addon"><i class="icofont icofont-mobile-phone"></i></span>
                                                            <input type="text" class="form-control" placeholder="Mobile Number" value="<?php echo $result_business->phone_number;  ?>" name="phone_number" required>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="input-group"> <span class="input-group-addon"><i class="icofont icofont-telephone"></i></span>
                                                            <input type="text" class="form-control" placeholder="Telephone Number" value="<?php echo $result_business->tel_number;  ?>" name="tel_number">
                                                            </div>
                                                        </td>
                                                    </tr> 
                                                </tbody>
                                            </table>
                                    </div>

                                    <div class="col-lg-6">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="input-group"> <span class="input-group-addon"><i class="icofont icofont-calendar"></i></span>
                                                        <input id="dropper-default" class="form-control" type="text" placeholder="Select Date Business Created" value="<?php echo $result_business->date_created;  ?>" name="date_created"/ required>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="input-group"> <span class="input-group-addon"><i class="icofont icofont-email"></i></span>
                                                        <input type="text" class="form-control" placeholder="Email" value="<?php echo $result_business->email;  ?>" name="email" required>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="input-group"> <span class="input-group-addon"><i class="icofont icofont-location-pin"></i></span>
                                                        <input type="text" class="form-control" placeholder="Address" value="<?php echo $result_business->business_address;  ?>" name="business_address" required>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light m-r-20">Save</button>
                                    <a href="#!" id="edit-cancel" class="btn btn-default waves-effect">Cancel</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Description About us</h5>
                        <button id="edit-info-btn" type="button" class="btn btn-sm btn-primary waves-effect waves-light f-right"> <i class="icofont icofont-edit"></i>
                        </button>
                    </div>
                    <div class="card-block user-desc">
                        <div class="view-desc">
                            <p><?php echo $result_business->description;  ?></p>
                        </div>
                        <form  method="post"  action="<?php  echo site_url('Business/edit_business_description');?>" enctype="multipart/form-data">
                            <div class="edit-desc">
                                <div class="col-md-12">
                                    <input type="hidden" name="business_id" id="business_id" value="<?php echo $result_business->business_id;  ?>">
                                    <textarea id="description" name="description">
                                    <p><?php echo $result_business->description;  ?></p>
                                    </textarea>
                                </div>
                                <div class="text-center"> <button type="submit" class="btn btn-primary waves-effect waves-light m-r-20">Save</button>
                                    <a href="#!" id="edit-cancel-btn" class="btn btn-default waves-effect m-t-20">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Business Settings</h5>
                    </div>
                    <div class="card-block">

                            <div class="row">
                                    <div class="col-lg-12 col-xl-6">
                                        <div class="table-responsive">
                                            <table class="table table_business_info">
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">Pick-up Service</th>
                                                        <td class="">
                                                            <div id="offers_pick_up">
                                                            <label class="switch">
                                                            <input type="checkbox" id="pick_up" data-value="pick_up" value="enabled" onChange="enable_service(this);" <?php echo ($result_setting->pick_up == 'enabled') ?  "checked" :  "" ?>>
                                                            <span class="slider round"></span>
                                                            </label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Dine-in Reservation</th>
                                                        <td class="">
                                                            <div id="offers_dine_in">
                                                            <label class="switch">
                                                            <input type="checkbox" id="dine_in" data-value="dine_in" value="enabled" onChange="enable_service(this);" <?php echo ($result_setting->dine_in == 'enabled') ?  "checked" :  "" ?>>
                                                            <span class="slider round"></span>
                                                            </label>    
                                                            </div>  
                                                        </td>
                                                    </tr>                                                
                                                </tbody>
                                            </table>
                                    </div>
                                </div>
                                    <div class="col-lg-12 col-xl-6">
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">Delivery Service</th>
                                                        <td class="">
                                                            <div id="offers_delivery">
                                                            <label class="switch">
                                                            <input type="checkbox" id="delivery_confirmation" <?php echo ($result_setting->delivery != 'disabled') ?  "checked" :  "" ?>>
                                                            <span class="slider round"></span>
                                                            </label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        <div class="row" id="delivery_option">
                                           <div class="col-xl-6">
                                            <h6>Paid Delivery <label class="switch">
                                                    <input type="radio" id="default_delivery" name="delivery" data-value="delivery" value="Paid" onChange="enable_service(this);" checked
                                                      <?php echo ($result_setting->delivery == 'Paid') ?  "checked" :  "" ?> >
                                                     
                                                    <span class="slider round"></span>
                                                </label>
                                            </h6>
                                           </div> 
                                           <div class="col-xl-6">
                                            <h6>free Delivery <label class="switch">
                                                    <input type="radio" name="delivery" value="Free" data-value="delivery" onChange="enable_service(this);" 
                                                     <?php echo ($result_setting->delivery == 'Free') ?  "checked" :  "" ?>>
                                                    <span class="slider round"></span>
                                                </label>
                                            </h6>
                                           </div>

                                        </div>
                                    </div>
                                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- END FOR BUSINESS INFO (EDIT)-->

<!-- END FOR BUSINESS INFO -->


<!-- TAB FOR BUSINESS GALLERY -->
<div class="tab-pane" id="gallery" role="tabpanel">
    <div class="card">
        <div class="card-header">
            <h5>Gallery</h5>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#upload_image">Add Image</button>
        </div>
        <div class="md-float-material card-block">
            <div class="card-block">
                <div class="container" style="min-height: 30vh;">
                    
                        
                        <!-- EMAGE GALEERY -->
                        <?php if(!empty($result_image)){ ?>
                        <div class="row text-center text-lg-left">
                        <?php foreach($result_image as $row) { ?>
                        <div class="col-lg-3 col-md-4 col-6">
                            <div class="hovereffect" >
                                <img src="<?php echo base_url();?>images/business/<?php echo $row->image_name; ?>"  alt="user-img" class="img-thumbnail" width="400px" height="300px">
                                <div class="overlay">
                                    <h2><?php echo $row->upload_date; ?></h2>
                                    <a class="info" href="<?php echo base_url();?>images/business/<?php echo $row->image_name; ?>" data-toggle="lightbox" data-gallery="gallery">View</a>
                                    <a class="info delete_image" href="<?php echo base_url();?>business/delete_image/<?php echo $row->image_id; ?>/" >Delete</a>
                                </div>business
                            </div>
                        </div>
                        <?php } ?>
                        </div>
                        <?php } else{ ?>

                        <h1 style="text-align: center;" class="text-muted">---Please upload business image ---</h1>
                        <?php } ?>           
                </div>
            </div>
        </div>
    </div>
</div>

<!-- END FOR BUSINESS GALLERY -->


<!-- TAB FOR BUSINESS REVIEW -->
<div class="tab-pane" id="review" role="tabpanel">
   <div class="card">
      <div class="card-header">
         <h5 class="card-header-text">Review</h5>
      </div>
      <div class="card-block">
         <ul class="media-list">
            <li class="media">
               <div class="media-left"> <a href="#">
                  <img class="media-object img-radius comment-img" src="<?php echo base_url();?>assets/adminty/files/assets/images/avatar-1.jpg" alt="Generic placeholder image">
                  </a>
               </div>
               <div class="media-body">
                  <h6 class="media-heading">Sortino media<span class="f-12 text-muted m-l-5">Just now</span></h6>
                  <div class="stars-example-css review-star"> <i class="icofont icofont-star"></i>
                     <i class="icofont icofont-star"></i>
                     <i class="icofont icofont-star"></i>
                     <i class="icofont icofont-star"></i>
                     <i class="icofont icofont-star"></i>
                  </div>
                  <p class="m-b-0">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
                  <div class="m-b-25"> <span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span>
                  </div>
                  <hr>
                  <div class="media mt-2">
                     <a class="media-left" href="#">
                     <img class="media-object img-radius comment-img" src="<?php echo base_url();?>assets/adminty/files/assets/images/avatar-2.jpg" alt="Generic placeholder image">
                     </a>
                     <div class="media-body">
                        <h6 class="media-heading">Larry heading <span class="f-12 text-muted m-l-5">Just now</span></h6>
                        <div class="stars-example-css review-star"> <i class="icofont icofont-star"></i>
                           <i class="icofont icofont-star"></i>
                           <i class="icofont icofont-star"></i>
                           <i class="icofont icofont-star"></i>
                           <i class="icofont icofont-star"></i>
                        </div>
                        <p class="m-b-0">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
                        <div class="m-b-25"> <span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span>
                        </div>
                        <hr>
                        <div class="media mt-2">
                           <div class="media-left"> <a href="#">
                              <img class="media-object img-radius comment-img" src="<?php echo base_url();?>assets/adminty/files/assets/images/avatar-3.jpg" alt="Generic placeholder image">
                              </a>
                           </div>
                           <div class="media-body">
                              <h6 class="media-heading">Colleen Hurst <span class="f-12 text-muted m-l-5">Just now</span></h6>
                              <div class="stars-example-css review-star"> <i class="icofont icofont-star"></i>
                                 <i class="icofont icofont-star"></i>
                                 <i class="icofont icofont-star"></i>
                                 <i class="icofont icofont-star"></i>
                                 <i class="icofont icofont-star"></i>
                              </div>
                              <p class="m-b-0">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
                              <div class="m-b-25"> <span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span>
                              </div>
                           </div>
                           <hr>
                        </div>
                     </div>
                  </div>
                  <div class="media mt-2">
                     <div class="media-left"> <a href="#">
                        <img class="media-object img-radius comment-img" src="<?php echo base_url();?>assets/adminty/files/assets/images/avatar-1.jpg" alt="Generic placeholder image">
                        </a>
                     </div>
                     <div class="media-body">
                        <h6 class="media-heading">Cedric Kelly<span class="f-12 text-muted m-l-5">Just now</span></h6>
                        <div class="stars-example-css review-star"> <i class="icofont icofont-star"></i>
                           <i class="icofont icofont-star"></i>
                           <i class="icofont icofont-star"></i>
                           <i class="icofont icofont-star"></i>
                           <i class="icofont icofont-star"></i>
                        </div>
                        <p class="m-b-0">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
                        <div class="m-b-25"> <span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span></div>
                        <hr>
                     </div>
                  </div>
                  <div class="media mt-2">
                     <a class="media-left" href="#">
                     <img class="media-object img-radius comment-img" src="<?php echo base_url();?>assets/adminty/files/assets/images/avatar-4.jpg" alt="Generic placeholder image">
                     </a>
                     <div class="media-body">
                        <h6 class="media-heading">Larry heading <span class="f-12 text-muted m-l-5">Just now</span></h6>
                        <div class="stars-example-css review-star"> <i class="icofont icofont-star"></i>
                           <i class="icofont icofont-star"></i>
                           <i class="icofont icofont-star"></i>
                           <i class="icofont icofont-star"></i>
                           <i class="icofont icofont-star"></i>
                        </div>
                        <p class="m-b-0">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
                        <div class="m-b-25"> <span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span></div>
                        <hr>
                        <div class="media mt-2">
                           <div class="media-left"> <a href="#">
                              <img class="media-object img-radius comment-img" src="<?php echo base_url();?>assets/adminty/files/assets/images/avatar-3.jpg" alt="Generic placeholder image">
                              </a>
                           </div>
                           <div class="media-body">
                              <h6 class="media-heading">Colleen Hurst <span class="f-12 text-muted m-l-5">Just now</span></h6>
                              <div class="stars-example-css review-star"> <i class="icofont icofont-star"></i>
                                 <i class="icofont icofont-star"></i>
                                 <i class="icofont icofont-star"></i>
                                 <i class="icofont icofont-star"></i>
                                 <i class="icofont icofont-star"></i>
                              </div>
                              <p class="m-b-0">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
                              <div class="m-b-25"> <span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span></div>
                           </div>
                           <hr>
                        </div>
                     </div>
                  </div>
                  <div class="media mt-2">
                     <div class="media-left"> <a href="#">
                        <img class="media-object img-radius comment-img" src="<?php echo base_url();?>assets/adminty/files/assets/images/avatar-2.jpg" alt="Generic placeholder image">
                        </a>
                     </div>
                     <div class="media-body">
                        <h6 class="media-heading">Mark Doe<span class="f-12 text-muted m-l-5">Just now</span></h6>
                        <div class="stars-example-css review-star"> <i class="icofont icofont-star"></i>
                           <i class="icofont icofont-star"></i>
                           <i class="icofont icofont-star"></i>
                           <i class="icofont icofont-star"></i>
                           <i class="icofont icofont-star"></i>
                        </div>
                        <p class="m-b-0">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
                        <div class="m-b-25"> <span><a href="#!" class="m-r-10 f-12">Reply</a></span><span><a href="#!" class="f-12">Edit</a> </span></div>
                        <hr>
                     </div>
                  </div>
               </div>
            </li>
         </ul>
         <div class="input-group">
            <input type="text" class="form-control" placeholder="Right addon"> <span class="input-group-addon"><i class="icofont icofont-send-mail"></i></span>
         </div>
      </div>
   </div>
</div>
<!-- END FOR BUSINESS REVIEW -->

<div class="tab-pane" id="owner_info" role="tabpanel">
        <div class="card">
            <div class="card-header">
                <h5 class="card-header-text">Owner Info</h5>
                <button id="edit-btn-merchant" type="button" class="btn btn-sm btn-primary waves-effect waves-light f-right"> <i id="edit-icon-merchant" class="icofont icofont-edit"></i><i id="close-icon-merchant" class="icofont icofont-close"></i>
                </button>
            </div>
            <div class="card-block">
                <div class="view-info-merchant">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="general-info">
                                <div class="row">
                                    <div class="col-lg-12 col-xl-3">
                                        <?php if ($result_merchant->profile_image !=''){ ?>

                                        <img class="img-fluid" src="<?php echo base_url();?>images/merchant/<?php echo $result_merchant->profile_image; ?>" >

                                        <?php }else{ ?>

                                        <img class="img-fluid" src="<?php echo base_url();?>images/merchant/default-profile.png" >

                                        <?php } ?>

                                    </div>
                                    <div class="col-lg-12 col-xl-5">
                                        <div class="table-responsive">
                                            <table class="table table_business_info">
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">First Name</th>
                                                        <td class="business_info"><?php echo $result_merchant->first_name;  ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Middle Name</th>
                                                        <td class="business_info"><?php echo $result_merchant->middle_name; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Last Name</th>
                                                        <td class="business_info"><?php echo $result_merchant->surname; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Gender</th>
                                                        <td class="business_info"><?php echo $result_merchant->gender; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Birthdate</th>
                                                        <td class="business_info"><?php echo $result_merchant->birthdate; ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-xl-4">
                                        <div class="table-responsive">
                                            <table class="table table_business_info">
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">Address</th>
                                                        <td class="business_info"><?php echo $result_merchant->address; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Telephone Number</th>
                                                        <td class="business_info"><?php echo $result_merchant->tel_number; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Cellphone Number</th>
                                                        <td class="business_info"><?php echo $result_merchant->phone_number; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Email</th>
                                                        <td class="business_info"><?php echo $result_merchant->email; ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                            <!-- TAB FOR OWNER INFO (EDIT) -->
                <div class="edit-info-merchant">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="general-info">
                                 <form  method="post"  action="<?php  echo site_url('Business/edit_merchant');?>" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-3">

                            <?php if($result_merchant->profile_image !=''){ ?>

                            <img id="image-field-merchant" class="img-fluid" accept ="image/jpg,image/png/,image/jpeg" src="<?php  echo site_url();?>images/merchant/<?php echo $result_merchant->profile_image;  ?>">

                            <?php } else{ ?>

                            <span class="text-muted text-center mt-3" id="no_image_merchant">No image.</span>
                            <img alt="No Image" id="image-field-merchant" class="img-fluid" accept ="image/jpg,image/png/,image/jpeg" src="<?php  echo site_url();?>images/merchant/<?php echo $result_merchant->profile_image;  ?>" >

                            <?php } ?>

                            <input type="hidden" id="old_display_image_merchant" name="old_display_image" value="<?php echo $result_merchant->profile_image;  ?>" >
                            <input type="hidden" id="username" name="name" value="<?php echo $result_merchant->username;  ?>" >
                            <input type="hidden" id="image_path" name="image_path" value="merchant" >
              
                            <div id="outer" style="margin: auto;">
                                <input type="file" id="display_image_merchant" name="display_image" class="custom-file-input" onchange= "previewImageMerchant(this)" >
                                <label class="input-group-addon btn btn-primary" for="display_image_merchant">Choose Image</label>
                            </div>
                                
                                    </div>

                                    <div class="col-lg-5">
                                        <form  method="post"  action="<?php  echo site_url('Business/edit_business');?>" enctype="multipart/form-data">
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <div class="input-group"> <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                                        <input type="text" class="form-control" placeholder="First Name" value="<?php echo $result_merchant->first_name;  ?>" name="first_name">
                                                        <input type="hidden" name="merchant_id" value="<?php echo $result_merchant->merchant_id;  ?>">
                                                        </div>
                                                    </tr>
                                                    <tr>
                                                        <div class="input-group"> <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                                        <input type="text" class="form-control" placeholder="Middle Name" value="<?php echo $result_merchant->middle_name;  ?>" name="middle_name">
                                                    </tr>
                                                    <tr>    
                                                        <div class="input-group"> <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                                        <input type="text" class="form-control" placeholder="Last Name" value="<?php echo $result_merchant->surname;  ?>" name="surname" required>
                                                        </div>                           
                                                    </tr>
                                                    <tr>                                                        
                                                        <div class="input-group"> <span class="input-group-addon"><i class="icofont icofont-user-alt-1"></i></span>
                                                        <select class="form-control custom-select important" name="gender">
                                                            <option value="Male" <?php if ($result_merchant->gender == "Male") echo "selected" ?>>Male</option>
                                                            <option value="Female" <?php if ($result_merchant->gender == "Female") echo "selected" ?>>Female</option>
                                                        </select>                                        
                                                    </tr>
                                                    <tr>
                                                       <div class="input-group"> <span class="input-group-addon"><i class="icofont icofont-birthday-cake"></i></span>
                                                        <input id="dropper-default" class="form-control" type="text" placeholder="Select Date Business Created" value="<?php echo $result_merchant->birthdate;  ?>" name="birthdate"/ required>

                                                    </tr> 
                                                </tbody>
                                            </table>
                                    </div>

                                    <div class="col-lg-4">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <div class="input-group"> <span class="input-group-addon"><i class="icofont icofont-location-pin"></i></span>
                                                    <input type="text" class="form-control" placeholder="Address" value="<?php echo $result_merchant->address;  ?>" name="address" required>
                                                    </div>     
                                                </tr>
                                                <tr>
                                                    <div class="input-group"> <span class="input-group-addon"><i class="icofont icofont-telephone"></i></span>
                                                    <input type="text" class="form-control" placeholder="Telephone Number" value="<?php echo $result_merchant->tel_number;  ?>" name="tel_number" required>
                                                    </div>      
                                                </tr>
                                                <tr>
                                                    <div class="input-group"> <span class="input-group-addon"><i class="icofont icofont-mobile-phone"></i></span>
                                                    <input type="text" class="form-control" placeholder="Cellphone Number" value="<?php echo $result_merchant->phone_number;  ?>" name="phone_number" required>
                                                    </div>   
                                                </tr>
                                                <tr>
                                                    <div class="input-group"> <span class="input-group-addon"><i class="icofont icofont-email"></i></span>
                                                    <input type="text" class="form-control" placeholder="Email" value="<?php echo $result_merchant->email;  ?>" name="email" required>
                                                    </div> 
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light m-r-20">Save</button>
                                    <a href="#!" id="edit-cancel-merchant" class="btn btn-default waves-effect" >Cancel</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
<!-- END FOR BUSINESS INFO (EDIT)-->

</div>
<!-- END FOR BUSINESS INFO -->


<!-- MODAL FOR UPLOAD IMAGE -->
<div class="modal fade" id="upload_image">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3>add image</h3>
            </div>
            <form  method="post"  action="<?php  echo site_url('Business/add_image');?>" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body text-center " >
                            <span class="text-muted text-center mt-5" id="no_image">No image.</span>
                            <img alt="No Image" id="image-field-gallery" width="400px" height="300px" class="responsive" accept = "image/jpg,image/png/,image/jpeg"  >
                            
                        </div>
                    </div>
                    <div class="input-group">
                        <div class="mx-auto input-group " >
                            <div id="outer" style="margin: auto;">
                                <input type="file" id="display_image_gallery" name="display_image" class="custom-file-input" onchange= "previewImageGallery(this)" >
                                <input type="hidden" id="image_path" name="image_path" value="business" >
                                <input type="text" name="type" value="business" hidden>
                                <input type="text" name="upload_date" value="<?php  echo date('m-d-Y');   ?>" hidden>
                                <input type="text" name="name" value="<?php  echo $result_business->business_name;   ?>" hidden>
                                <input type="text" name="business_id" value="<?php  echo $result_business->business_id;   ?>" hidden>
                                <label class="input-group-addon btn btn-primary" for="display_image_gallery">Choose Image</label>
                            </div>
                        </div>
                    </div>
                    <div class= "modal-footer">
                        <button type="submit" id="add-business-btn" class="btn btn-success" >Upload</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </form>
                </div>
            </div>
        </div>
<!-- END FOR MODAL UPLOAD IMAGE -->

</div>
</div>
</div>
</div>


<!-- Modal for update business logo -->
<div class="modal fade" id="mymodal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Update Logo</h3>
            </div>
            <form  method="post"  action="<?php  echo site_url('Business/edit_logo');?>" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body text-center " >
                            <?php if($result_business->display_image !='')                            
                            {?>
                            <img id="image-field" class="img-fluid" accept ="image/jpg,image/png/,image/jpeg" src="<?php  echo site_url();?>images/business/<?php echo $result_business->display_image;  ?>">
                            <?php } else{ ?>
                            <span class="text-muted text-center mt-3" id="no_image">No image.</span>
                            <img alt="No Image" id="image-field" class="img-fluid" accept ="image/jpg,image/png/,image/jpeg" src="<?php  echo site_url();?>images/business/<?php echo $result_business->display_image;  ?>" >
                            <?php } ?>
                        </div>

                        <input type="hidden" id="old_display_image_logo" name="old_display_image" value="<?php echo $result_business->display_image;  ?>" >
                        <input type="hidden" id="business_name" name="name" value="<?php echo $result_business->business_name;  ?>" >
                        <input type="hidden" id="business_id" name="business_id" value="<?php echo $result_business->business_id;  ?>" >
                        <input type="hidden" id="image_path" name="image_path" value="business" >
                        <div class="input-group">
                            <div class="mx-auto input-group " >
                                <div id="outer" style="margin: auto;">
                                    <input type="file" id="display_image" name="display_image" class="custom-file-input" onchange= "previewImage(this)" >
                                    <label class="input-group-addon btn btn-primary" for="display_image">Choose Image</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class= "modal-footer">
                    <button type="submit" id="add-business-btn" class="btn btn-success" >Update</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </form>
        </div>
    </div>
</div>
<!-- END FOR MODAL UPDATE LOGO -->





    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/modernizr/js/css-scrollbars.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/assets/pages/advance-elements/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/assets/pages/advance-elements/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/bootstrap-daterangepicker/js/daterangepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/datedropper/js/datedropper.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/chart/echarts/js/echarts-all.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next/js/i18next.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/user-profile.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/js/pcoded.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/js/vartical-layout.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/assets/js/script.js"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>

    <script> var site_url = '<?php echo site_url(); ?>';  </script>

    <script src="<?php  echo site_url();?>assets/js/ekko-lightbox.min.js"></script> 
     
    <script src="<?php  echo site_url();?>assets/js/sweetalert.min.js"></script>

    <script src="<?php  echo site_url();?>assets/js/adminty_design_code.js"></script>

    <script src="<?php  echo site_url();?>assets/js/check_image.js"></script>

    <script src="<?php  echo site_url();?>assets/js/check_image_for_gallery.js"></script>

    <script src="<?php  echo site_url();?>assets/js/check_image_for_merchant.js"></script>

    <script src="<?php  echo site_url();?>assets/js/display_image_hide.js"></script>

    <script src="<?php  echo site_url();?>assets/js/show_image_lightbox.js"></script>

    <script src="<?php  echo site_url();?>assets/js/delete_image.js"></script>

    <script src="<?php  echo site_url();?>assets/js/sidebar.js"></script>

    <script type="text/javascript">

//--- THIS IS FOR OWNER INFO EDIT (FOR TOGGLING)---->
$('.edit-info-merchant, #close-icon-merchant').hide();
$("#delivery_option").toggle();

$('#edit-btn-merchant').click(function() {

    $('#close-icon-merchant, #edit-icon-merchant').toggle();
    $('.view-info-merchant, .edit-info-merchant').toggle();
});

$('#edit-cancel-merchant').click(function() {

    $('#close-icon-merchant, #edit-icon-merchant').toggle();

    $('.view-info-merchant, .edit-info-merchant').toggle();
});
//------- END HERE ------------------->

if ($("#delivery_confirmation").is(':checked')) {
    $("#delivery_option").toggle();
}


var sweet_alert_title = "Are you sure to disable";
var sweet_alert_text = "The customer can't use this service.";
var sweet_alert_confirm = "Disable";

function enable_service(input) {

    var element = $(input);
    var service = element.attr("data-value");

    if (element.is(':checked')) {

        var status = element.val();
        update_setting(service, status);

    } else {
        Swal.fire({
            title: sweet_alert_title,
            text: sweet_alert_text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: sweet_alert_confirm
        }).then((result) => {

            if (result.value) {
                element.prop("checked", false);
                var status = 'disabled';
                update_setting(service, status);
            } else {
                element.prop("checked", true);
                var status = element.val();
                update_setting(service, status);
            }

        });
    }

}


$("#delivery_confirmation").change(function() {

    var service = "delivery";

    if (this.checked) {

        $("#default_delivery").attr("checked", true);
        var status = $("#default_delivery").val();
        $("#delivery_option").slideToggle();
        update_setting(service, status);

    } else {

        Swal.fire({
            title: sweet_alert_title,
            text: sweet_alert_text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: sweet_alert_confirm
        }).then((result) => {
            if (result.value) {

                var status = 'disabled';
                $("#delivery_option").slideToggle();
                update_setting(service, status);

            } else {
 
                $('#delivery_confirmation').prop("checked", true);
                var status = $("#default_delivery").val();
                update_setting(service, status);

                console.log(test);

            }

        });

    }

});



function update_setting(service,status){
    $.ajax({
        url: site_url + "Business/edit_setting",
        type: "POST",
        data: {
            'service': service,
            'status': status,
        },
        dataType: 'json',
        success: function(data) { console.log(data);},
        error: function(data) { console.log(data);}
    });
}


    </script>


</body>

</html>