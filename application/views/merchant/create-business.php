<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Turistara</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="<?php echo site_url(); ?>assets/img/icon.png" rel="icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="<?php echo site_url(); ?>assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="<?php echo site_url(); ?>assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo site_url(); ?>assets/lib/animate/animate.min.css" rel="stylesheet">
  <link href="<?php echo site_url(); ?>assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="<?php echo site_url(); ?>assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo site_url(); ?>assets/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

</head>

<body>

 <!--==========================
  Header
  ============================-->
  <header id="header">

    <div id="topbar">
      <div class="container">
        <div class="social-links">
          <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
          <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
          <a href="#" class="google"><i class="fa fa-google"></i></a>
          <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
        </div>
      </div>
    </div>

    <div class="container">

      <div class="logo float-left">
        <a href="#intro" class="scrollto"><img src="<?php echo site_url(); ?>assets/img/turistara/logo-dark.png" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li><a href="index.html">Home</a></li>
          <li><a href="signup.html">Sign up</a></li>
        </ul>
      </nav><!-- .main-nav -->
  
    </div>
  </header><!-- #header -->

    <section id="intro" class="clearfix">


<!--==========================
    Create Business
  ============================-->
  <section id="business" class="wow fadeInUp">
    <div class="business-info">
      <div class="card">
        <div class="business-container">

          <div id="map"></div>

        <div class="form-group">
          <input id="pac-input" type="text" class="form-control" placeholder="Search Location" data-rule="minlen:4"/>
          <div class="validation"></div>
        </div>

        <div class="row">

          <div class="col-lg-6">

            <div class="form">
              
              <h4>Business Info</h4>
              <form action="" method="post" role="form" class="contactForm">
                <div class="form-group">
                  <label>Business Name</label>
                  <input type="text" name="name" class="form-control" id="name" placeholder="Business Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validation"></div>
                </div>

                <div class="row">
                <div class="col-lg-6">
                <div class="form-group">
                  <label>City</label>
                  <input type="business" class="form-control" name="business" id="business" placeholder="City" data-rule="business" data-msg="Please enter a valid business" />
                <div class="validation"></div>
                </div>
                </div>

                <div class="col-lg-6">
                <div class="form-group">
                  <label>Region</label>
                  <input type="text" class="form-control" name="subject" id="subject" placeholder="Region" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                  <div class="validation"></div>
                </div>
                </div>
                </div>

                <div class="row">
                <div class="col-lg-6">
                <div class="form-group">
                  <label>Country</label>
                  <input type="business" class="form-control" name="business" id="business" placeholder="Country" data-rule="business" data-msg="Please enter a valid business" />
                <div class="validation"></div>
                </div>
                </div>

                <div class="col-lg-6">
                <div class="form-group">
                  <label>Business Type</label>
                  <select id="business">
                    <option value="#">Select Business Type</option>
                    <option value="saab">Admin</option>
                    <option value="opel">Business Owner</option>
                  </select>
                  <div class="validation"></div>
                </div>
                </div>
                </div>

                <div class="form-group">
                  <label>Business Address</label>
                  <input type="text" name="name" class="form-control" id="name" placeholder="Business Address" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validation"></div>
                </div>

                <div class="form-group">
                  <div id="img-upload">
                  <label>Upload Image</label>
                  <div class="input-group">
                      <span class="input-group-btn">
                          <span class="btn btn-default btn-file">
                              Browse… <input type="file" id="imgInp">
                          </span>
                      </span>
                      <input type="text" class="form-control" readonly>
                  </div>
                  <img id='img-upload'/>
                </div>
                </div>

                <div class="form-group">
                  <label>Phone Number</label>
                  <input type="text" name="name" class="form-control" id="name" placeholder="Phone Number" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validation"></div>
                </div>

                <div class="form-group">
                  <label>Telephone Number</label>
                  <input type="text" name="name" class="form-control" id="name" placeholder="Telephone Number" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validation"></div>
                </div>

                <div class="form-group">
                  <label>Business Email-address</label>
                  <input type="text" name="name" class="form-control" id="name" placeholder="Business Email-address" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validation"></div>
                </div>

              </form>
            </div>


          </div>

          <div class="col-lg-6">

            <div class="form">
              
              <h4>Owner Info</h4>

              <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label>First Name</label>
                  <input type="business" class="form-control" name="business" id="business" placeholder="John Doe" data-rule="business" data-msg="Please enter a valid business" />
                <div class="validation"></div>
                </div>
                </div>

                <div class="col-lg-6">
                <div class="form-group">
                  <label>Last Name</label>
                  <input type="text" class="form-control" name="subject" id="subject" placeholder="Donut" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                  <div class="validation"></div>
                </div>
              </div>
              </div>

              <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label>Gender</label>
                  <input type="business" class="form-control" name="business" id="business" placeholder="Gender" data-rule="business" data-msg="Please enter a valid business" />
                <div class="validation"></div>
                </div>
                </div>

                <div class="col-lg-6">
                <div class="form-group">
                  <label>Date of Birth</label>
                  <input type="text" class="form-control" name="subject" id="subject" placeholder="Date of Birth" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                  <div class="validation"></div>
                </div>
              </div>
              </div>

              <h4>Access</h4>        

              <form action="" method="post" role="form" class="contactForm">
                <div class="form-group">
                  <label>Email Address</label>
                  <input type="text" name="name" class="form-control" id="name" placeholder="Email Address" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validation"></div>
                </div>
                
              <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" class="form-control" name="business" id="business" placeholder="Password" data-rule="business" data-msg="Please enter a valid business" />
                <div class="validation"></div>
                </div>
                </div>

                <div class="col-lg-6">
                <div class="form-group">
                  <label>Confirm Password</label>
                  <input type="password" class="form-control" name="subject" id="subject" placeholder="Confirm Password" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                  <div class="validation"></div>
                </div>
              </div>
              </div>

              <h4>Subscription</h4>

                <div class="form-group">
                  <label>Plan Category</label>
                  <select id="business">
                    <option value="#">Select Subscription Plan</option>
                    <option value="saab">Admin</option>
                    <option value="opel">Business Owner</option>
                  </select>
                  <div class="validation"></div>
                </div>

                <div class="form-group">
                  <a class="btn" type="submit" value="Submit">Submit</a>
                </div>
              </form>
            </div>

          </div>

          

        </div> 
        </div>
      </div>
    </div>

  </section>
  </section>


</body>

  <!-- JavaScript Libraries -->
  <script src="<?php echo site_url(); ?>assets/lib/jquery/jquery.min.js"></script>
  <script src="<?php echo site_url(); ?>assets/lib/jquery/jquery-migrate.min.js"></script>
  <script src="<?php echo site_url(); ?>assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo site_url(); ?>assets/lib/easing/easing.min.js"></script>
  <script src="<?php echo site_url(); ?>assets/lib/mobile-nav/mobile-nav.js"></script>
  <script src="<?php echo site_url(); ?>assets/lib/wow/wow.min.js"></script>
  <script src="<?php echo site_url(); ?>assets/lib/waypoints/waypoints.min.js"></script>
  <script src="<?php echo site_url(); ?>assets/lib/counterup/counterup.min.js"></script>
  <script src="<?php echo site_url(); ?>assets/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="<?php echo site_url(); ?>assets/lib/isotope/isotope.pkgd.min.js"></script>
  <script src="<?php echo site_url(); ?>assets/lib/lightbox/js/lightbox.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="<?php echo site_url(); ?>assets/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="<?php echo site_url(); ?>assets/js/main.js"></script>

  <!-- Geo Location -->
  <script src="<?php echo site_url(); ?>assets/map/js/map.js"></script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDU5mKV4oCZcxKQfOka5Mz5LlcqS3eB2YU&libraries=places&signed_in=true&callback=initMap"></script>