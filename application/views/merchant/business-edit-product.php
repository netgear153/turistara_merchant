<!DOCTYPE html>
<html lang="en">

<head>
    <title>Edit Product</title>

    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <link rel="icon" href="<?php echo base_url();?>assets/img/turistara/icon.png" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-bar-rating/css/bars-1to10.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-bar-rating/css/bars-horizontal.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-bar-rating/css/bars-movie.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-bar-rating/css/bars-pill.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-bar-rating/css/bars-reversed.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-bar-rating/css/bars-square.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-bar-rating/css/css-stars.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/themify-icons/themify-icons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/icofont/css/icofont.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/feather/css/feather.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/sweetalert.css">
</head>

<body>

<?php include 'navbar.php'; ?>
<?php include 'sidebar.php'; ?>

<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-header">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <div class="d-inline">
                                <h4><?php echo $result_food->business_name;  ?></h4>
                                <span><?php echo $result_food->business_type; ?> - <?php echo $result_food->category;  ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class="breadcrumb-title">
                                    <?php include 'shortcut_links.php'; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title">Edit Product</h5>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="product-edit">
                                                <div class="tab-content">
                                                    <!-- <div class="tab-pane active" id="home7" role="tabpanel"> -->
                                                        <form method="post" class="md-float-material card-block"
                                                            action="<?php  echo site_url('Business/edit_food');?>" enctype="multipart/form-data">
                                                            <div class="row">
                                                                <div class="mx-auto card">
                                                                    <div class="card-body">
                                                                        <?php if($result_food->display_image !=''){ ?>

                                                                        <img alt="No Image" id="image-field" class="img-fluid" accept="image/jpg,image/png/,image/jpeg"src="<?php  echo site_url();?>images/food/<?php echo $result_food->display_image;  ?>">

                                                                        <?php } else{ ?>

                                                                        <span class="text-muted text-center mt-3" id="no_image">No image.</span>
                                                                        <img alt="No Image" id="image-field" class="img-fluid" accept="image/jpg,image/png/,image/jpeg"src="<?php  echo site_url();?>images/food/<?php echo $result_food->display_image;  ?>">

                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                                <div class="mx-auto input-group ">
                                                                    <div id="outer" style="margin: auto;">

                                                                       <!-- This is for upload image requirements    -->
                                                                        <input type="hidden" id="old_display_image" name="old_display_image"
                                                                            class="form-control pname" placeholder="Product Name"
                                                                            value="<?php echo $result_food->display_image; ?>">
                                                                        <input type="hidden" id="image_path" name="image_path" value="food">
                                                                        <input type="file" id="display_image" name="display_image" class="custom-file-input"
                                                                            onchange="previewImage(this)" hidden>
                                                                        <label class="input-group-addon btn btn-primary" for="display_image">Choose
                                                                            Image</label>
                                                                        <input type="hidden" id="food_id" name="food_id" value="<?php echo $result_food->food_id; ?>">
                                                                        <!-- Ends Here    -->

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon"><i class="icofont icofont-fast-food"></i></span>
                                                                                <input id="food_name" name="name" class="form-control pname"
                                                                                    placeholder="Product Name" value="<?php echo $result_food->food_name; ?>" required>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <div class="input-group">
                                                                                <span class="input-group-addon"><i
                                                                                        class="icofont icofont-cur-peso"></i></span>
                                                                                <input type="number" id="price" name="price" class="form-control pamount"
                                                                                    placeholder="Amount in Peso" step="0.01" value="<?php echo $result_food->price; ?>"
                                                                                    required>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <div class="input-group">
                                                                                <select name="stock" id="description" class="form-control form-control-primary"
                                                                                    required>
                                                                                    <option hidden>---- Select Stock----</option>

                                                                                    <option value="In Stock" <?php if($result_food->stock == "In Stock" ){
                                                                                    echo "selected";}
                                                                                    ?>>In Stock</option>

                                                                                    <option value="Out of Stock" <?php if($result_food->stock == "Out of Stock" ){
                                                                                    echo "selected";}
                                                                                    ?>>Out of Stock</option>

                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <textarea class="form-control station-important"
                                                                                    placeholder="Description of the Product" name="description" id="description"
                                                                                    rows="6" cols="50"><?php echo $result_food->description; ?></textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="text-center m-t-20">
                                                                        <button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Save
                                                                        </button>
                                                                        <button type="button" class="btn btn-warning waves-effect waves-light"
                                                                            onclick="discard_food();">Discard
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                 <!-- </div> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/popper.js/js/popper.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/modernizr/js/modernizr.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/modernizr/js/css-scrollbars.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-bar-rating/js/jquery.barrating.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/assets/pages/rating/rating.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next/js/i18next.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>
<script src="<?php echo base_url();?>assets/adminty/files/assets/js/pcoded.min.js"></script>
<script src="<?php echo base_url();?>assets/adminty/files/assets/js/vartical-layout.min.js"></script>
<script src="<?php echo base_url();?>assets/adminty/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/assets/js/script.js"></script>

<script src="<?php  echo site_url();?>assets/js/sweetalert.min.js"></script>

<script src="<?php  echo site_url();?>assets/js/check_image.js"></script>

<script src="<?php  echo site_url();?>assets/js/adminty_design_code.js"></script>

<script>

if(!$('#old_display_image').val()) {

    $('#image-field').hide();
}

</script>

<script>

var site_url = '<?php echo site_url(); ?>';

//-- Alerts when the user click descard button
function discard_food() {

    Swal.fire({
        title: 'Are you sure to discard?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, discard it!'
    }).then((result) => {
        if(result.value) {
            window.location = site_url + 'Business/foods';
        }
    });
}

</script>

<script src="<?php  echo site_url();?>assets/js/sidebar.js"></script>

</body>

</html>