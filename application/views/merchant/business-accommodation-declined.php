<!DOCTYPE html>
<html lang="en">

<head>
    <title>Transaction - Accommodation Declined </title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">

    <link rel="icon" href="<?php echo base_url();?>assets/img/turistara/icon.png" type="image/x-icon">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/themify-icons/themify-icons.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/icofont/css/icofont.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/icon/feather/css/feather.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/extensions/responsive/css/responsive.dataTables.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/adminty/files/assets/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/ekko-lightbox.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/sweetalert.css">

</head>

<body>

<?php include 'navbar.php'; ?> 
<?php include 'sidebar.php'; ?> 

<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-header">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <div class="d-inline">
                                    <?php foreach($result_accommodation_business as $row) { ?>
                                    <h4><?php echo $row->business_name;  ?></h4>
                                    <span><?php echo $row->description;  ?></span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class="breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="index.html"> <i class="feather icon-home"></i> </a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">Transactions</a></li>
                                    <li class="breadcrumb-item"><a href="#!">bookings</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page-body">
                    <div class="card">
                        <div class="card-block">
                            <?php include 'transaction_datatable.php'; ?> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- MODAL FOR VIEW RESERVATION -->
<div id="dataModal" class="modal fade">
    <div class="modal-dialog" style="
        position: relative;
        display: table;
        overflow-y: auto;
        overflow-x: auto;
        width: auto;
        min-width: 600px;   ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Transaction Details</h4>
            </div>
            <div class="modal-body" id="employee_detail">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title ">Customer Details</h5>
                        <br>
                        <div class="row">
                            <div class="col-lg-12 col-xl-6">
                                <div class="table-responsive">
                                    <table class="table table-sm">
                                        <tbody>
                                            <tr>
                                                <th scope="row">First Name</th>
                                                <td><span id="first_name"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">username</th>
                                                <td><span id="username"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">address</th>
                                                <td><span id="address"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-12 col-xl-6">
                                <div class="table-responsive">
                                    <table class="table w-auto">
                                        <tbody>
                                            <tr>
                                                <th scope="row">Last Name</th>
                                                <td><span id="last_name"></span></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Email</th>
                                                <td><span id="email"></span></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Contact Number</th>
                                                <td><span id="contact_number"></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title ">Reservation Details</h5>
                        <br>
                        <div class="row">
                            <div class="col-lg-12 col-xl-6">
                                <div class="table-responsive">
                                    <table class="table table-sm">
                                        <tbody>
                                            <tr>
                                                <th scope="row">Check In</th>
                                                <td><span id="check_in_date"></span></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Guests</th>
                                                <td><span id="guest"></span></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Nights</th>
                                                <td><span id="night"></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-12 col-xl-6">
                                <div class="table-responsive">
                                    <table class="table table-sm">
                                        <tbody>
                                            <tr>
                                                <th scope="row">Check Out</th>
                                                <td><span id="check_out_date"></span></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Adult</th>
                                                <td><span id="num_of_adult"></span></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Children</th>
                                                <td><span id="num_of_children"></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title ">Room Details</h5>
                        <br>
                        <div class="row">
                            <div class="col-lg-12 col-xl-6 text-center">
                                <img id="display_image" class="img-fluid text-center" alt="tbl" width="250px" height="250px">
                            </div>
                            <div class="col-lg-12 col-xl-6">
                                <div class="table-responsive">
                                    <table class="table table-sm">
                                        <tbody>
                                            <tr>
                                                <th scope="row">Room Type</th>
                                                <td><span id="room_type"></span></td>
                                            </tr>
                                            
                                            <tr>
                                                <th scope="row">Per Night</th>
                                                <td>&#8369;<span id="per_night"></span></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Max Guest</th>
                                                <td><span id="max_guest"></span></td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <br>
                        <h5>Description</h5>
                        <div class="row" style="margin: 5px;">
                        <textarea rows="5" cols="100%" wrap="hard" id="description"  style="border: none"> </textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/bootstrap/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/modernizr/js/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/modernizr/js/css-scrollbars.js"></script>

    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/js/jszip.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/js/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/extensions/responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next/js/i18next.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/bower_components/jquery-i18next/js/jquery-i18next.min.js"></script>

    <script src="<?php echo base_url();?>assets/adminty/files/assets/pages/data-table/extensions/responsive/js/responsive-custom.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/js/pcoded.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/js/vartical-layout.min.js"></script>
    <script src="<?php echo base_url();?>assets/adminty/files/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/adminty/files/assets/js/script.js"></script>

    <script src="<?php  echo site_url();?>assets/js/ekko-lightbox.min.js"></script> 

    <script src="<?php  echo site_url();?>assets/js/sweetalert.min.js"></script>

    <script src="<?php  echo site_url();?>assets/js/check_image.js"></script>

    <script src="<?php  echo site_url();?>assets/js/adminty_design_code.js"></script>

   
<script>

  var site_url = '<?php echo site_url(); ?>';

  //------- GLOBAL VARIABLE--------------//

  //----VARIABLE FOR DATATABLE-----//

  var transaction_type = "reservation";
  var business_type = "Accommodation";
  var status = "declined";

  //----VARIABLE FOR TRANSACTION-----//

  var global_transaction_id = "";

</script>


<!--  ----Global Transaction Table----- -->
<script src="<?php  echo site_url();?>assets/js/transaction_datable.js"></script>

<!--  ----Global Reservation Transaction Details----- -->
<script src="<?php  echo site_url();?>assets/js/view_reservation_transaction_details.js"></script>

<script src="<?php  echo site_url();?>assets/js/sidebar.js"></script>

</body>

</html>