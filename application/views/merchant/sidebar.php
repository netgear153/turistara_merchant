

<div id="sidebar" class="users p-chat-user showChat">
                <div class="had-container">
                    <div class="card card_main p-fixed users-main">
                        <div class="user-box">
                            <div class="chat-inner-header">
                                <div class="back_chatBox">
                                    <div class="right-icon-control">
                                        <input type="text" class="form-control  search-text" placeholder="Search Friend" id="search-friends">
                                        <div class="form-icon">
                                            <i class="icofont icofont-search"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="main-friend-list">
                                <div class="media userlist-box" data-id="1" data-status="online" data-username="Josephin Doe" data-toggle="tooltip" data-placement="left" title="Josephin Doe">
                                    <a class="media-left" href="#!">
<img class="media-object img-radius img-radius" src="<?php echo base_url();?>assets/adminty/files/assets/images/avatar-3.jpg" alt="Generic placeholder image ">
<div class="live-status bg-success"></div>
</a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Josephin Doe</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="2" data-status="online" data-username="Lary Doe" data-toggle="tooltip" data-placement="left" title="Lary Doe">
                                    <a class="media-left" href="#!">
<img class="media-object img-radius" src="<?php echo base_url();?>assets/adminty/files/assets/images/avatar-2.jpg" alt="Generic placeholder image">
<div class="live-status bg-success"></div>
</a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Lary Doe</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="3" data-status="online" data-username="Alice" data-toggle="tooltip" data-placement="left" title="Alice">
                                    <a class="media-left" href="#!">
<img class="media-object img-radius" src="<?php echo base_url();?>assets/adminty/files/assets/images/avatar-4.jpg" alt="Generic placeholder image">
<div class="live-status bg-success"></div>
</a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Alice</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="4" data-status="online" data-username="Alia" data-toggle="tooltip" data-placement="left" title="Alia">
                                    <a class="media-left" href="#!">
<img class="media-object img-radius" src="<?php echo base_url();?>assets/adminty/files/assets/images/avatar-3.jpg" alt="Generic placeholder image">
<div class="live-status bg-success"></div>
</a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Alia</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="5" data-status="online" data-username="Suzen" data-toggle="tooltip" data-placement="left" title="Suzen">
                                    <a class="media-left" href="#!">
<img class="media-object img-radius" src="<?php echo base_url();?>assets/adminty/files/assets/images/avatar-2.jpg" alt="Generic placeholder image">
<div class="live-status bg-success"></div>
</a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Suzen</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="showChat_inner">
                <div class="media chat-inner-header">
                    <a class="back_chatBox">
                        <i class="feather icon-chevron-left"></i> Josephin Doe
                    </a>
                </div>
                <div class="media chat-messages">
                    <a class="media-left photo-table" href="#!">
<img class="media-object img-radius img-radius m-t-5" src="<?php echo base_url();?>assets/adminty/files/assets/images/avatar-3.jpg" alt="Generic placeholder image">
</a>
                    <div class="media-body chat-menu-content">
                        <div class="">
                            <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                            <p class="chat-time">8:20 a.m.</p>
                        </div>
                    </div>
                </div>
                <div class="media chat-messages">
                    <div class="media-body chat-menu-reply">
                        <div class="">
                            <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                            <p class="chat-time">8:20 a.m.</p>
                        </div>
                    </div>
                    <div class="media-right photo-table">
                        <a href="#!">
<img class="media-object img-radius img-radius m-t-5" src="<?php echo base_url();?>assets/adminty/files/assets/images/avatar-4.jpg" alt="Generic placeholder image">
</a>
                    </div>
                </div>
                <div class="chat-reply-box p-b-20">
                    <div class="right-icon-control">
                        <input type="text" class="form-control search-text" placeholder="Share Your Thoughts">
                        <div class="form-icon">
                            <i class="feather icon-navigation"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <nav class="pcoded-navbar">
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="pcoded-navigatio-lavel">Restaurant</div>
                            <ul class="pcoded-item pcoded-left-item">

                                <li class="pcoded-hasmenu <?php if($hasmenu == 'home'){echo 'active pcoded-trigger';} ?>">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                                        <span class="pcoded-mtext">Home</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class="<?php if($submenu == 'dashboard'){echo 'active';} ?>" >
                                            <a href="<?php echo base_url();?>business/dashboard">
                                                <span class="pcoded-mtext">Dashboard</span>
                                            </a>
                                        </li>
                                        <li class="<?php if($submenu == 'profile'){echo 'active';} ?>">
                                            <a href="<?php echo base_url();?>business/profile">
                                                <span class="pcoded-mtext">Profile</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="pcoded-hasmenu <?php if($hasmenu == 'bookings'){echo 'active pcoded-trigger';} ?>" id="li_booking">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-credit-card"></i></span>
                                        <span class="pcoded-mtext">Bookings</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class="<?php if($submenu == 'booking'){echo 'active';} ?>" >
                                            <a href="<?php echo base_url();?>business/booking">
                                                <span class="pcoded-mtext">Transactions</span>
                                            </a>
                                        </li> 
                                    
                        
                                    </ul>
                                </li>

                                <li class="pcoded-hasmenu <?php if($hasmenu == 'accommodation_reservations'){echo 'active pcoded-trigger';} ?>" id="li_accomodation_reservation">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-credit-card"></i></span>
                                        <span class="pcoded-mtext">Reservation</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class="<?php if($submenu == 'reservation'){echo 'active';} ?>" >
                                            <a href="<?php echo base_url();?>business/accommodation_reservation">
                                                <span class="pcoded-mtext">Transactions</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="pcoded-submenu">
                                        <li class="<?php if($submenu == 'reserved'){echo 'active';} ?>" >
                                            <a href="<?php echo base_url();?>business/accommodation_reserved">
                                                <span class="pcoded-mtext">Reserved</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="pcoded-submenu">
                                        <li class="<?php if($submenu == 'declined'){echo 'active';} ?>" >
                                            <a href="<?php echo base_url();?>business/accommodation_declined">
                                                <span class="pcoded-mtext">Declined</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="pcoded-submenu">
                                        <li class="<?php if($submenu == 'completed'){echo 'active';} ?>" >
                                            <a href="<?php echo base_url();?>business/accommodation_reservation_completed">
                                                <span class="pcoded-mtext">Completed</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="pcoded-submenu">
                                        <li class="<?php if($submenu == 'expired'){echo 'active';} ?>" >
                                            <a href="<?php echo base_url();?>business/accommodation_expired">
                                                <span class="pcoded-mtext">Expired</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="pcoded-hasmenu <?php if($hasmenu == 'order'){echo 'active pcoded-trigger';} ?>"  id="li_order" >
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-credit-card"></i></span>
                                        <span class="pcoded-mtext">Delivery</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                    
                                        <li class="<?php if($submenu == 'transaction'){echo 'active';} ?>" id="li_transaction">
                                            <a href="<?php echo base_url();?>business/order">
                                                <span class="pcoded-mtext">Transactions</span>
                                            </a>
                                        </li>

                                        <li class="<?php if($submenu == 'pending'){echo 'active';} ?>" id="li_pending">
                                            <a href="<?php echo base_url();?>business/pending">
                                                <span class="pcoded-mtext">Pendings</span>
                                            </a>
                                        </li>

                                        <li class="<?php if($submenu == 'confirmed'){echo 'active';} ?>" id="li_confirmed">
                                            <a href="<?php echo base_url();?>business/confirmed">
                                                <span class="pcoded-mtext">Confirmed</span>
                                            </a>
                                        </li>

                                        <li class="<?php if($submenu == 'preparing'){echo 'active';} ?>" id="li_preparing">
                                            <a href="<?php echo base_url();?>business/preparing">
                                                <span class="pcoded-mtext">Preparing</span>
                                            </a>
                                        </li>

                                        <li class="<?php if($submenu == 'deliver'){echo 'active';} ?>" id="li_deliver">
                                            <a href="<?php echo base_url();?>business/deliver">
                                                <span class="pcoded-mtext">Deliver</span>
                                            </a>
                                        </li>

                                        <li class="<?php if($submenu == 'completed'){echo 'active';} ?>" id="li_deliver">
                                            <a href="<?php echo base_url();?>business/completed">
                                                <span class="pcoded-mtext">Completed</span>
                                            </a>
                                        </li>

                                        <li class="<?php if($submenu == 'cancelled'){echo 'active';} ?>" id="li_deliver">
                                            <a href="<?php echo base_url();?>business/cancelled">
                                                <span class="pcoded-mtext">Cancelled</span>
                                            </a>
                                        </li>
                                    
                                    
                                    </ul>
                                </li>

                                <li class="pcoded-hasmenu <?php if($hasmenu == 'products and services'){echo 'active pcoded-trigger';} ?>">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-box"></i></span>
                                        <span class="pcoded-mtext">Products and Services</span>
                                    </a>
                                    <ul class="pcoded-submenu">

                                        <li class="<?php if($submenu == 'rooms'){echo 'active';} ?>" id="li_rooms">
                                            <a href="<?php echo base_url();?>business/rooms">
                                                <span class="pcoded-mtext">Rooms</span>
                                            </a>
                                        </li>
       
                                        <li class="<?php if($submenu == 'foods'){echo 'active';} ?>" id="li_foods">
                                            <a href="<?php echo base_url();?>business/foods">
                                                <span class="pcoded-mtext">Foods</span>
                                            </a>
                                        </li>
                          
                                    </ul>
                                </li>

                                <li class="">
                                    <a href="<?php echo base_url();?>business/history">
                                        <span class="pcoded-micon"><i class="feather icon-clock"></i></span>
                                        <span class="pcoded-mtext">History</span>
                                    </a>
                                </li>

                                <li class="">
                                    <a href="<?php echo base_url();?>business/subscription">
                                        <span class="pcoded-micon"><i class="feather icon-star"></i></span>
                                        <span class="pcoded-mtext">Subscription</span>
                                    </a>
                                </li>
                        </div>
                    </nav>