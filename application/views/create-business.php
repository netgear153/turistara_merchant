<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Turistara</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicons -->
    <link href="<?php echo site_url(); ?>assets/img/icon.png" rel="icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="<?php echo site_url(); ?>assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="<?php echo site_url(); ?>assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/lib/animate/animate.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="<?php echo site_url(); ?>assets/css/multiform_style.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/multiform_style.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/css/toggle.css" rel="stylesheet">

    <style type="text/css">
      #map {
      height: 500px;
      width: 100%;
      }
    </style>
</head>

<body>

  <header id="header">
    <div id="topbar">
        <div class="container">
            <div class="social-links">
                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                <a href="#" class="google"><i class="fa fa-google"></i></a>
                <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="logo float-left">
            <a href="#intro" class="scrollto"><img src="<?php echo site_url(); ?>assets/img/turistara/logo-dark.png"
                    alt="" class="img-fluid"></a>
        </div>
        <nav class="main-nav float-right d-none d-lg-block">
            <ul>
                <li><a href="<?php echo base_url();?>Home/index">Home</a></li>
                <li><a href="<?php echo base_url();?>Signup/index">Sign up</a></li>
            </ul>
        </nav>
    </div>
</header>

<br>

<form id="regForm" method="post"  action="<?php  echo site_url('Plan/validate'); ?>" enctype="multipart/form-data">

<!-- TAB 1 -->
<div class="tab">
    <h3 class="card-title m-t-15 text-primary">Owner Info</h3>
    <div class="form">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>First Name</label>
                    <input type="business" class="input_validate form-control" name="first_name" id="first_name"
                        placeholder="" required />
                    <div class="validation"></div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Last Name</label>
                    <input type="text" class="input_validate form-control" name="last_name" id="last_name"
                        placeholder="" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject"
                        required />
                    <div class="validation"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="control-label">Gender</label>
                    <select class="form-control custom-select important" name="gender">
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                    </select>
                    <div class="validation"></div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Date of Birth</label>
                    <input type="date" class="input_validate form-control" name="birthdate" id="birthdate"
                        placeholder="" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject"
                        required />
                    <div class="validation"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Telephone Number</label>
                    <input type="business" class="input_validate form-control" name="owner_tel_number" id="tel_number"
                        placeholder="e.g (2288759)" required />
                    <div class="validation"></div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label>CellPhone Number</label>
                    <input type="text" class="input_validate form-control" name="owner_phone_number" id="phone_number"
                        placeholder="e.g (09783346112)" data-rule="minlen:4"
                        data-msg="Please enter at least 8 chars of subject" required />
                    <div class="validation"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Address</label>
                    <input type="text" class="input_validate form-control" name="owner_address" id="owner_address"
                        placeholder="John Doe" required />
                    <div class="validation"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- TAB 2 -->
<div class="tab">
    <h3 class="card-title m-t-15 text-primary">Please locate your business location</h3>
    <div id="map"></div>
    <br>
    <div class="form-group">
        <input id="pac-input" type="text" class="form-control" placeholder="Search Location" data-rule="minlen:4"
            oninput="this.className = 'valid'" />
          <span class="text-secondary text-center">If your business is not in the selection, just dragged the marker.</span>
        <input type="hidden" value="" id="longitude" name="longitude" class="input_validate important" required>
        <input type="hidden" value="" id="latitude" name="latitude" class="input_validate important" required>
    </div>
</div>

<!-- TAB 3 -->
<div class="tab">
    <h3 class="card-title m-t-15 text-primary">Business Info</h3>
    <div class="form">
        <div class="row">
            <div class="col-md-12 ">
                <div class="form-group">
                    <label>Business Name</label>
                    <input type="text" id="business_name" name="business_name"
                        class="input_validate form-control important" placeholder="Enter Business Name">
                    <span class="text-danger"> <?php echo form_error("business_name");   ?></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Cellphone Number</label>
                    <input type="text" id="business_phone" name="phone_number"
                        class="input_validate form-control important" placeholder="Phone Number">
                    <span class="text-danger"></span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Telephone Number</label>
                    <input type="text" id="business_telephone" name="tel_number"
                        class="input_validate form-control important" placeholder="Telephone Number">
                    <span class="text-danger"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Business Logo</label>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" id="display_image" name="display_image" class="custom-file-input"
                                    onchange="previewImage(this)">
                                <label class="custom-file-label" for="display_image">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body h-50">
                        <span class="text-muted text-center mt-5" id="no_image">No image.</span>
                        <img id="image-field" class="img-fluid" 
                            accept="image/jpg,image/png/,image/jpeg">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Business Type</label>
                    <select class="input_validate form-control custom-select important" name="category" id="category"
                        required>
                        <option value="" hidden>-- Select business type --</option>
                        <!-- <optgroup label="Accommodation " >
                            <option value="Accommodation-Inn" disabled>Inn</option>
                            <option value="Accommodation-Hotel" disabled>Hotel </option>
                            <option value="Accommodation-Hostel" disabled>Hostel </option>
                            <option value="Accommodation-Resort" disabled>Resort </option>
                            <option value="Pension House">Pension House</option>
                            <option value="Homestay">Homestay</option>
                        </optgroup> -->
                        <optgroup label="Foods & Restaurants">
                            <option value="Foods & Restaurants-Restaurant">Restaurant </option>
                            <option value="Foods & Restaurants-Pizza">Pizza</option>
                            <option value="Foods & Restaurants-Coffee Shop">Coffee Shop </option>
                            <option value="Foods & Restaurants-Cakes and Pastries">Cakes & Pastries</option>
                        </optgroup>
                        <!-- <optgroup label="Clubs & Bar">
                            <option value="Clubs & Bar-Club" disabled>Club</option>
                            <option value="Clubs & Bar-Bar" disabled>Bar</option>
                            <option value="Clubs & Bar-Restobar" disabled>Restobar</option>
                        </optgroup> -->
                        <!-- <optgroup label="Travel and Tours">
                            <option value="Travel and Tours-Car Rental" disabled>Car Rental</option>
                            <option value="Travel and Tours-Ticketing" disabled>Ticketing</option>
                        </optgroup> -->
                    </select>
                </div>
                <div class="form-group">
                    <label>Date Created your Business</label>
                    <input type="date" name="date_created" class="input_validate form-control important"
                        placeholder="Enter City" data-date="" data-date-format="DD MMMM YYYY">
                    <span class="text-danger"> </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Business Address</label>
                            <input type="text" id="address" name="business_address"
                                class="input_validate form-control important" placeholder="Busines Address">
                            <span class="text-danger"> </span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Business Email-address</label>
                            <input type="email" id="business_email" name="email"
                                class="input_validate form-control important" placeholder="Busines Email-address">
                            <span class="text-danger"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ">
            <div class="form-group">
                <label>Description</label>
                <textarea class="input_validate form-control important" placeholder="Enter Description"
                    name="description" id="description" rows="4" cols="50"></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Plan Category</label>
                <select class="input_validate form-control custom-select important" name="subscription"
                    required>
                    <option value="listing" selected>Listing (Free)</option>
                </select>
            </div>

        </div>   
    </div>
    <div class="row">
        <div class="col-md-6">
            <div id="offers_pick_up">
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="form-group">
                            <h5>Does your business offers pick-up service? <label class="switch">
                                    <input type="hidden" name="pick_up" value="disabled" />
                                    <input type="checkbox" name="pick_up" value="enabled">
                                    <span class="slider round"></span>
                                </label>
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
            <div id="offers_dine_in">
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="form-group">
                            <h5>Does your business offers dine-in reservation? <label class="switch">
                                    <input type="hidden" name="dine_in" value="disabled" />
                                    <input type="checkbox" name="dine_in" value="enabled">
                                    <span class="slider round"></span>
                                </label>
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
        <div class="col-md-6">
            <div id="offers_delivery">
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="form-group">
                            <h5>Does your business offers delivery service? <label class="switch">
                                    <input type="checkbox" id="delivery_confirmation" value="enabled">
                                    <span class="slider round"></span>
                                </label>
                            </h5>
                        </div>
                    </div>
                </div>
                <div class="row" id="delivery_option">
                    <div class="col-md-6">
                        <div class="form-group">
                            <h6>Paid Delivery <label class="switch">
                                    <input type="radio" name="delivery" value="Paid" checked>
                                    <span class="slider round"></span>
                                </label>
                            </h6>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <h6>Free Delivery <label class="switch">
                                    <input type="radio" name="delivery" value="Free">
                                    <span class="slider round"></span>
                                </label>
                            </h6>
                        </div>
                    </div>
                </div>
            </div>    
        </div> 
    </div>
</div>

<!-- TAB 4 LAST -->
<div class="tab">
    <h3 class="card-title m-t-15 text-primary">Login Info</h3>
    <div class="form-group">
        <label>Username</label>
        <input type="text" name="username" class="input_validate form-control" id="username" placeholder="Username"
            required />
        <div class="validation" id="username_result"><span class="text-danger"></span></div>
    </div>
    <div class="form-group">
        <label>Email Address</label>
        <input type="text" name="owner_email" class="input_validate form-control" id="owner_email"
            placeholder="Email Address" required />
        <div class="validation" id="email_result"></span></div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="input_validate form-control" class="form-control" name="password" />
                <div class="validation"></div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label>Confirm Password</label>
                <input type="password" class="input_validate form-control" name="confirm_password" id="confirm_password"
                    placeh2older="Confirm Password" data-rule="minlen:4"
                    data-msg="Please enter at least 8 chars of subject" />
                <div class="validation"></div>
            </div>
        </div>
    </div>
</div>
<div style="overflow:auto;">
    <div style="float:right;">
        <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
        <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
    </div>
</div>
<!-- Circles which indicates the steps of the form: -->
<div style="text-align:center;margin-top:40px;">
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
</div>

</form>

<!-- JavaScript Libraries -->
<script src="<?php echo site_url(); ?>assets/lib/jquery/jquery.min.js"></script>
<script src="<?php echo site_url(); ?>assets/lib/jquery/jquery-migrate.min.js"></script>
<script src="<?php echo site_url(); ?>assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo site_url(); ?>assets/lib/easing/easing.min.js"></script>
<script src="<?php echo site_url(); ?>assets/lib/mobile-nav/mobile-nav.js"></script>
<script src="<?php echo site_url(); ?>assets/lib/wow/wow.min.js"></script>
<script src="<?php echo site_url(); ?>assets/lib/waypoints/waypoints.min.js"></script>
<script src="<?php echo site_url(); ?>assets/lib/counterup/counterup.min.js"></script>
<script src="<?php echo site_url(); ?>assets/lib/owlcarousel/owl.carousel.min.js"></script>
<script src="<?php echo site_url(); ?>assets/lib/isotope/isotope.pkgd.min.js"></script>
<script src="<?php echo site_url(); ?>assets/lib/lightbox/js/lightbox.min.js"></script>
<!-- Contact Form JavaScript File -->
<script src="<?php echo site_url(); ?>assets/contactform/contactform.js"></script>

<!-- Template Main Javascript File -->
<script src="<?php echo site_url(); ?>assets/js/main.js"></script>

<!-- Custom Javascript File -->

<script src="<?php  echo site_url();?>assets/js/check_image.js"></script>

<script src="<?php  echo site_url();?>assets/js/confirm_password.js"></script>

<script src="<?php  echo site_url();?>assets/js/check_username_email_availability.js"></script>

<script src="<?php  echo site_url();?>assets/js/multiform_create_business.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBX3pXE0_O4WKGg-pNInE4cdbEacQ6cPvE&libraries=places&callback=map" async defer></script>

<script src="<?php echo site_url(); ?>assets/map/js/map.js"></script>


<script type="text/javascript">

//----THIS IS FOR SHPWING DELIVERY INPUT  
$('#image-field').hide();

$("#offers_delivery").hide();
$("#offers_pick_up").hide();
$("#offers_dine_in").hide();
$("#delivery_option").hide();

$('#category').change(function() {

    var category = $(this).val();
    var dashChar = category.indexOf("-");

    category = category.substring(0, dashChar);

    if(category == 'Foods & Restaurants') {

        $("#offers_delivery").show();
        $("#offers_pick_up").show();
        $("#offers_dine_in").show();

    } else {

        $("#offers_delivery").hide();
        $("#offers_pick_up").hide();
        $("#offers_dine_in").hide();

    }

});

$("#delivery_confirmation").change(function() {

    $("#delivery_option").toggle();
});


</script>

</body>
</html>
