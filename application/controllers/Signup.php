<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {

 public function __construct()
 {
  parent::__construct();
  $this->load->library('form_validation');
  $this->load->library('encryption');

  $this->load->model('Signup_model');

 }

 function index()
 {
  $this->load->view('auth/signup');
 }

 function validation()
 {
  $this->form_validation->set_rules('username','Username','required|is_unique[merchant_access.username]');
  $this->form_validation->set_rules('email', 'Email Address', 'required|trim|valid_email|is_unique[merchant_access.email]');
  $this->form_validation->set_rules('password', 'Password', 'required');
  $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');

  if($this->form_validation->run())
  {


if( $this->input->post('signup_type') == "user"){
         
             $encrypted_password = $this->encryption->encrypt($this->input->post('password'));
               $data = array(
                'username'  => $this->input->post('username'),
                'email'  => $this->input->post('email'),
                'password' => $encrypted_password,
               
               );

               $id = $this->Signup_model->insert_user($data);

                               if($id > 0)
                               {

                                 redirect('login');

                               }
                              else
                              {
                               $this->index();
                              }
           
        }

if( $this->input->post('signup_type') == "admin"){
         
             $encrypted_password = $this->encryption->encrypt($this->input->post('password'));
               $data = array(
                'username'  => $this->input->post('username'),
                'email'  => $this->input->post('email'),
                'password' => $encrypted_password,
               
               );
               $id = $this->Signup_model->insert_admin($data);
                               if($id > 0)
                               {

                                 redirect('login');

                               }
                              else
                              {
                               $this->index();
                              }
           
        }

if( $this->input->post('signup_type') == "business_owner"){
         
             $encrypted_password = $this->encryption->encrypt($this->input->post('password'));
               $data = array(
                'username'  => $this->input->post('username'),
                'email'  => $this->input->post('email'),
                'password' => $encrypted_password,
               
               );
               $id = $this->Signup_model->insert_merchant($data);
                               if($id > 0)
                               {

                                 redirect('login');

                               }
                              else
                              {
                               $this->index();
                              }
           
        }
  
} else
  {
   $this->index();
  }
}
}
?>
