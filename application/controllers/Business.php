<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Business extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->model('Business_model');
        $this->check_session();
    }
    
    
    //----------Pages of Merchant----------------
    
    public function index()
    {
        $data['hasmenu'] = 'home';
        $data['submenu'] = 'dashboard';
        $this->load->view('merchant/business-dashboard', $data);
    }
    
    public function dashboard()
    {
        $data['hasmenu'] = 'home';
        $data['submenu'] = 'dashboard';
        $this->load->view('merchant/business-dashboard', $data);
    }
    
    public function profile()
    {
        $merchant_id = $this->session->userdata('merchant_id');

        $result_business      = $this->Business_model->get_business($merchant_id);
        $row_business         = $result_business[0];

        $business_id =  $row_business->business_id; 

        $data['result_image'] = $this->Business_model->get_all_business_image($business_id);
        $data['result_merchant'] = $this->Business_model->get_merchant($merchant_id);
        $data['result_setting'] = $this->Business_model->get_setting($business_id);
        $data['result_business'] = $row_business;    
        $data['hasmenu'] = 'home';
        $data['submenu'] = 'profile';

        $this->load->view('merchant/business-profile', $data);
    }


    //--------------------------ACCOMmODATION BOOKING TRANSACTION PAGES --------------------------//
    public function booking()
    {
        $merchant_id = $this->session->userdata('merchant_id');
    
        $data['result_accommodation_business'] = $this->Business_model->get_all_accommodation_business($merchant_id);
        $data['hasmenu'] = 'bookings';
        $data['submenu'] = 'booking';
        $this->load->view('merchant/business-booking', $data);
    }
    

    //--------------------------ACCOMmODATION_RESERVATION PAGES --------------------------//
    public function accommodation_reservation()
    {
        $merchant_id = $this->session->userdata('merchant_id');
    
        $data['result_accommodation_business'] = $this->Business_model->get_all_accommodation_business($merchant_id);
        $data['hasmenu'] = 'accommodation_reservations';
        $data['submenu'] = 'reservation';
        $this->load->view('merchant/business-accommodation-reservation', $data);
    }

    public function accommodation_reserved()
    {
        $merchant_id = $this->session->userdata('merchant_id');
    
        $data['result_accommodation_business'] = $this->Business_model->get_all_accommodation_business($merchant_id);
        $data['hasmenu'] = 'accommodation_reservations';
        $data['submenu'] = 'reserved';
        $this->load->view('merchant/business-accommodation-reserved', $data);
    }

    public function accommodation_declined()
    {
        $merchant_id = $this->session->userdata('merchant_id');
    
        $data['result_accommodation_business'] = $this->Business_model->get_all_accommodation_business($merchant_id);
        $data['hasmenu'] = 'accommodation_reservations';
        $data['submenu'] = 'declined';
        $this->load->view('merchant/business-accommodation-declined', $data);
    }

    public function accommodation_reservation_completed()
    {
        $merchant_id = $this->session->userdata('merchant_id');
    
        $data['result_accommodation_business'] = $this->Business_model->get_all_accommodation_business($merchant_id);
        $data['hasmenu'] = 'accommodation_reservations';
        $data['submenu'] = 'completed';
        $this->load->view('merchant/business-accommodation-reservation-completed', $data);
    }

    public function accommodation_expired()
    {
        $merchant_id = $this->session->userdata('merchant_id');
    
        $data['result_accommodation_business'] = $this->Business_model->get_all_accommodation_business($merchant_id);
        $data['hasmenu'] = 'accommodation_reservations';
        $data['submenu'] = 'expired';
        $this->load->view('merchant/business-accommodation-expired', $data);
    }
    
    

    //--------------------------FOOD AND RESTAURANT ORDER TRANSACTION PAGES --------------------------//
    public function order()
    {
        $merchant_id = $this->session->userdata('merchant_id');

        $data['result_order_business'] = $this->Business_model->get_food_business($merchant_id);
        $data['hasmenu'] = 'order';
        $data['submenu'] = 'transaction';

        $this->load->view('merchant/business-order', $data);
    }

    public function pending()
    {
        $merchant_id = $this->session->userdata('merchant_id');

        $data['result_order_business'] = $this->Business_model->get_food_business($merchant_id);
        $data['hasmenu'] = 'order';
        $data['submenu'] = 'pending';

        $this->load->view('merchant/business-pending', $data);
    }

    public function confirmed()
    {
        $merchant_id = $this->session->userdata('merchant_id');

        $data['result_order_business'] = $this->Business_model->get_food_business($merchant_id);
        $data['hasmenu'] = 'order';
        $data['submenu'] = 'confirmed';

        $this->load->view('merchant/business-confirmed', $data);
    }

    public function preparing()
    {
        $merchant_id = $this->session->userdata('merchant_id');

        $data['result_order_business'] = $this->Business_model->get_food_business($merchant_id);
        $data['hasmenu'] = 'order';
        $data['submenu'] = 'preparing';

        $this->load->view('merchant/business-preparing', $data);
    }

    public function deliver()
    {
        $merchant_id = $this->session->userdata('merchant_id');

        $data['result_order_business'] = $this->Business_model->get_food_business($merchant_id);
        $data['hasmenu'] = 'order';
        $data['submenu'] = 'deliver';

        $this->load->view('merchant/business-deliver', $data);
    }

    public function completed()
    {
        $merchant_id = $this->session->userdata('merchant_id');

        $data['result_order_business'] = $this->Business_model->get_food_business($merchant_id);
        $data['hasmenu'] = 'order';
        $data['submenu'] = 'completed';

        $this->load->view('merchant/business-completed', $data);
    }

    public function cancelled()
    {
        $merchant_id = $this->session->userdata('merchant_id');

        $data['result_order_business'] = $this->Business_model->get_food_business($merchant_id);
        $data['hasmenu'] = 'order';
        $data['submenu'] = 'cancelled';

        $this->load->view('merchant/business-cancelled', $data);
    }
    //-------------------------- END OF FOOD AND RESTAURANT ORDER TRANSACTION PAGES --------------------------//
    
    
    public function rooms()
    {
        $merchant_id = $this->session->userdata('merchant_id');
        $business_id = $this->get_accommodation_business_id();

        $result_room_rate = $this->Business_model->get_room_rate($business_id);

        $data['result_accommodation_business'] = $this->Business_model->get_all_accommodation_business($merchant_id);
        $data['result_room_rate'] = '';
        $data['hasmenu']   = 'products and services';
        $data['submenu']   = 'rooms';

        $this->load->view('merchant/business-rooms', $data);
    }
    
    public function foods()
    {
        
        $merchant_id = $this->session->userdata('merchant_id');
        
        $data['result_food_business'] = $this->Business_model->get_food_business($merchant_id);
        $data['hasmenu']              = 'products and services';
        $data['submenu']              = 'foods';
        
        $this->load->view('merchant/business-foods', $data);
    }
    
    
    public function edit_product($food_id)
    {
        $data['result_food'] = $this->Business_model->get_food($food_id);
        $data['hasmenu']     = 'products and services';
        $data['submenu']     = 'foods';
        
        $this->load->view('merchant/business-edit-product', $data);
    }
    
    public function edit_rooms($room_id)
    {

        $result_room    = $this->Business_model->get_room($room_id);;
        $room           = $result_room[0];

        $data['result_room_image'] = $this->Business_model->get_all_room_image($room_id);
        $data['result_room'] = $room;
        $data['result_room_rate'] = $this->Business_model->get_all_room_rate($room_id);
        $data['hasmenu']     = 'products and services';
        $data['submenu']     = 'rooms';
        $this->load->view('merchant/business-edit-rooms', $data);
    }




    //----------Ajax request----------------
    public function fetch_dashboard()
    {
        $business_id = $this->get_food_business_id();
        
        $customer_num = $this->Business_model->get_num_customer($business_id);
        $transaction_num = $this->Business_model->get_num_order_transaction($business_id);
        $order_num = $this->Business_model->get_num_order($business_id);
        $order_payment = $this->Business_model->get_total_order($business_id);

        $data_for_dashboard = array(

            'customer_num' => $customer_num, 
            'transaction_num' => $transaction_num, 
            'order_num' => $order_num, 
            'order_payment' => $order_payment 

        );

        echo json_encode($data_for_dashboard);
        
    }

    public function fetch_customer_details()
    {
        $transaction_id = $this->input->post('transaction_id');
        
        $customer_details = $this->Business_model->get_customer_details($transaction_id);
        echo json_encode($customer_details);
        
    }

    public function fetch_order_confirmation()
    {
        $transaction_id = $this->input->post('transaction_id');
        
        $order_confirmation_details = $this->Business_model->get_order_confirmation($transaction_id);
        echo json_encode($order_confirmation_details);
        
    }

    public function fetch_payment()
    {
        $transaction_id = $this->input->post('transaction_id');
        
        $payment_details = $this->Business_model->get_payment($transaction_id);

        echo json_encode($payment_details);
        
    }
    
    public function fetch_order()
    {
        $transaction_id = $this->input->post('transaction_id');
        
        $order        = $this->Business_model->get_order($transaction_id);
        echo json_encode($order);
        
    }
    

    public function fetch_reservation_room_details()
    {
        $transaction_id = $this->input->post('transaction_id');
        
        $reservation_room_details   = $this->Business_model->get_reservation($transaction_id);
        echo json_encode($reservation_room_details);
        
    }
    
    public function fetch_booking_transaction()
    {
        
        $business_id = $this->get_accommodation_business_id();
        
        $transaction  = $this->Business_model->get_all_booking_transaction($business_id);
        $list['data'] = $transaction;
        echo json_encode($list);
        
    }

    public function fetch_transaction()
    {   

        $business_type = $this->input->post('business_type');

        if( $business_type == "Accommodation"){
            $business_id = $this->get_accommodation_business_id();  
        } 
        if( $business_type == "Foods & Restaurants"){
            $business_id = $this->get_food_business_id();
        }


        $data = array(
             'status' => $this->input->post('status'),
              'transaction_type' => $transaction_type = $this->input->post('transaction_type'),
               'business_type' => $business_type = $this->input->post('business_type'),
                'business_id' => $business_id
        );
        
        $transaction  = $this->Business_model->get_transaction($data);
        $list['data'] = $transaction;
        $list['count'] = count($transaction);
        echo json_encode($list);
        
    }

    public function fetch_setting()
    {
        
        $business_id = $this->get_food_business_id();
        
        $delivery_setting    = $this->Business_model->get_setting($business_id);
        echo json_encode($delivery_setting);
        
    }
    
    public function fetch_food()
    {
        
        $business_id = $this->get_food_business_id();
        
        $food         = $this->Business_model->get_all_food($business_id);
        $list['data'] = $food;
        echo json_encode($list);
        
    }
    
    public function fetch_room()
    {
        
        $business_id = $this->get_accommodation_business_id();
        
        $room         = $this->Business_model->get_all_room($business_id); 

        $list['data'] = $room;
        echo json_encode($list);
        
    }

    public function fetch_room_rate()
    {
        $room_id = $this->input->post('room_id');

        $room_rate    = $this->Business_model->get_all_room_rate($room_id);

        echo json_encode($room_rate);
        
    }

    public function fetch_sidebar()
    {
        
        $merchant_id = $this->session->userdata('merchant_id');

        $food_business = $this->Business_model->get_food_business($merchant_id);
        $accommodation_business = $this->Business_model->get_all_accommodation_business($merchant_id);

        $ul_food = array('food' => ''); 
        $ul_accommodation = array('accommodation' => '');

        if(empty($food_business)){
        $ul_food = array('food' => 'none');
        } 

        if(empty($accommodation_business)){
        $ul_accommodation = array('accommodation' => 'none');
        } 

        $result = array_merge($ul_accommodation,$ul_food);
        echo json_encode($result);
        
    }


//--------order part -----------//
    public function confirm_order()
    {
        
        $data = $this->input->post();

        $this->Business_model->update_order($data);
        $this->Business_model->update_payment($data);
        $this->Business_model->update_transaction($data);
        $this->Business_model->insert_order_confirmation($data);
        echo json_encode($data);
        
    }

    public function cancel_order()
    {
        
        $data = $this->input->post();

        $this->Business_model->update_transaction($data);
        $this->Business_model->update_all_order($data);
        $this->Business_model->insert_order_confirmation($data);

       $sql = $this->db->last_query(); 
        echo json_encode($sql);
        
    }

    public function update_order_transaction()
    {
        
        $data = $this->input->post();

        $this->Business_model->update_transaction($data);
        
        echo json_encode($data);     
    }

     public function completed_order_transaction()
    {
        
        $data = $this->input->post();

        $this->Business_model->update_order_transaction_and_payment($data);
        
        echo json_encode($data);     
    }

//------- Reservation part ------////

    public function decline_reservation()
    {
        
        $data = $this->input->post();


        $this->Business_model->update_transaction($data);
        $this->Business_model->update_reservation_status($data);
        //$this->Business_model->insert_order_confirmation($data);
        echo json_encode($data);
        
    }

    public function reserve_reservation()
    {
        
        $data = $this->input->post();

        $transaction_id = $data['transaction_id'];
        $expiry_hours_interval = $data['expiry_hours_interval'];
        $expiry_minutes_interval = $data['expiry_minutes_interval'];
        $expiry_seconds_interval = $data['expiry_seconds_interval'];

        //------- GETS THE CHECK IN DATE----------------//
        $reservation = $this->Business_model->get_reservation($transaction_id);
        $check_in_date = $reservation->check_in_date;

        //-------- ADDS THE INTERVAL TIME TO CHECK IN TIME -----//
        $expiry_date = date('m-d-Y H:i:s',strtotime('+'.$expiry_hours_interval.' hour +'.$expiry_minutes_interval.' minutes +'.$expiry_seconds_interval.' seconds',strtotime($check_in_date)));

        $this->Business_model->update_transaction($data);
        $this->Business_model->update_reservation_status($data);
        $this->Business_model->update_reservation_expiry_date($transaction_id,$expiry_date);
        //$this->Business_model->insert_order_confirmation($data);
        echo json_encode($expiry_date);
        
    }

    public function update_expire_reservation()
    {
        $current_date = date('m-d-Y H:i:s');

        $business_id = $this->get_accommodation_business_id(); 

        $result_reservation = $this->Business_model->get_all_reservation($business_id);

        //------- checks all expiry dates and compare to the current time--------
        foreach ($result_reservation as $reservation) {

            $expiry_date = $reservation->expiry_date; 
            echo json_encode($expiry_date);

            if($current_date <= $expiry_date){

                $transaction_id = $reservation->transaction_id;

                $data_reservation = array(
                    'status' => 'expired', 
                );

                $this->Business_model->update_reservation_expiry_date($data_reservation,$transaction_id);
            }

        }

       
        
    }



    //----------CRUD----------------
    
    public function add_food()
    {
        
        $business_id = $this->get_food_business_id();
        
        $display_image = $this->do_upload_image();

        $new_display_image = $this->do_update_image($display_image);
        
        $data_for_food = array(
            'business_id' => $business_id,
            'food_name' => $this->input->post('name'),
            'description' => $this->input->post('description'),
            'price' => $this->input->post('price'),
            'stock' => $this->input->post('stock'),
            'display_image' => $new_display_image
        );
        
        $this->Business_model->insert_food($data_for_food);
        
        redirect("Business/foods");
        
    }
    
    public function add_room()
    {
        
        $business_id = $this->get_accommodation_business_id();
        
        $display_image = $this->do_upload_image();
        
        $data_for_room = array(
            'business_id' => $business_id,
            'room_type' => $this->input->post('name'),
            'description' => $this->input->post('description'),
            'max_guest' => $this->input->post('max_guest'),
            'room_rate_type' => $this->input->post('room_rate_type'),
            'room_status' => $this->input->post('room_status'),
            'display_image' => $display_image
        );

        $room_id = $this->Business_model->insert_room($data_for_room);


        $data_for_room_rate = array(
            'room_id' => $room_id,
            'room_type' => $this->input->post('name'),
            'room_rate_type' => $this->input->post('room_rate_type'),
            'rate' => $this->input->post('rate'),
            'price' => $this->input->post('price'),
        );
        
        $this->Business_model->insert_room_rate($data_for_room_rate);

        print_r($data_for_room_rate);

        redirect("Business/rooms");
        
    }

     public function add_image()
    {
        
        $display_image = $this->do_upload_image();

        $display_image_type = explode('.', $display_image);

        $image_type = $display_image_type[1];

        $data = array (
            
                'type' => $this->input->post('type'),
                'id' => $this->input->post('business_id'),
                'upload_date' => $this->input->post('upload_date'),
                'image_name' => $display_image,
                'image_type' => $image_type,
                
    
                      );

        $this->Business_model->insert_business_image($data);
        redirect("Business/profile#gallery");
        
    }

     public function add_room_image()
    {
        
        $display_image = $this->do_upload_image();
        $room_id       = $this->input->post('room_id');

        $data = array (
            
                'room_id' => $room_id,
                'upload_date' => $this->input->post('upload_date'),
                'image_name' => $display_image,               
    
                      );

        $this->Business_model->insert_room_image($data);
        redirect("Business/edit_rooms/".$room_id."#gallery");
        
    }
    
    public function delete_food()
    {
        
        $food_id         = $this->input->post('food_id');
        $result_for_food = $this->db->get_where('food', array(
            'food_id' => $food_id
        ));
        
        $rows = $result_for_food->result();
        foreach ($rows as $row) {
            unlink("./images/food/" . $row->display_image);
        }
        
        $this->Business_model->delete_food($food_id);
        
    }
    
    public function delete_room()
    {
        
        $room_id         = $this->input->post('room_id');
        $result_for_room = $this->db->get_where('room', array(
            'room_id' => $room_id
        ));
        
        $rows = $result_for_room->result();
        foreach ($rows as $row) {
            unlink("./images/room/" . $row->display_image);
        }
        
        $this->Business_model->delete_room($room_id);
        $this->Business_model->delete_room_rate($room_id);
        
    }

     public function delete_image()
    {   

        $image_id =  $this->uri->segment(3);
        $image_path =  $this->uri->segment(4);
    
        $result = $this->db->get_where('image', array('image_id'=>$image_id));

        $rows = $result->result();

        foreach ($rows as $row) {
            unlink("./images/".$image_path."/".$row->image_name);
        }

        $this->Business_model->delete_image($image_id);
        redirect("Business/profile#gallery");
        
    }

     public function delete_room_image()
    {   

        $image_id =  $this->uri->segment(3);
        $image_path =  $this->uri->segment(4);
        $room_id =  $this->uri->segment(5);
    
        $result = $this->db->get_where('room_image', array('room_image_id'=>$image_id));

        $rows = $result->result();

        foreach ($rows as $row) {
            unlink("./images/".$image_path."/".$row->image_name);
        }

        $this->Business_model->delete_room_image($image_id);
        
        redirect("Business/edit_rooms/".$room_id."#gallery");
        
    }
    
    public function edit_food()
    {
        
        $display_image = $this->do_upload_image();

        $new_display_image = $this->do_update_image($display_image);
        
        
        $food_id = $this->input->post('food_id');
        
        $data_for_food = array(
            'food_name' => $this->input->post('name'),
            'description' => $this->input->post('description'),
            'price' => $this->input->post('price'),
            'stock' => $this->input->post('stock'),
            'display_image' => $new_display_image
        );
        
        $this->Business_model->update_food($data_for_food, $food_id);

        $this->session->set_flashdata('success','Food is successfully edited.');
        
        redirect('Business/foods');
        
    }
    
    public function edit_room()
    {
        
        $display_image = $this->do_upload_image();

        $new_display_image = $this->do_update_image($display_image);
        
        $room_id = $this->input->post('room_id');
        
        $data_for_room = array(
            'room_type' => $this->input->post('name'),
            'description' => $this->input->post('description'),
            'max_guest' => $this->input->post('max_guest'),
            'room_status' => $this->input->post('room_status'),
            'display_image' => $new_display_image
        );
        
        $this->Business_model->update_room($data_for_room, $room_id);

        $data_for_room_rate = array(
            'room_rate_id' => $this->input->post('room_rate_id'),
            'rate' => $this->input->post('rate'),
            'price' => $this->input->post('price'),
        );

        $this->Business_model->update_room_rate($data_for_room_rate);
        print_r($data_for_room_rate);
        redirect('Business/rooms');
        
    }

    public function edit_business()
    {
        
        $business_id = $this->input->post('business_id');
        
        $data_for_business = array (
                'business_name' => $this->input->post('business_name'),
                'business_address' => $this->input->post('business_address'),
                'date_created' => $this->input->post('date_created'),
                'email' => $this->input->post('email'),
                'tel_number' => $this->input->post('tel_number'),
                'phone_number' => $this->input->post('phone_number'),
                      );
        
        $this->Business_model->update_business($data_for_business, $business_id);
        
        redirect('Business/profile');
        
    }

     public function edit_business_description()
    {
        
        $business_id = $this->input->post('business_id');
        
        $data_for_business = array (
                'description' => $this->input->post('description'),
                      );
        
        $this->Business_model->update_business_description($data_for_business, $business_id);
        
        redirect('Business/profile');
        
    }

     public function edit_setting()
    {
        $data = $this->input->post();

        $business_id = $this->get_food_business_id();

        $this->Business_model->update_setting($data,$business_id);
  
    }

       public function edit_logo()
    {
        
        $display_image = $this->do_upload_image();

        $new_display_image = $this->do_update_image($display_image);
        
        $business_id = $this->input->post('business_id');
        
        $data_for_logo = array(
            'display_image' => $new_display_image
        );
        
        $this->Business_model->update_logo($data_for_logo, $business_id);

        $this->session->set_flashdata('success','Your Business Logo has been changed.');


        
        redirect('Business/profile');
        
    }

       public function edit_merchant()
    {
        
        $display_image = $this->do_upload_image();

        $new_display_image = $this->do_update_image($display_image);
        
        $merchant_id = $this->input->post('merchant_id');
        
        $data_for_merchant = array(
            'first_name' => $this->input->post('first_name'),
            'middle_name' => $this->input->post('middle_name'),
            'surname' => $this->input->post('surname'),
            'gender' => $this->input->post('gender'),
            'birthdate' => $this->input->post('birthdate'),
            'address' => $this->input->post('address'),
            'tel_number' => $this->input->post('tel_number'),
            'phone_number' => $this->input->post('phone_number'),
            'email' => $this->input->post('email'),
            'profile_image' => $new_display_image,
        );
        
        $this->Business_model->update_merchant($data_for_merchant, $merchant_id);

        $this->session->set_flashdata('success','Your Merchant details has been changed.');


        
        redirect('Business/profile#owner_info');
        
    }



    //----------Get Business ID ONLY----------------
    
    private function get_food_business_id()
    {
        
        $merchant_id = $this->session->userdata('merchant_id');
        
        $result      = $this->Business_model->get_food_business($merchant_id);
        $row         = $result[0];
        $business_id = $row->business_id;
        
        return $business_id;
        
    }
    
    private function get_accommodation_business_id()
    {

        $merchant_id = $this->session->userdata('merchant_id');
        
        $result      = $this->Business_model->get_all_accommodation_business($merchant_id);
        $row         = $result[0];
        $business_id = $row->business_id;
        
        return $business_id;
        
    }

    //----------Get Business URL ONLY----------------

    private function get_business_url()
    {

        $merchant_id = $this->session->userdata('merchant_id');

        $result      = $this->Business_model->get_business($merchant_id);
        $row         = $result[0];
        $business_url = $row->url;
        
        return $business_url;
        
    }


    //----------Upload image files to server----------------

    private function do_upload_image()
    {
        
        $image_name = $this->input->post('name');
        $image_path = $this->input->post('image_path');
        
        $type      = explode('.', $_FILES["display_image"]["name"]);
        $type      = $type[count($type) - 1];
        $image_name = str_replace(' ', '_', $image_name);
        
        $site_url = site_url();
        
        $display_image            = $image_name . '_' . uniqid(rand()) . '.' . $type;
        $move_image_url = "./images/".$image_path."/" . $display_image;
        
        if (!empty($_FILES['display_image'])) {
            if (is_uploaded_file($_FILES["display_image"]["tmp_name"])) {
                if (move_uploaded_file($_FILES["display_image"]["tmp_name"], $move_image_url)) {
                    return $display_image;
                }
            }
        } else
            return '';
    }

    private function do_update_image($display_image)
    {
        //deletes old image 
        $image_path = $this->input->post('image_path');

        if ($display_image != '') {
            
            $old_display_image = $this->input->post('old_display_image');
            unlink("./images/".$image_path."/" . $old_display_image);

            return $display_image;
            
        } else {
            
            $display_image = $this->input->post('old_display_image');

            return $display_image;
            
        }

    }


    
    //----------Logout and Check Session----------------
    
    public function logout()
    {

        $business_url = $this->get_business_url();

        $data = $this->session->all_userdata();
        foreach ($data as $row => $rows_value) {
            $this->session->unset_userdata($row);
        }
        redirect('login/'.$business_url);
    }
    
    private function check_session()
    {
        $business_url = $this->get_business_url();

        if (!$this->session->userdata('merchant_access_id')) {
            
            redirect('login/'.$business_url);
        }
    }

    
}

?>