<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->model('Plan_model');
        $this->load->helper('string');
        
    }


    public function create_business()
    {
        
        $this->load->view('create-business');
    }
    
    public function validate()
    {
        
    
        $encrypted_password = $this->encryption->encrypt($this->input->post('password'));
        
        $data = array(
            'username' => $this->input->post('username'),
            'email' => $this->input->post('owner_email'),
            'password' => $encrypted_password
        );
        
        $merchant_access_id = $this->Plan_model->insert_merchant_access($data);    
        
        $data_for_merchant = array(
            'first_name' => $this->input->post('first_name'),
            'surname' => $this->input->post('last_name'),
            'gender' => $this->input->post('gender'),
            'birthdate' => $this->input->post('birthdate'),
            'address' => $this->input->post('owner_address'),
            'tel_number' => $this->input->post('owner_tel_number'),
            'phone_number' => $this->input->post('owner_phone_number'),
            'email' => $this->input->post('owner_email'),
            'merchant_access_id' => $merchant_access_id
        );
        
        $merchant_id = $this->Plan_model->insert_merchant($data_for_merchant);
        
        
        $display_image = $this->do_upload();
        
        $business_type_and_category = explode('-', $_POST['category']);
        
        $business_type = $business_type_and_category[0];
        $category      = $business_type_and_category[1];

        $url = random_string('alnum',25);
        
        $data_for_business = array(
            'business_name' => $this->input->post('business_name'),
            'business_address' => $this->input->post('business_address'),
            'date_created' => $this->input->post('date_created'),
            'email' => $this->input->post('email'),
            'tel_number' => $this->input->post('tel_number'),
            'phone_number' => $this->input->post('phone_number'),
            'description' => $this->input->post('description'),
            'subscription' => $this->input->post('subscription'),
            'category' => $category,
            'business_type' => $business_type,
            'display_image' => $display_image,
            'merchant_id' => $merchant_id,
            'url' => $url
        );
        
        $business_id = $this->Plan_model->insert_business($data_for_business);
        
        $data_for_marker = array(
            'id' => $business_id,
            'longitude' => $this->input->post('longitude'),
            'latitude' => $this->input->post('latitude'),
            'name' => $this->input->post('business_name'),
            'marker_type' => 'business'
        );
        
        $this->Plan_model->insert_marker($data_for_marker, $business_id);

        $data_for_setting = array(
            'business_id' => $business_id,
            'business_type' => $business_type,
            'delivery' => $this->input->post('delivery'),
            'pick_up' => $this->input->post('pick_up'),
            'dine_in' => $this->input->post('dine_in'),
        );

        $this->Plan_model->insert_setting($data_for_setting);
        
        redirect("Login");
        
    }
    
    
    private function do_upload()
    {
        
        $business_name = $_POST['business_name'];
        
        $type          = explode('.', $_FILES["display_image"]["name"]);
        $type          = $type[count($type) - 1];
        $business_name = str_replace(' ', '_', $business_name);
        
        $site_url = site_url();
        
        $url            = $business_name . '_' . uniqid(rand()) . '.' . $type;
        $move_image_url = "./images/business/" . $url;
        
        if (in_array($type, array("jpg","jpeg","png")))
            if (is_uploaded_file($_FILES["display_image"]["tmp_name"]))
                if (move_uploaded_file($_FILES["display_image"]["tmp_name"], $move_image_url))
                    return $url;
        return "";
        
    }
    
    function check_email_avalibility()
    {
        if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
            echo '<label class="text-danger"><span class="glyphicon glyphicon-remove"></span> Invalid Email</span></label>';
        } else {
            
            if ($this->Plan_model->is_email_available($_POST["email"])) {
                echo '<label class="text-danger"><span class="glyphicon glyphicon-remove"></span> Email Already register</label>';
            } else {
                echo '<label class="text-success"><span class="glyphicon glyphicon-ok"></span> Email Available</label>';
            }
        }
    }
    
    function check_username_avalibility()
    {
        
        if ($this->Plan_model->is_username_available($_POST["username"])) {
            echo '<label class="text-danger"><span class="glyphicon glyphicon-remove"></span> username Already register</label>';
        } else {
            echo '<label class="text-success"><span class="glyphicon glyphicon-ok"></span> Username Available</label>';
        }
        
        
        
    }
}