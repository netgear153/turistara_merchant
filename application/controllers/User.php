<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user extends CI_Controller {
 public function __construct()
 {
  parent::__construct();
  if(!$this->session->userdata('user_access_id'))
  {
   redirect('login');
  }
 }

 function index()
 {
 
   $this->load->view('user_index');
 }

 function logout()
 {
  $data = $this->session->all_userdata();
  foreach($data as $row => $rows_value)
  {
   $this->session->unset_userdata($row);
  }
  redirect('login');
 }
}

?>