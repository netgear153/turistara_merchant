<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

 public function __construct()
 {
  parent::__construct();

  $this->load->library('form_validation');
  $this->load->library('encryption');
  $this->load->helper('string');
  $this->load->model('Login_model');
 }

 function index($route = '')
 {
    if ($route != '')
    {

    $data['logo'] = $this->Login_model->get_logo($route);

    $this->load->view('auth/login',$data);

    }else{

    $data['logo'] = ''; 
    $this->load->view('auth/login',$data);
    
    }
  
 }


 function validation()
 {

  $this->form_validation->set_rules('username', 'Username', 'required');
  $this->form_validation->set_rules('password', 'Password', 'required');
  
  if($this->form_validation->run())
  {

          $result = $this->Login_model->can_login_merchant($this->input->post('username'), $this->input->post('password'));

           if($result == '')
                     {
                      redirect('Business');
                     }
                     else
                     {
                      $this->session->set_flashdata('message',$result);
                      redirect('login');
                     }

  }
  else
  {
   redirect('login');
  }
 }

}

?>