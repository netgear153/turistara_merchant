<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->model('Home_model');
        
    }
    
    public function index()
    {
        
        $this->load->view('index');
    }
    
    public function create_business()
    {
        
        $this->load->view('create-business');
    }
    
    
    
}