<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup_model extends CI_Model {
 
	public function __construct(){
		$this->load->database();
	}

	function insert_user($data)
 {
  $this->db->insert('user_access', $data);
  return $this->db->insert_id();
 }

 function insert_admin($data)
 {
  $this->db->insert('admin', $data);
  return $this->db->insert_id();
 }


function insert_merchant($data)
 {
  $this->db->insert('merchant_access', $data);
  return $this->db->insert_id();
 }


}

 