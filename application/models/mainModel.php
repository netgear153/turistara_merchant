<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mainModel extends CI_Model {
 
	public function __construct(){
		$this->load->database();
	}
	

	function insertStation(){
		$data = array (
				'station_type' => $this->input->post('station_type'),
				'station_name' => $this->input->post('station_name'),
				'address' => $this->input->post('address'),
				'description' => $this->input->post('description'),
				'email' => $this->input->post('email'),
					  );

		$this->db->insert('station', $data);
	}

	function getAllData(){

		$query = $this->db->query('SELECT * from station');
		return $query->result();
	} 	

	function getData($id) {
        $query = $this->db->query('SELECT * FROM station WHERE `station_id` =' .$id);
        return $query->row();
    }

  function updateData($id){

    	$data = array (
				'station_type' => $this->input->post('station_type'),
				'station_name' => $this->input->post('station_name'),
				'address' => $this->input->post('address'),
				'description' => $this->input->post('description'),
				'email' => $this->input->post('email'),
					  );

		$this->db->where('station_id', $id);
		$this->db->update('station', $data);

    }

    function deleteData($id){

    	$this->db->where('station_id', $id);
		$this->db->delete('station');
    }


	}

 