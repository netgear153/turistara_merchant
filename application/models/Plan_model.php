<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plan_model extends CI_Model {
 
	public function __construct(){
		$this->load->database();
	}

function insert_merchant_access($data)
 {
  $this->db->insert('merchant_access', $data);
  return $this->db->insert_id();
 }

function insert_merchant($data)
 {
  $this->db->insert('merchant', $data);
  return $this->db->insert_id();
 }

function insert_business($data_for_business)
 {    
    $this->db->insert('business', $data_for_business);
    return $this->db->insert_id();
 }

function insert_marker($data_for_marker, $business_id){

    $this->db->insert('marker', $data_for_marker);
    $marker_id = $this->db->insert_id();

  //updates business to input marker_id
      $this->db->set('marker_id', $marker_id);      
      $this->db->where('business_id', $business_id);  
      $this->db->update('business');  
 }

function insert_setting($data_for_setting)
 {
  $this->db->insert('setting', $data_for_setting);
  return $this->db->insert_id();
 }

  function is_email_available($email)  
      {  
           $this->db->where('email', $email);  
           $query = $this->db->get("merchant_access");  
           if($query->num_rows() > 0)  
           {  
                return true;  
           }  
           else  
           {  
                return false;  
           }  
      }  

  function is_username_available($username)  
      {  
           $this->db->where('username', $username);  
           $query = $this->db->get("merchant_access");  
           if($query->num_rows() > 0)  
           {  
                return true;  
           }  
           else  
           {  
                return false;  
           }  
      } 

 }

 