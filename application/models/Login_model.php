<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

  public function __construct(){
  $this->load->database();
  }

 function can_login_user($username, $password)
 {
  $this->db->where('username', $username);
  $this->db->or_where('email', $username);
  $query = $this->db->get('user_access');
  if($query->num_rows() > 0)
  {
   foreach($query->result() as $row)
   {

     $store_password = $this->encryption->decrypt($row->password);

     if($password == $store_password)
     {
       $data = array(
          'user_access_id'       => $row->user_access_id,
          'username'             => $row->username,
          'email'                => $row->email,
      );

      $this->session->set_userdata($data);


      //update last login  
      date_default_timezone_set('Asia/Manila');
      $last_login = date('m-d-Y H:i:s');

      $this->db->set('last_login', $last_login);
       
      $this->db->where('user_access_id', $row->user_access_id);  
      $this->db->update('user_access');  
     }
     else
     {
      return 'The password you’ve entered is incorrect.';
     }
    }

  }
  else
  {
   return 'The email or username you’ve entered doesn’t match any account.';
  }
 }


 function can_login_admin($username, $password)
 {
  $this->db->where('username', $username);
  $this->db->or_where('email', $username);
  $query = $this->db->get('admin');
  if($query->num_rows() > 0)
  {
   foreach($query->result() as $row)
   {

     $store_password = $this->encryption->decrypt($row->password);
     if($password == $store_password)
     {
       $data = array(
          'admin_id'       => $row->admin_id,
          'username'       => $row->username,
          'email'          => $row->email,
          'first_name'     => $row->first_name,
          'surname'        => $row->surname,
      );

      $this->session->set_userdata($data);


      //update last login and ip 
      $last_login = date('Y-m-d H:i:s');

      $this->db->set('last_login', $last_login);      
      $this->db->where('admin', $row->admin_id);  
      $this->db->update('admin');  


     }
     else
     {
      return 'The password you’ve entered is incorrect.';
     }
    }

  }
  else
  {
   return 'The email or username you’ve entered doesn’t match any account.';
  }
 }



  function can_login_merchant($username, $password)
 {

  $this->db->select('merchant_access.*, merchant.*');    
  $this->db->from('merchant_access');
  $this->db->join('merchant', 'merchant_access.merchant_access_id = merchant.merchant_access_id');
  $this->db->where('username', $username);
  $this->db->or_where('merchant_access.email', $username);
  $query = $this->db->get();

  if($query->num_rows() > 0)
  {
   foreach($query->result() as $row)
   {

     $store_password = $this->encryption->decrypt($row->password);

     if($password == $store_password)
     {
      
      $data = array(
          'merchant_access_id'   => $row->merchant_access_id,
          'merchant_id'          => $row->merchant_id,
          'username'             => $row->username,
          'email'                => $row->email,
          'profile_image'        => $row->profile_image,
      );

      $this->session->set_userdata($data);

      //update last login and ip 
      date_default_timezone_set('Asia/Manila');
      $ip_addr = $this->input->ip_address();
      $last_login = date('m-d-Y H:i:s');

      $this->db->set('ip', $ip_addr);
      $this->db->set('last_login', $last_login);
       
      $this->db->where('merchant_access_id', $row->merchant_access_id);  
      $this->db->update('merchant_access');  
     }
     else
     {
      return 'The password you’ve entered is incorrect.';
     }
    }

  }
  else
  {
   return 'The email or username you’ve entered doesn’t match any account.';
  }
 }

 function get_logo($route){

    $this->db->select('*');    
    $this->db->from('business');
    $this->db->where('url', $route);
    $result = $this->db->get()->row();
    $display_image = $result->display_image;

    return $display_image;
  } 


}

?>