<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Business_model extends CI_Model 
{
	public function __construct(){
		$this->load->database();
	}

	//query for dashboard
	function get_num_customer($business_id){
	$this->db->distinct();
	$this->db->select('user_id');    
	$this->db->from('transaction');
	$this->db->where('business_id', $business_id);
	$query = $this->db->get();

	return $query->num_rows();
	} 

	function get_num_order_transaction($business_id){
	$this->db->where('business_id', $business_id);
	$this->db->where('business_type', 'Foods & Restaurants');
	$query = $this->db->get('transaction');

	return $query->num_rows();
	} 

	function get_num_order($business_id){
	$this->db->where('business_id', $business_id);
	$query = $this->db->get('order_item');

	return $query->num_rows();
	} 


	function get_total_order($business_id){
	$this->db->select_sum('amount_with_fee');
	$this->db->from('payment');
	$this->db->join('transaction', 'payment.transaction_id = transaction.transaction_id');
	$this->db->where('transaction.business_id', $business_id);
	$this->db->where('transaction.payment_status', 'PAID');
	$this->db->where('business_type', 'Foods & Restaurants');
	
	$query = $this->db->get()->row();

	return $query->amount_with_fee;
	} 

	//ends here
	function get_merchant($merchant_id){
		
		$this->db->select('merchant.*, merchant_access.username');    
		$this->db->from('merchant_access');
		$this->db->join('merchant', 'merchant_access.merchant_access_id = merchant.merchant_access_id');
		$this->db->where('merchant_id', $merchant_id);
		$query = $this->db->get()->row();

		return $query;
	} 

	function get_setting($business_id){
		
		$this->db->select('*');    
		$this->db->from('setting');
		$this->db->where('business_id', $business_id);
		$query = $this->db->get()->row();

		return $query;
	} 

	function get_transaction($data){
		
		$this->db->select('*');    
		$this->db->from('transaction');
		$this->db->join('user', 'transaction.user_id = user.user_id');
		$this->db->where('transaction_type', $data['transaction_type']);
		$this->db->where('business_id', $data['business_id']);
		$this->db->where('status', $data['status']);
		$this->db->order_by("transaction.transaction_id", "asc");
		$query = $this->db->get();

		return $query->result();
	} 

	function get_order($transaction_id){

		$this->db->distinct();
		$this->db->select('order_item.* , transaction.*, food.*');    
		$this->db->from('order_item');
		$this->db->join('transaction', 'order_item.transaction_id = transaction.transaction_id');
		$this->db->join('food', 'order_item.food_id = food.food_id');
		$this->db->where('order_item.transaction_id', $transaction_id);
		$this->db->where('order_item.status !=', 'Cancelled');
		$this->db->order_by("order_item.order_id", "asc");
		$query = $this->db->get();

		return $query->result();
	} 	

	function get_reservation($transaction_id){

		$this->db->distinct();
		$this->db->select('reservation.* , room.*');    
		$this->db->from('reservation');
		$this->db->join('room', 'reservation.room_id = room.room_id');
		$this->db->where('transaction_id', $transaction_id);
		$this->db->order_by("reservation_id", "asc");
		$query = $this->db->get()->row();

		return $query;
	} 

	//_-----THIS IS FOR EXPIRY DATE FOR RESERVATION
	function get_all_reservation($business_id){

		$this->db->select('reservation.* , room.*');    
		$this->db->from('reservation');
		$this->db->join('room', 'reservation.room_id = room.room_id');
		$this->db->where('reservation.business_id', $business_id);
		$this->db->order_by("reservation_id", "asc");
		$query = $this->db->get();

		return $query->result();
	} 

	function get_customer_details($transaction_id){

		$this->db->select('user.* , user_access.* ');    
		$this->db->from('transaction');
		$this->db->join('user', 'transaction.user_id = user.user_id');
		$this->db->join('user_access', 'user.user_access_id = user_access.user_access_id');
		$this->db->where('transaction.transaction_id', $transaction_id);

		$query = $this->db->get()->row();

		return $query;
	} 

	function get_order_confirmation($transaction_id){

		$this->db->select('order_confirmation.* ');    
		$this->db->from('transaction');
		$this->db->join('order_confirmation', 'transaction.transaction_id = order_confirmation.transaction_id');
		$this->db->where('transaction.transaction_id', $transaction_id);

		$query = $this->db->get()->row();

		return $query;
	} 

	function get_payment($transaction_id){

		$this->db->select('payment.* , note');    
		$this->db->from('transaction');
		$this->db->join('payment', 'transaction.transaction_id = payment.transaction_id');
		$this->db->where('transaction.transaction_id', $transaction_id);

		$query = $this->db->get()->row();

		return $query;
	} 

	function get_all_food($business_id){

		$this->db->select('*');    
		$this->db->from('food');
		$this->db->where('business_id', $business_id);
		$this->db->order_by("food_id", "asc");
		$query = $this->db->get();

		return $query->result();
	} 

	function get_all_room($business_id){

		$this->db->select('*');    
		$this->db->from('room');
		$this->db->where('business_id', $business_id);
		$this->db->order_by("room_id", "asc");
		$query = $this->db->get();

		return $query->result();
	} 

	function get_all_business_image($business_id){

		$this->db->select('*');    
		$this->db->from('image');
		$this->db->where('id', $business_id);
		$this->db->where('type', 'business');
		$this->db->order_by("image_id", "asc");
		$query = $this->db->get();

		return $query->result();
	} 

	function get_all_room_image($room_id){

		$this->db->select('*');    
		$this->db->from('room_image');
		$this->db->where('room_id', $room_id);
		$this->db->order_by("room_image_id", "asc");
		$query = $this->db->get();

		return $query->result();
	}

	function get_all_room_rate($room_id){

		$this->db->select('*');    
		$this->db->from('room_rate');
		$this->db->where('room_id', $room_id);
		$this->db->order_by("room_rate_id", "asc");
		$query = $this->db->get();

		return $query->result();
	}  

	function get_food($food_id){

		$this->db->select('food.* , business_name, business_type, category');    
		$this->db->from('food');
		$this->db->join('business', 'food.business_id = business.business_id');
		$this->db->where('food.food_id', $food_id);
		$query = $this->db->get()->row();

		return $query;
	} 

	function get_room($room_id){

		$this->db->select('*');    
		$this->db->from('room');
		$this->db->where('room_id', $room_id);
		$query = $this->db->get();

		return $query->result();
	} 

	function get_room_rate($business_id){

		$this->db->select('DISTINCT(room_rate.rate), room_rate.room_rate_type' );    
		$this->db->from('room');
		$this->db->join('room_rate', 'room.room_id = room_rate.room_id');
		$this->db->where('business_id', $business_id);
		$query = $this->db->get();

		return $query->result();
	} 

	function get_food_business($merchant_id){

		$business_type = 'Foods & Restaurants';

		$this->db->select('*');  
		$this->db->from('business');
		$this->db->where('merchant_id',$merchant_id);
		$this->db->where('business_type',$business_type);			
		$query = $this->db->get();

		return $query->result();
	}

	function get_all_accommodation_business($merchant_id){

		$business_type = 'Accommodation';

		$this->db->select('*');  
		$this->db->from('business');
		$this->db->where('merchant_id',$merchant_id);
		$this->db->where('business_type',$business_type);			
		$query = $this->db->get();

		return $query->result();
	}

	function get_business($merchant_id){

		$this->db->select('*');  
		$this->db->from('business');
		$this->db->where('merchant_id',$merchant_id);	
		$this->db->where('business_type', 'Foods & Restaurants');			
		$query = $this->db->get();

		return $query->result();
	}


	function insert_food($data_for_food){

		$this->db->insert('food', $data_for_food);
		return $this->db->insert_id();
	}	

	function insert_room($data_for_room){

		$this->db->insert('room', $data_for_room);
		return $this->db->insert_id();
	}	

	function insert_room_rate($data_for_room_rate){
		$array_rate = $data_for_room_rate['rate'];
		$array_price = $data_for_room_rate['price'];

	    foreach ($array_rate as $key => $rate) {

	    	$room_rate_type = $data_for_room_rate['room_rate_type'];

	    	if( $room_rate_type == 'per hour'){
	    		$rate = $rate." hours";
	    	}

	    	$data_room_rate = array(
	    		'room_id' => $data_for_room_rate['room_id'],
           		'room_type' => $data_for_room_rate['room_type'],
            	'room_rate_type' => $data_for_room_rate['room_rate_type'],
            	'rate' => $rate,
            	'price' => $array_price[$key],
	    		);

	    	$this->db->insert('room_rate', $data_room_rate);
	   
    	} 		return ;
	}	

	function insert_business_image($data){

    	$this->db->insert('image', $data);
    }
    function insert_room_image($data){

    	$this->db->insert('room_image', $data);
    }

	function delete_food($food_id){

    	$this->db->where('food_id', $food_id);
		$this->db->delete('food');
    }

    function delete_room($room_id){

    	$this->db->where('room_id', $room_id);
		$this->db->delete('room');
    }

    function delete_room_rate($room_id){

    	$this->db->where('room_id', $room_id);
		$this->db->delete('room_rate');
    }

    function delete_image($image_id){

    	$this->db->where('image_id', $image_id);
		$this->db->delete('image');
    }

    function delete_room_image($image_id){

    	$this->db->where('room_image_id', $image_id);
		$this->db->delete('room_image');
    }

    function update_food($data_for_food,$food_id){

		$this->db->where('food_id', $food_id);
		$this->db->update('food', $data_for_food);

    }

    function update_room($data_for_room,$room_id){

		$this->db->where('room_id', $room_id);
		$this->db->update('room', $data_for_room);

    }

    function update_room_rate($data_for_room_rate){

		$array_room_rate_id = $data_for_room_rate['room_rate_id'];
		$array_rate = $data_for_room_rate['rate'];
		$array_price =  $data_for_room_rate['price'];

	foreach ($array_room_rate_id as $key => $room_rate_id) {

    		$data_room_rate = array(
            	'rate' => $array_rate[$key],
            	'price' => $array_price[$key],
	    		);

	    	$this->db->where('room_rate_id', $room_rate_id);
			$this->db->update('room_rate', $data_room_rate);
		}


	}

    function update_business($data_for_business,$business_id){

		$this->db->where('business_id', $business_id);
		$this->db->update('business', $data_for_business);

    }

    function update_setting($data,$business_id){

    	$data_setting = array(
            	$data['service'] => $data['status'],
	    		);

		$this->db->where('business_id', $business_id);
		$this->db->update('setting', $data_setting);

    }

    function update_merchant($data_for_merchant, $merchant_id){

		$this->db->where('merchant_id', $merchant_id);
		$this->db->update('merchant', $data_for_merchant);

    }

    function update_business_description($data_for_business,$business_id){

		$this->db->where('business_id', $business_id);
		$this->db->update('business', $data_for_business);

    }

    function update_logo($data_for_logo,$business_id){

		$this->db->where('business_id', $business_id);
		$this->db->update('business', $data_for_logo);

    }

    function update_order($data){

    	$array_order_id = $data['order_id'];
		$array_cancelled_order = $data['cancelled_order'];
		$array_updated_quantity = $data['quantity'];
		$array_updated_amount =  $data['order_amount'];

	    foreach ($array_order_id as $key => $order_id) {

	    	$data_order = array(
	    		'status' => $array_cancelled_order[$key],
	    		'quantity' => $array_updated_quantity[$key],
	    		'amount' => $array_updated_amount[$key],
	    		);

	    	$this->db->where('order_id',$order_id);
			$this->db->update('order_item', $data_order);

    	}

    }

    //------ used for cancel order function -------
    function update_all_order($data){

    	$transaction_id =  $data['transaction_id'];

    	$data_order = array(
    		'status' => $data['transaction_status'],
    		);

    	$this->db->where('transaction_id',$transaction_id);
		$this->db->update('order_item', $data_order);
		
    	}

    function update_payment($data){

    	$payment_id =  $data['payment_id'];

    	$data_payment = array(
    		'amount' => $data['amount'],
    		'fee' => $data['delivery_fee'],
    		'amount_with_fee' => $data['amount_with_fee']
    		);

    	$this->db->where('payment_id',$payment_id);
		$this->db->update('payment', $data_payment);
		
    	}

    function update_reservation_status($data){

    	$transaction_id =  $data['transaction_id'];

    	$data_reservation = array(
    		'status' => $data['transaction_status'],
    		);

    	$this->db->where('transaction_id',$transaction_id);
		$this->db->update('reservation', $data_reservation);
		
    	}

    function update_reservation_expiry_date($data_reservation,$transaction_id){

    	$this->db->where('transaction_id',$transaction_id);
		$this->db->update('reservation', $data_reservation);
    
    	}

     function update_transaction($data){

    	$transaction_id =  $data['transaction_id'];

    	$data_transaction = array(
    		'status' => $data['transaction_status'],
    		);

    	$this->db->where('transaction_id',$transaction_id);
		$this->db->update('transaction', $data_transaction);
		
    	}

     function update_order_transaction_and_payment($data){

    	$transaction_id =  $data['transaction_id'];

    	$data_transaction = array(
    		'status' => $data['transaction_status'],
    		'payment_status' => $data['payment_status']
    		);

    	$data_payment = array(
    		'payment_status' => $data['payment_status'],
    		);

    	$this->db->where('transaction_id',$transaction_id);
		$this->db->update('transaction', $data_transaction);

		$this->db->where('transaction_id',$transaction_id);
		$this->db->update('payment', $data_payment);
		
    	}

     function insert_order_confirmation($data){

    	$data_order_confirmation = array(
    		'user_id' => $data['user_id'],
    		'transaction_id' => $data['transaction_id'],
    		'payment_id' => $data['payment_id'],
    		'message' => $data['message'],
    		'amount' => $data['amount'],
    		'fee' => $data['delivery_fee'],
    		'amount_with_fee' => $data['amount_with_fee']
    		);

    	$this->db->insert('order_confirmation', $data_order_confirmation);
		
    	}


}

 