-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2019 at 07:44 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `turistara`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `email` varchar(255) NOT NULL,
  `last_login` date NOT NULL,
  `first_name` varchar(35) NOT NULL,
  `middle_name` varchar(35) NOT NULL,
  `surname` varchar(35) NOT NULL,
  `birthdate` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `age` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `booking_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `room` varchar(255) NOT NULL,
  `transaction_date` varchar(255) NOT NULL,
  `check_in_date` varchar(255) NOT NULL,
  `check_out_date` varchar(255) NOT NULL,
  `number_of_person` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`booking_id`, `transaction_id`, `business_id`, `room_id`, `room`, `transaction_date`, `check_in_date`, `check_out_date`, `number_of_person`) VALUES
(1, 1, 5, 1, 'Single', '2019-04-29 6:00 AM', '2019-04-30 ', '2019-05-1 ', 1),
(2, 2, 5, 2, 'Double', '2019-05-2 6:00 AM', '2019-05-3', '2019-05-6 ', 2),
(3, 3, 5, 3, 'Triple', '2019-05-8  6:00 AM', '2019-05-9', '2019-05-12 ', 3),
(4, 4, 5, 4, 'Quad', '2019-05-12 6:00 AM', '2019-05-13 ', '2019-05-15 ', 4),
(5, 5, 6, 5, 'Premier', '2019-05-18 6:00 AM', '2019-05-19 ', '2019-05-19 \r\n', 3),
(6, 6, 6, 6, 'Deluxe', '2019-05-20 6:00 AM\r\n', '2019-05-21 \r\n', '2019-05-24 \r\n', 7),
(7, 7, 6, 7, 'Studio', '2019-05-25 6:00 AM\r\n', '2019-05-26 \r\n', '2019-05-28 \r\n', 6),
(8, 8, 6, 8, 'Superior', '2019-05-20 6:00 AM', '2019-05-20 ', '2019-05-21 \r\n', 3);

-- --------------------------------------------------------

--
-- Table structure for table `branch_category`
--

CREATE TABLE `branch_category` (
  `branch_category_id` int(11) NOT NULL,
  `type` varchar(25) NOT NULL,
  `category` varchar(25) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `business`
--

CREATE TABLE `business` (
  `business_id` int(11) NOT NULL,
  `business_address` varchar(255) NOT NULL,
  `business_name` varchar(255) NOT NULL,
  `business_type` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `date_created` varchar(255) NOT NULL,
  `marker_id` int(11) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `url` longtext NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `tel_number` varchar(15) NOT NULL,
  `description` longtext NOT NULL,
  `display_image` longtext NOT NULL,
  `subscription` varchar(35) NOT NULL,
  `multi_category` varchar(25) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business`
--

INSERT INTO `business` (`business_id`, `business_address`, `business_name`, `business_type`, `category`, `date_created`, `marker_id`, `merchant_id`, `url`, `phone_number`, `tel_number`, `description`, `display_image`, `subscription`, `multi_category`, `email`) VALUES
(5, 'Plaza Alemania Hotel, Quezon Avenue Extension, Iligan City, Lanao del Norte, Philippines', 'Plaza Alemania ', 'Accommodation', 'Hotel', '2019-04-30', 21, 3, 'UVlIPfMETutgzdkGLRWJ1crCo', '0936545875', '2234215', '<p>Plaza&nbsp;Alemania&nbsp;Hotel&nbsp;at&nbsp;Plaza&nbsp;Alemania&nbsp;Hotel,&nbsp;Quezon&nbsp;Avenue&nbsp;Extension,&nbsp;Iligan&nbsp;City,&nbsp;Lanao&nbsp;del&nbsp;Norte,</p>\r\n', 'Plaza_Alemania__20312646135cdb68e015318.jpg', 'listing', '', 'ldanray01@gmail.com'),
(6, '158 Macapagal Ave, Iligan City, 9200 Lanao del Norte, Philippines', 'Go Hotels Iligan', 'Accommodation', 'Hotel', '2019-04-30', 22, 0, '1Er9jB70wFZqiXKUGRMdHVJmy', '9399173673', '6282044', 'Go Hotels Iligan at National Road, Madrid, 8316 Surigao del Sur, Philippines', 'Go_Hotels_Iligan_13346951025cc7f220438fd.jpg', 'listing', '', 'alexlato@gmail.com'),
(7, 'National Road, San Miguel, 8301 Surigao del Sur, Philippines', 'Calle Dos Food Hub', 'Foods & Restaurants', 'Restaurant', '05/28/1972', 23, 3, 'LBJpaxEeR8PO0t46CZU7GzSH9', '9399023976', '2234215', '<p>Calle&nbsp;Dos&nbsp;Food&nbsp;Hub&nbsp;at&nbsp;National&nbsp;Road,&nbsp;San&nbsp;Miguel,&nbsp;8301&nbsp;<em>Surigao&nbsp;</em>del&nbsp;Sur,&nbsp;Philippines</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'Calle_Dos_Food_Hub_10382978585cdc0893c8be6.jpg', 'listing', '', 'ldanray01@gmail.com'),
(8, 'Echeverri St, Iligan City, 9200 Lanao del Norte, Philippines', 'Shann Fille Food Station', 'Foods & Restaurants', 'Restaurant', '2019-04-30', 24, 0, 'PUnQvoeSLgd9Yh8KD4ZlNJ2tF', '9399023976', '09399023976', 'Shann Fille Food Station at Echeverri St, Iligan City, 9200 Lanao del Norte, Philippines', 'Shann_Fille_Food_Station_7276053745cc81df578a9f.jpg', 'listing', '', 'ldanray01@gmail.com'),
(9, 'Iligan City San Miguel St., Iligan City, Lanao del Norte, Philippines', 'Pop Rock', 'Foods & Restaurants', 'Cakes and Pastries', '2019-05-09', 25, 8, '9glhCycRrXaWkB6zxTMdVS84f', '096654855465', '22341', 'Pop Rock at Iligan City San Miguel St., Iligan City, Lanao del Norte, Philippines', 'Pop_Rock_1587997255cd5bd9f7f0bf.png', 'listing', '', 'poprock@gmail.com'),
(10, 'Iligan City, Lanao del Norte, Philippines', 'Lacsi\'s Restaurant', 'Foods & Restaurants', 'Restaurant', '2019-05-30', 26, 9, 'znKEbG57ARNijMX8yJsa6tCFl', '0936545875', '22346589', 'Lacsis\' Restaurant... KAON NA!!!', 'Lacsi\'s_Restaurant_18947210545cefb94a88faa.jpg', 'listing', '', 'alexlazarito@gmail.com'),
(15, 'Fairlight NSW 2094, Australia', 'Irene\'s Eatery', 'Foods & Restaurants', 'Restaurant', '2019-06-04', 31, 14, 'GS791ufvtrPg8TakiN6wMFn3b', '2335458', '123248568', 'Kaon na', 'Irene\'s_Eatery_20400988005cf5fb8a9ca07.png', 'listing', '', 'fdsafd@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `business_subscription_plan`
--

CREATE TABLE `business_subscription_plan` (
  `subscription_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `category` varchar(25) NOT NULL,
  `business_id` int(11) NOT NULL,
  `payment_status` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `child_event`
--

CREATE TABLE `child_event` (
  `child_event_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `child_event_name` varchar(255) NOT NULL,
  `child_event_category` varchar(255) NOT NULL,
  `child_event_address` varchar(255) NOT NULL,
  `child_event_url` text NOT NULL,
  `description` longtext NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `display_date_and_time` longtext NOT NULL,
  `attendee` text NOT NULL,
  `organizer` text NOT NULL,
  `child_event_contact_number` varchar(255) NOT NULL,
  `display_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `child_event`
--

INSERT INTO `child_event` (`child_event_id`, `event_id`, `child_event_name`, `child_event_category`, `child_event_address`, `child_event_url`, `description`, `start_date`, `end_date`, `display_date_and_time`, `attendee`, `organizer`, `child_event_contact_number`, `display_image`) VALUES
(7, 4, 'Danty', 'Religious', 'Pyrmont NSW 2009, Australia', 'http://localhost/turistaraadmin/Child_Event/create/4', 'fdasfsd', '2019-04-25', '2019-04-25', 'fdasfdasfdas', 'fdsafasdfd', 'afdasfas', '23123123', 'Danty_18609358445cc1803bc7a42.png'),
(8, 5, 'Danty', 'Conference and Conventions', '199 George St, The Rocks NSW 2000, Australia', 'http://localhost/turistaraadmin/Child_Event/create/4', 'fdasfdaf', '2019-04-25', '2019-04-25', 'dasfdsfad', 'fdsafasd', 'fdsafadsf', '3213213', 'Danty_205268185cc1809e92d24.png'),
(12, 9, 'Danty', 'Conference and Conventions', '50 Hunter St, Sydney NSW 2000, Australia', 'http://localhost/turistaraadmin/Child_Event/create/4', 'fdafsdfsa', '2019-04-25', '2019-04-25', 'fdsafdfsa', 'fdsafdasfd', 'afdafdsf', '343242342', 'Danty_19704453085cc189921e237.png');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `event_id` int(11) NOT NULL,
  `event_name` varchar(255) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `display_image` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `province` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`event_id`, `event_name`, `start_date`, `end_date`, `display_image`, `city`, `country`, `province`) VALUES
(10, 'fdsaff fdaf fdfda', '2019-04-25', '2019-04-29', 'fdsaff_fdaf_fdfda_4947492525cc18a273d079.jpg', 'Hinatuan', 'Philippines', 'Caraga');

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

CREATE TABLE `food` (
  `food_id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL,
  `food_name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `price` varchar(255) NOT NULL,
  `stock` varchar(255) NOT NULL,
  `display_image` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`food_id`, `business_id`, `food_name`, `description`, `price`, `stock`, `display_image`) VALUES
(1, 7, 'Grilled Tuna Belly', 'Wash tuna belly and pat dry. In a bowl, combine lemon juice, garlic, chili sauce, oyster sauce, sesame oil and pepper. Stir until well-blended. Add tuna and marinate in the refrigerator overnight, turning fish occasionally in the marinade. On lightly-greased grates over hot coals, arrange tuna in a single layer.', '105.50', 'In Stock', 'Grilled_Tuna_Belly_3178466685cd513f6d283b.jpg'),
(2, 7, 'Grilled Tangigue Steak', 'Season both sides of tanigue with lemon juice, salt, pepper, garlic, and olive oil. Oil and preheat a grill pan over medium-high heat.', '146', 'In Stock', 'Grilled_Tangigue_Steak_4000344695cd290dd7bd64.jpg'),
(3, 7, 'Grilled Pork Belly', 'Combine pork belly with the soy sauce, lemon, salt, ground black pepper, garlic and mix well. Marinade the pork belly for at least 3 hours', '156', 'In Stock', 'Grilled_Pork_Belly_9714427465cd2912dc6eee.jpg'),
(4, 7, 'Lechon Kawali', 'Lechon kawali, also known as lechon carajay, is crispy pork belly deep-fried in a pan or wok. It is seasoned beforehand, cooked then served chopped into pieces.', '163', 'In Stock', 'Lechon_Kawali_11429713745cd2915ce03fc.jpg'),
(5, 7, 'Chicken Adobo', 'Delicous and tasty marinated Filipino chicken adobo with soy sauce, vinegar, and garlic. This is best paired with warm rice.', '156', 'In Stock', 'Chicken_Adobo_14470139725cd2918270f4f.jpeg'),
(6, 8, 'Bitterballen', 'A round shaped beef-ragout version of croquette, typically containing a mixture of beef or veal (minced or chopped), beef broth, butter, flour for thickening, parsley, salt and pepper, resulting in a thick ragout. Most recipes include nutmeg and there are also variations utilizing curry powder or that add in finely chopped vegetables such as carrot.', '80.00', 'In Stock', ''),
(7, 8, 'Bonda', 'Various sweet and spicy versions of exist different regions. The process of making a spicy bonda involves deep-frying potato (or other vegetables) filling dipped in gram flour batter.', '75.00', 'In Stock', ''),
(8, 8, 'Cokodok', 'A traditional Malaysian fritter snack that is made with flour and banana, and is fried. It is usually round in shape and tends to vary in size.', '69.00', 'In Stock', ''),
(9, 8, 'Crêpe', 'A type of very thin pancake, usually made from wheat flour (crêpes de Froment) or buckwheat flour (galettes). The word is of French origin, deriving from the Latin crispa, meaning \"curled\".\r\n', '50.00', 'In Stock', ''),
(10, 8, 'Doughnut', 'A fried dough snack popular in most parts of the world', '20.00', 'In Stock', ''),
(11, 7, 'Kare-kare', 'Kare-kare is a Philippine stew complemented with a thick savory peanut sauce. It is made from a variation base of stewed oxtail, pork hocks, calves feet, pig feet, beef stew meat, and occasionally offal or tripe. Kare-kare can also be made with seafood or vegetables.', '148.00', 'In Stock', 'Kare-kare_18456583265cd291b8bd27a.jpg'),
(12, 10, 'Elnido', 'dfdafdsf', '23', 'In Stock', 'Elnido_16708096015cefcd91b0e7f.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `image_id` int(11) NOT NULL,
  `type` varchar(25) NOT NULL,
  `id` int(11) NOT NULL,
  `upload_date` varchar(255) NOT NULL,
  `image_name` text NOT NULL,
  `image_type` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`image_id`, `type`, `id`, `upload_date`, `image_name`, `image_type`) VALUES
(9, 'business', 9, '05-10-2019', 'Pop_Rock_21426368305cd586023e243.jpg', 'jpg'),
(10, 'business', 9, '05-10-2019', 'Pop_Rock_16171130245cd58610e16ea.jpg', 'jpg'),
(11, 'business', 9, '05-10-2019', 'Pop_Rock_9256957805cd58626238af.jpg', 'jpg'),
(13, 'business', 5, '05-11-2019', 'Plaza_Alemania__3798826675cd6f71cc3ecf.png', 'png'),
(14, 'business', 5, '05-11-2019', 'Plaza_Alemania__15571259645cd6f751570a5.jpg', 'jpg'),
(15, 'business', 5, '05-11-2019', 'Plaza_Alemania__2062309625cd6f7598d5ab.jpg', 'jpg'),
(21, 'business', 7, '05-21-2019', 'Calle_Dos_Food_Hub_11601387775ce3ce33a6c91.jpg', 'jpg'),
(22, 'business', 7, '05-21-2019', 'Calle_Dos_Food_Hub_11005727085ce3ce6803b2a.jpg', 'jpg'),
(23, 'business', 7, '05-21-2019', 'Calle_Dos_Food_Hub_15899476075ce3cea1f1cfe.jpg', 'jpg'),
(24, 'business', 7, '05-21-2019', 'Calle_Dos_Food_Hub_14763819735ce3cec551a93.jpg', 'jpg');

-- --------------------------------------------------------

--
-- Table structure for table `marker`
--

CREATE TABLE `marker` (
  `marker_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `longitude` longtext NOT NULL,
  `latitude` longtext NOT NULL,
  `name` varchar(35) NOT NULL,
  `marker_type` varchar(25) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `marker`
--

INSERT INTO `marker` (`marker_id`, `id`, `longitude`, `latitude`, `name`, `marker_type`, `status`) VALUES
(21, 5, '124.24284979999993', '8.2281718', 'Plaza Alemania Hotel', 'business', ''),
(22, 6, '124.24052710000001', '8.2186918', 'Go Hotels Iligan', 'business', ''),
(23, 7, '121.7696181', '16.9355755', 'Calle Dos Food Hub', 'business', ''),
(24, 8, '124.23893539999995', '8.2295445', 'Shann Fille Food Station', 'business', ''),
(25, 9, '124.23333330000003', '8.216666699999998', 'Pop Rock', 'business', ''),
(26, 10, '124.24678715239259', '8.225047737610698', 'Lacsi\'s Restaurant', 'business', ''),
(31, 15, '151.27700000000004', '-33.79600000000001', 'Irene\'s Eatery', 'business', '');

-- --------------------------------------------------------

--
-- Table structure for table `merchant`
--

CREATE TABLE `merchant` (
  `merchant_id` int(11) NOT NULL,
  `first_name` varchar(35) NOT NULL,
  `middle_name` varchar(35) NOT NULL,
  `surname` varchar(35) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `birthdate` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `tel_number` varchar(25) NOT NULL,
  `phone_number` varchar(25) NOT NULL,
  `email` varchar(255) NOT NULL,
  `profile_image` longtext NOT NULL,
  `merchant_access_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `merchant`
--

INSERT INTO `merchant` (`merchant_id`, `first_name`, `middle_name`, `surname`, `gender`, `birthdate`, `address`, `tel_number`, `phone_number`, `email`, `profile_image`, `merchant_access_id`) VALUES
(3, 'John', 'Gabriel', 'Doe', 'Male', '11/16/1998', 'Zone Aries 1, Suarez, Iligan City', '2234215', '09364617161', 'johndoe@gmail.com', 'darren123_12141635585cf09d984be28.jpg', 8),
(8, 'John', '', 'Doe', 'Male', '2019-05-09', 'Iligan City', '2234515', '9399023976', 'poprock@gmail.com', '', 13),
(9, 'Dante', '', 'Lacsi', 'Male', '1960-05-30', 'Iligan City', '223452214', '22348587', 'lacsirestaurant@gmail.com', '', 14),
(14, 'Irene', '', 'Paragamac', 'Male', '2019-06-04', 'ritzcen123', '09399023976', '9399023976', 'irene@gmail.com', '', 19);

-- --------------------------------------------------------

--
-- Table structure for table `merchant_access`
--

CREATE TABLE `merchant_access` (
  `merchant_access_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `last_login` varchar(255) NOT NULL,
  `ip` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `merchant_access`
--

INSERT INTO `merchant_access` (`merchant_access_id`, `username`, `email`, `password`, `last_login`, `ip`) VALUES
(8, 'darren123', 'alexlazarito@gmail.com', '77a3ba4681722fdad89cddb39bc7515be2f9f9707f23c87a6e8d32bc1d00d80efe224b1c1281e531cdce110364fcbc9cbf84bdaf00efc5e2066f41ee19aae8c47mPdgzmR5+gRTBsy7rxls/PnwR8Oj7cX8Lm6r0ip9v4=', '06-04-2019 13:52:00', '::1'),
(13, 'poprock', 'poprock@gmail.com', 'cad2b8394bdc0503e1c7632148b0ba6dfa1466c949012eb0c4f95bc66048af6af938211e4dc2c79c86108a3282927f5dc88888655ee58d5cedbd5e414f83abcbi+hGMg0u31aev4E3GYlQ/4h/L6lhpxPihI2QhE7EmHw=', '2019-05-21 02:11:13', '::1'),
(14, 'lacsi_restaurant123', 'lacsirestaurant@gmail.com', '3bc250b231aa747d0ac69d18878c2d99a111b6dcf6333bf07f3f130e4f632f4e58207eb3e23cc6b8dea4887c902ee36b9179db46e790989ce58e7f95e11e07e0idXojGXZoaOZpMUnVaTcoB3mvBVXv9Rmnkd2zLkVvME=', '05-30-2019 19:19:09', '::1'),
(19, 'irene123', 'irene@gmail.com', '94317e8d53bfb448dca1a86b406efaf9cd6603b39face907d24994a80c808b131a14afbcf7edd5702b36ec4963154863766e2e4449e1cd4ff695119641e504984a5w1RxgMPIQrXzIvvub9s5TvEbdFjd/BS9TO60VkT0=', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `order_confirmation`
--

CREATE TABLE `order_confirmation` (
  `order_confirmation_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `message` longtext NOT NULL,
  `amount` varchar(255) NOT NULL,
  `fee` varchar(255) NOT NULL,
  `amount_with_fee` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_confirmation`
--

INSERT INTO `order_confirmation` (`order_confirmation_id`, `user_id`, `transaction_id`, `payment_id`, `message`, `amount`, `fee`, `amount_with_fee`) VALUES
(6, 1, 12, 10, 'Were sorry we currently out of grilled tangigue steak and grilled pork belly....', '520', '50', '570'),
(7, 1, 13, 10, 'Were sorry we currently out of grilled tangigue steak and grilled pork belly....', '520', '50', '570'),
(8, 1, 14, 10, 'Were sorry we currently out of grilled tangigue steak and grilled pork belly....', '520', '50', '570'),
(9, 1, 15, 10, 'Were sorry we currently out of grilled tangigue steak and grilled pork belly....', '520', '50', '570');

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE `order_item` (
  `order_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL,
  `food_id` int(11) NOT NULL,
  `food_name` varchar(255) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_item`
--

INSERT INTO `order_item` (`order_id`, `transaction_id`, `business_id`, `food_id`, `food_name`, `quantity`, `amount`, `status`) VALUES
(6, 11, 8, 6, 'Bitterballen', '2', '160.00', ''),
(7, 11, 8, 7, 'Bonda', '2', '150.00', ''),
(8, 11, 8, 8, 'Cokodok', '1', '138.00', ''),
(9, 11, 8, 9, 'Crêpe', '1', '', ''),
(10, 11, 8, 10, 'Doughnut', '1', '', ''),
(12, 12, 7, 5, 'Chicken Adobo', '1', '', ''),
(13, 12, 7, 4, 'Lechon Kawali', '1', '', ''),
(14, 12, 7, 3, 'Grilled Pork Belly', '1', '', ''),
(15, 13, 7, 5, 'Chicken Adobo', '1', '', ''),
(16, 13, 7, 4, 'Lechon Kawali', '1', '', ''),
(17, 13, 7, 3, 'Grilled Pork Belly', '1', '', ''),
(18, 14, 7, 5, 'Chicken Adobo', '1', '', ''),
(19, 14, 7, 4, 'Lechon Kawali', '1', '', ''),
(20, 14, 7, 3, 'Grilled Pork Belly', '1', '', ''),
(21, 15, 7, 5, 'Chicken Adobo', '1', '', ''),
(22, 15, 7, 4, 'Lechon Kawali', '1', '', ''),
(23, 15, 7, 3, 'Grilled Pork Belly', '1', '', ''),
(24, 16, 7, 5, 'Chicken Adobo', '1', '', ''),
(25, 16, 7, 4, 'Lechon Kawali', '1', '', ''),
(26, 16, 7, 3, 'Grilled Pork Belly', '1', '', ''),
(28, 16, 7, 4, 'Lechon Kawali', '1', '', ''),
(29, 16, 7, 3, 'Grilled Pork Belly', '1', '', ''),
(30, 17, 7, 5, 'Chicken Adobo', '1', '', ''),
(31, 17, 7, 4, 'Lechon Kawali', '1', '', ''),
(32, 17, 7, 3, 'Grilled Pork Belly', '1', '', ''),
(33, 17, 7, 4, 'Lechon Kawali', '1', '', ''),
(34, 17, 7, 3, 'Grilled Pork Belly', '1', '', ''),
(35, 10, 7, 5, 'Chicken Adobo', '3', '468', ''),
(36, 10, 7, 4, 'Lechon Kawali', '2', '326', ''),
(37, 10, 7, 3, 'Grilled Pork Belly', '2', '312', '');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `payment_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `fee` float NOT NULL,
  `amount_with_fee` float NOT NULL,
  `date_of_payment` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`payment_id`, `transaction_id`, `amount`, `fee`, `amount_with_fee`, `date_of_payment`, `payment_status`, `payment_method`) VALUES
(1, 1, 200, 0, 525, '2019-04-29 5:00 PM', 'UNPAID', 'Bank'),
(2, 2, 250, 0, 525, '2019-05-2 8:00 AM', 'UNPAID', 'Bank'),
(3, 3, 300, 0, 525, '2019-05-08 2:00 PM', 'UNPAID', 'Bank'),
(4, 4, 350, 0, 525, '2019-05-12 2:00 PM', 'UNPAID', 'Bank'),
(5, 5, 375, 0, 525, '2019-05-18 2:00 PM', 'UNPAID', 'Bank'),
(6, 6, 600, 0, 525, '2019-05-20 2:00 PM', 'UNPAID', 'Bank'),
(7, 7, 550, 0, 525, '2019-04-26 2:00 PM', 'UNPAID', 'Bank'),
(8, 8, 750, 0, 525, '2019-05-20 2:00 PM', 'UNPAID', 'Bank'),
(9, 9, 450, 0, 525, '2019-04-29 2:00 PM', 'UNPAID', 'Bank'),
(11, 11, 294, 0, 525, '2019-04-29 2:00 PM', 'UNPAID', 'Bank'),
(12, 10, 1106, 0, 1106, '2019-04-29 2:00 PM', 'UNPAID', 'Bank'),
(13, 12, 475, 50, 525, '2019-04-29 2:00 PM', 'UNPAID', 'Bank'),
(14, 13, 475, 50, 525, '2019-04-29 2:00 PM', 'UNPAID', 'Bank'),
(15, 14, 475, 50, 525, '2019-04-29 2:00 PM', 'UNPAID', 'Bank'),
(16, 15, 475, 50, 525, '2019-04-29 2:00 PM', 'UNPAID', 'Bank'),
(17, 16, 475, 50, 525, '2019-04-29 2:00 PM', 'UNPAID', 'Bank'),
(19, 17, 794, 0, 794, '2019-04-29 2:00 PM', 'UNPAID', 'Bank');

-- --------------------------------------------------------

--
-- Table structure for table `plan`
--

CREATE TABLE `plan` (
  `plan_id` int(11) NOT NULL,
  `plan_type` varchar(25) NOT NULL,
  `pricing` int(11) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `plan_item_type`
--

CREATE TABLE `plan_item_type` (
  `plan_item_type_id` int(11) NOT NULL,
  `type` varchar(25) NOT NULL,
  `plan_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `plan_pricing`
--

CREATE TABLE `plan_pricing` (
  `plan_pricing_id` int(11) NOT NULL,
  `type` varchar(25) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `reservation_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `room_type` varchar(255) NOT NULL,
  `transaction_date` varchar(255) NOT NULL,
  `check_in_date` varchar(255) NOT NULL,
  `check_out_date` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `night` int(11) NOT NULL,
  `guest` int(11) NOT NULL,
  `num_adult` int(11) NOT NULL,
  `num_children` int(11) NOT NULL,
  `children_age` longtext NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`reservation_id`, `transaction_id`, `business_id`, `room_id`, `room_type`, `transaction_date`, `check_in_date`, `check_out_date`, `expiry_date`, `night`, `guest`, `num_adult`, `num_children`, `children_age`, `status`) VALUES
(1, 18, 5, 1, 'Single', '05-29-2019 15:00:00', '06-01-2014 14:00:00', '2014-06-02 14:00:00', '01-06-2014 19:03:03', 1, 3, 1, 3, 'children 1 - 2 yrs old\r\nchildren 2 - 5 yrs old\r\nchildren 3 - 8 yrs old\r\n', 'expired');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `review_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `category` varchar(25) NOT NULL,
  `title` varchar(50) NOT NULL,
  `rating` varchar(35) NOT NULL,
  `description` longtext NOT NULL,
  `date` date NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE `room` (
  `room_id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL,
  `room_type` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `max_guest` int(11) NOT NULL,
  `room_rate_type` varchar(255) NOT NULL,
  `room_status` varchar(255) NOT NULL,
  `display_image` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`room_id`, `business_id`, `room_type`, `description`, `max_guest`, `room_rate_type`, `room_status`, `display_image`) VALUES
(1, 5, 'Single ', '                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                Our single rooms are ideal for one person. Our single rooms overlook the street, and all have double glazing. Single rooms have a single bed (120cm), en-suite bathroom with bath and toilet. For children under the age of 2, cots are available and can be requested at the time of booking.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                ', 2, 'per hour', 'Available', 'Single__12303349185ce897c9a07ad.jpg'),
(2, 5, 'Double', '                                                                                                                                                                A room assigned to two people. May have one or more beds.\r\n\r\nThe room size or area of Double Rooms are generally between 40 m² to 45 m²                                                                                                                                                        ', 4, 'per hour', 'Available', 'Double_15907905875ce8974c11260.jpg'),
(3, 5, 'Triple', '                                                                                                                                                                                                                                                                                                                                                                                                                A room that can accommodate three persons and has been fitted with three twin beds, one double bed and one twin bed or two double beds.\r\n\r\nThe room size or area of Triple Rooms are generally between 45 m² to 65 m².                                                                                                                                                                                                                                                                                                                                                                                            ', 2, 'per hour', 'Available', 'Triple_14220968025ce89846d9eb7.jpg'),
(4, 5, 'Quad', '                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        A room assigned to four people. May have two or more beds.\r\n\r\nThe room size or area of Quad Rooms are generally between 70 m² to 85 m².                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', 2, 'per hour', 'Available', 'Quad_16467223635ce89ceb61936.jpg'),
(5, 5, 'Queen', '                                                                                                                                                                                                                                                                                                                                                                                                                A room with a queen-sized bed. May be occupied by one or more people.\r\n\r\nThe room size or area of Queen Rooms are generally between 32 m² to 50 m².                                                                                                                                                                                                                                                                                                                                                                                            ', 2, 'per hour', 'Available', 'Queen_8410061915ce8a89e5a9d2.jpg'),
(6, 6, 'Premier', '1 Queen-sized bed, arm chair', 2, 'per night', '', ''),
(7, 6, 'Deluxe', ' 1 Queen-sized bed, patio chairs with open air patio', 2, 'per night', '', ''),
(8, 6, 'Studio', '1 Queen-sized bed & 1 single mattress', 2, 'per night', '', ''),
(9, 6, 'Superior', '1 Queen-sized bed', 2, 'per night', '', ''),
(10, 6, 'Twin', '2 single-beds', 2, 'per night', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `room_image`
--

CREATE TABLE `room_image` (
  `room_image_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `upload_date` longtext NOT NULL,
  `image_name` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `room_rate`
--

CREATE TABLE `room_rate` (
  `room_rate_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `room_type` varchar(255) NOT NULL,
  `room_rate_type` varchar(255) NOT NULL,
  `rate` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_rate`
--

INSERT INTO `room_rate` (`room_rate_id`, `room_id`, `room_type`, `room_rate_type`, `rate`, `price`) VALUES
(1, 1, 'Single', 'per hour', '6 hours', 80),
(32, 1, 'Single', 'per hour', '12 hours', 160),
(33, 2, 'Double', 'per hour', '6 hours', 90),
(34, 2, 'Double', 'per hour', '12 hours', 180),
(35, 3, 'Triple', 'per hour', '6 hours', 100),
(36, 3, 'Triple', 'per hour', '12 hours', 200),
(37, 4, 'Quad', 'per hour', '6 hours', 120),
(38, 4, 'Quad', 'per hour', '12 hours', 240),
(39, 5, 'Queen', 'per hour', '6 hours', 280),
(40, 5, 'Queen', 'per hour', '12 hours', 560);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `setting_id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL,
  `business_type` varchar(255) NOT NULL,
  `delivery` varchar(255) NOT NULL,
  `pick_up` varchar(255) NOT NULL,
  `dine_in` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`setting_id`, `business_id`, `business_type`, `delivery`, `pick_up`, `dine_in`) VALUES
(2, 15, 'Foods & Restaurants', 'disabled', 'enable', 'enable'),
(3, 7, 'Foods & Restaurants', 'Paid', 'disabled', 'enabled');

-- --------------------------------------------------------

--
-- Table structure for table `station`
--

CREATE TABLE `station` (
  `station_id` int(11) NOT NULL,
  `station_type` varchar(25) NOT NULL,
  `station_name` varchar(35) NOT NULL,
  `station_address` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `display_image` longtext NOT NULL,
  `email` varchar(255) NOT NULL,
  `tel_number` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `station`
--

INSERT INTO `station` (`station_id`, `station_type`, `station_name`, `station_address`, `description`, `display_image`, `email`, `tel_number`, `phone_number`) VALUES
(1, 'Port Station', 'Cortes Municipal Police Station ', 'National Road, San Miguel, 8301 Surigao del Sur, Philippines', 'afdasfdsaf', 'Cortes_Municipal_Police_Station__16604676235cc26b9a0f37a.png', 'ldanray01@gmail.com', '09399023976', '9088602814');

-- --------------------------------------------------------

--
-- Table structure for table `subscription_payment`
--

CREATE TABLE `subscription_payment` (
  `subscription_payment_id` int(11) NOT NULL,
  `total` decimal(11,0) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `payment_status` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `touristspot`
--

CREATE TABLE `touristspot` (
  `touristspot_id` int(11) NOT NULL,
  `touristspot_name` varchar(35) NOT NULL,
  `tel_number` varchar(15) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `email` varchar(255) NOT NULL,
  `touristspot_address` varchar(25) NOT NULL,
  `description` longtext NOT NULL,
  `display_image` longtext NOT NULL,
  `multi_category` varchar(25) NOT NULL,
  `category` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `touristspot`
--

INSERT INTO `touristspot` (`touristspot_id`, `touristspot_name`, `tel_number`, `phone_number`, `email`, `touristspot_address`, `description`, `display_image`, `multi_category`, `category`) VALUES
(1, 'Maria Christina Falls', '2313213', '21312312', 'alexlazarito@gmail.com', '50 Hunter St, Sydney NSW ', 'fdasdf', 'Maria_Christina_Falls_6949692805cc26c5bbade1.png', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `transaction_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL,
  `transaction_type` varchar(255) NOT NULL,
  `business_type` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL,
  `date_of_transaction` varchar(255) NOT NULL,
  `note` longtext NOT NULL,
  `status` varchar(255) NOT NULL,
  `user_confirmation_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`transaction_id`, `user_id`, `business_id`, `transaction_type`, `business_type`, `payment_status`, `date_of_transaction`, `note`, `status`, `user_confirmation_status`) VALUES
(1, 1, 5, 'booking', 'Accommodation', 'UNPAID', '2019-04-30 1:29 PM', '', '', '0'),
(2, 1, 5, 'booking', 'Accommodation', 'UNPAID', '2019-04-30 1:29 PM', '', '', '0'),
(3, 1, 5, 'booking', 'Accommodation', 'PAID', '2019-04-30 1:29 PM', '', '', '0'),
(4, 1, 5, 'booking', 'Accommodation', 'PAID', '2019-04-30 1:29 PM', '', '', '0'),
(5, 1, 5, 'booking', 'Accommodation', 'PAID', '2019-04-30 1:29 PM', '', '', '0'),
(6, 1, 5, 'booking', 'Accommodation', 'UNPAID', '2019-04-30 1:29 PM', '', '', '0'),
(7, 1, 5, 'booking', 'Accommodation', 'UNPAID', '2019-04-30 1:29 PM', '', '', '0'),
(8, 1, 5, 'booking', 'Accommodation', 'PAID', '2019-04-30 1:29 PM', '', '', '0'),
(10, 1, 7, 'order', 'Foods & Restaurants', 'PAID', '2019-04-30 1:29 PM', 'Please add extra hot sauce, and extra 2 straws', '', '0'),
(11, 1, 7, 'order', 'Foods & Restaurants', 'UNPAID', '2019-04-30 1:29 PM', '', '', '0'),
(12, 1, 7, 'order', 'Foods & Restaurants', 'UNPAID', '2019-04-30 1:29 PM', 'Please add extra hot sauce, and extra 2 straws', 'Pending', '0'),
(13, 1, 7, 'order', 'Foods & Restaurants', 'UNPAID', '2019-04-30 1:29 PM', 'Please add extra hot sauce, and extra 2 straws', 'preparing', '0'),
(14, 1, 7, 'order', 'Foods & Restaurants', 'UNPAID', '2019-04-30 1:29 PM', 'Please add extra hot sauce, and extra 2 straws', 'Preparing', '0'),
(15, 1, 7, 'order', 'Foods & Restaurants', 'UNPAID', '2019-04-30 1:29 PM', 'Please add extra hot sauce, and extra 2 straws', 'delivering', '0'),
(16, 1, 7, 'order', 'Foods & Restaurants', 'UNPAID', '2019-04-30 1:29 PM', 'Please add extra hot sauce, and extra 2 straws', 'Completed', '0'),
(17, 1, 7, 'order', 'Foods & Restaurants', 'UNPAID', '2019-04-30 1:29 PM', 'Please add extra hot sauce, and extra 2 straws', 'Cancelled', '0'),
(18, 1, 5, 'reservation', 'Accmomodation', 'UNPAID', '2019-04-30 1:29 PM', '', 'reserved', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contact_number` varchar(255) NOT NULL,
  `profile_image` varchar(255) NOT NULL,
  `user_access_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `address`, `contact_number`, `profile_image`, `user_access_id`, `email`) VALUES
(1, 'Dan Ray', 'Lacsi', 'Iligan City', '9088602814', '', 2, 'alexlazarito@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `user_access`
--

CREATE TABLE `user_access` (
  `user_access_id` int(11) NOT NULL,
  `firebase_uid` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `last_login` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_access`
--

INSERT INTO `user_access` (`user_access_id`, `firebase_uid`, `username`, `password`, `email`, `last_login`) VALUES
(1, '', 'edgar123', '2266493c2cfe5bad29ea630ed8c9f58b0cc1f6ec44c1dfc22dbf318395c5f7630c41c566bf001702ab91106268368cfc108f91cc6b58a87e1cb41bbee110d171oWrvXnXt259fZjOHHRVD4fH46Bk34Du8ddzggLsmPqc=', 'alexlazarito@gmail.com', '2019-05-09 11:47:19'),
(2, '', 'ritzcen123', 'cba9be02dfb339410c084b7bda55f6b025c8ecc8dcf9cdd48cf0f8296b57752074f024f7b8bc9a9ef2d1d03b01b4a0642c9084d636ab0455f35a9e9fc07428debweOBJ500YhVcAa+OC8gp3/z813VxEbi6X3my0pHkns=', 'alexlazarito@gmail.com', '2019-05-09 12:29:04');

-- --------------------------------------------------------

--
-- Table structure for table `user_device`
--

CREATE TABLE `user_device` (
  `user_device_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_token` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`booking_id`);

--
-- Indexes for table `branch_category`
--
ALTER TABLE `branch_category`
  ADD PRIMARY KEY (`branch_category_id`);

--
-- Indexes for table `business`
--
ALTER TABLE `business`
  ADD PRIMARY KEY (`business_id`);

--
-- Indexes for table `business_subscription_plan`
--
ALTER TABLE `business_subscription_plan`
  ADD PRIMARY KEY (`subscription_id`);

--
-- Indexes for table `child_event`
--
ALTER TABLE `child_event`
  ADD PRIMARY KEY (`child_event_id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `food`
--
ALTER TABLE `food`
  ADD PRIMARY KEY (`food_id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `marker`
--
ALTER TABLE `marker`
  ADD PRIMARY KEY (`marker_id`);

--
-- Indexes for table `merchant`
--
ALTER TABLE `merchant`
  ADD PRIMARY KEY (`merchant_id`);

--
-- Indexes for table `merchant_access`
--
ALTER TABLE `merchant_access`
  ADD PRIMARY KEY (`merchant_access_id`);

--
-- Indexes for table `order_confirmation`
--
ALTER TABLE `order_confirmation`
  ADD PRIMARY KEY (`order_confirmation_id`);

--
-- Indexes for table `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `plan`
--
ALTER TABLE `plan`
  ADD PRIMARY KEY (`plan_id`);

--
-- Indexes for table `plan_item_type`
--
ALTER TABLE `plan_item_type`
  ADD PRIMARY KEY (`plan_item_type_id`);

--
-- Indexes for table `plan_pricing`
--
ALTER TABLE `plan_pricing`
  ADD PRIMARY KEY (`plan_pricing_id`);

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`reservation_id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`review_id`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`room_id`);

--
-- Indexes for table `room_image`
--
ALTER TABLE `room_image`
  ADD PRIMARY KEY (`room_image_id`);

--
-- Indexes for table `room_rate`
--
ALTER TABLE `room_rate`
  ADD PRIMARY KEY (`room_rate_id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `station`
--
ALTER TABLE `station`
  ADD PRIMARY KEY (`station_id`);

--
-- Indexes for table `subscription_payment`
--
ALTER TABLE `subscription_payment`
  ADD PRIMARY KEY (`subscription_payment_id`);

--
-- Indexes for table `touristspot`
--
ALTER TABLE `touristspot`
  ADD PRIMARY KEY (`touristspot_id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`transaction_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_access`
--
ALTER TABLE `user_access`
  ADD PRIMARY KEY (`user_access_id`);

--
-- Indexes for table `user_device`
--
ALTER TABLE `user_device`
  ADD PRIMARY KEY (`user_device_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `booking_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `branch_category`
--
ALTER TABLE `branch_category`
  MODIFY `branch_category_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `business`
--
ALTER TABLE `business`
  MODIFY `business_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `business_subscription_plan`
--
ALTER TABLE `business_subscription_plan`
  MODIFY `subscription_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `child_event`
--
ALTER TABLE `child_event`
  MODIFY `child_event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `food`
--
ALTER TABLE `food`
  MODIFY `food_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `marker`
--
ALTER TABLE `marker`
  MODIFY `marker_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `merchant`
--
ALTER TABLE `merchant`
  MODIFY `merchant_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `merchant_access`
--
ALTER TABLE `merchant_access`
  MODIFY `merchant_access_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `order_confirmation`
--
ALTER TABLE `order_confirmation`
  MODIFY `order_confirmation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `order_item`
--
ALTER TABLE `order_item`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `plan`
--
ALTER TABLE `plan`
  MODIFY `plan_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `plan_item_type`
--
ALTER TABLE `plan_item_type`
  MODIFY `plan_item_type_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `plan_pricing`
--
ALTER TABLE `plan_pricing`
  MODIFY `plan_pricing_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `reservation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
  MODIFY `room_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `room_image`
--
ALTER TABLE `room_image`
  MODIFY `room_image_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `room_rate`
--
ALTER TABLE `room_rate`
  MODIFY `room_rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `station`
--
ALTER TABLE `station`
  MODIFY `station_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subscription_payment`
--
ALTER TABLE `subscription_payment`
  MODIFY `subscription_payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `touristspot`
--
ALTER TABLE `touristspot`
  MODIFY `touristspot_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_access`
--
ALTER TABLE `user_access`
  MODIFY `user_access_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_device`
--
ALTER TABLE `user_device`
  MODIFY `user_device_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
