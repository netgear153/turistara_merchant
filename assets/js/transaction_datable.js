
$.ajax({
    url: site_url + "business/fetch_transaction",
    method: 'POST',
    data: {'transaction_type': transaction_type, 'business_type': business_type, 'status': status },
    dataType: 'json',
    success: function(response) {

        // HIDES TRANSACTION STATUS COLOUMN
        if(status == ''){
            var visible = "false";

        } else {
            var visible = "true";
        }

        //------ removes the error message of the data table --------//
        $.fn.dataTable.ext.errMode = 'none';

        $('#dataTable').dataTable({
            responsive: true,
            data: response.data,
            destroy: true,
            columns: [{
                    'data': 'transaction_id',
                    'sortable': true
                },
                {
                    'data': 'first_name',
                    'sortable': true
                },
                {
                    'data': 'last_name',
                    'sortable': true
                },
                {
                    'data': 'date_of_transaction',
                    'sortable': true,
                },
                {
                    'data': 'payment_method',
                    'sortable': true,
                },
                {
                    'data': 'status',
                    'sortable': true,
                    'render': function(status) {
                        if (visible == 'false'){
                            $('#remove_status').remove();
                        } else{
                        return status
                        }
                    }
                },

                {
                    'data': 'transaction_id',
                    'render': function(transaction_id) {
                        if (status == 'reserved'){
                        return  '<div class="btn-group dropdown">'+
                                '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>'+
                                '<ul class="dropdown-menu">'+
                                '<a class="dropdown-item " id="' + transaction_id + '" onclick="complete(' + transaction_id + ');">Complete</a>'+
                                '<a class="dropdown-item " id="'+transaction_id+'" onclick="view_transaction_details('+transaction_id+');" >View Details</a>'+
                                '</ul>'+
                                '</div>';
                        }

                        if (status == 'confirmed'){

                        return  '<div class="btn-group">'+
                                '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>'+
                                '<ul class="dropdown-menu">'+
                                '<a class="dropdown-item " id="'+transaction_id+'" onclick="update_order_transaction('+transaction_id+');">&nbsp;&nbsp;Prepare</a>'+
                                '<a class="dropdown-item " id="'+transaction_id+'" onclick="view_transaction_details('+transaction_id+');" >&nbsp;&nbsp;View Details</a>'+
                                '</ul>'+
                                '</div>';
                               
                        }

                        if (status == 'preparing'){

                        return  '<div class="btn-group">'+
                                '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>'+
                                '<ul class="dropdown-menu">'+
                                '<a class="dropdown-item " id="'+transaction_id+'" onclick="update_order_transaction('+transaction_id+');" >&nbsp;&nbsp;Deliver Order</a>'+
                                '<a class="dropdown-item " id="'+transaction_id+'"  onclick="view_transaction_details('+transaction_id+');" >&nbsp;&nbsp;View Details</a>'+
                                '</ul>'+
                                '</div>'; 
                                  
                        }

                        if (status == 'delivering'){

                        return  '<div class="btn-group">'+
                                '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Action</button>'+
                                '<ul class="dropdown-menu">'+
                                '<a class="dropdown-item " id="'+transaction_id+'"  onclick="update_order_transaction('+transaction_id+');" >&nbsp;&nbsp;Complete</a>'+
                                '<a class="dropdown-item " id="'+transaction_id+'"  onclick="view_transaction_details('+transaction_id+');" >&nbsp;&nbsp;View Details</a>'+
                                '</ul>'+
                                '</div>' ;
                        }

                        return '<div class="text-center"> <button type="button" name="view_booking" id="' + transaction_id + '" onclick="view_transaction_details(' + transaction_id + ');" class="btn btn-primary waves-effect m-r-20 f-w-600 view_booking">View</button></div>';

                    },
                    'sortable': false
                }

            ]
        });
    },
    error: function(error) {
        console.log(error)
    }
});
