
function view_transaction_details(transaction_id){

        $('#dataModal').modal("show");
        $('.remove_append').remove();


        // ajax for customer details
        $.ajax({
                    url: site_url+"Business/fetch_customer_details",
                    type: "POST",
                    data: {'transaction_id': transaction_id},
                    dataType: 'json',
                    success: function(data) {
                                    
                      $('#first_name').text(data.first_name);
                      $('#last_name').text(data.last_name);
                      $('#username').text(data.username);
                      $('#address').text(data.address);
                      $('#contact_number').text(data.contact_number);
                      $('#email').text(data.email);

                      global_transaction_id = transaction_id ;
                       
                    }
                });

        // ajax for reservation with rooms
        $.ajax({
                    url: site_url+"Business/fetch_reservation_room_details",
                    type: "POST",
                    data: {'transaction_id': transaction_id},
                    dataType: 'json',
                    success: function(data) {

                    //--- REMOVES EXPIRY TIME INTERVAL IN RESERVATION DETAILS----//
                       if(status != ""){ 
                         $('.remove_time_interval').remove();
                        }
                    //-----ENDS HERE ---------//
                            
                      $('#check_in_date').text(data.check_in_date);
                      $('#check_out_date').text(data.check_out_date);
                      $('#night').text(data.night);
                      $('#guest').text(data.guest);
                      $('#num_of_adult').text(data.num_adult);
                      $('#num_of_children').text(data.num_children);
                      $('#children_age').text(data.children_age);
                      $('#display_image').attr("src", site_url+"images/room/"+data.display_image);


                      var num_children = data.num_children;

                      //----------REMOVES NUMBER OF CHILDREN IN RESERVATION DETAILS------///
                      if( num_children == '0'){
                        $('#children_age').closest("tr").remove();
                      }

                      $('#room_type').text(data.room_type);
                      $('#description').text(data.description);
                      $('#max_guest').text(data.max_guest);

                        $.ajax({
                            url: site_url+"business/fetch_room_rate",
                            type: "POST",
                            data: {'room_id': data.room_id},
                            dataType: 'json',
                            success: function(data) {

                            room_rate_value = [];

                            $.each(data, function(index, value) {                          
                                                
                                 $('#room_rate').append(
                                                '<tr class="remove_append">'+
                                                '<th scope="row" >Per '+value.rate +'</th>'+
                                                '<td><span>&#8369;'+value.price+'</span></td>'+
                                                '</tr>');
                            });

                            }
                          });

                    }

                });


         
}