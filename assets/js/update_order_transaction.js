function update_order_transaction(transaction_id){

        Swal.fire({
              title: sweetalert_title,
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: sweetalert_confirm
            }).then((result) => {
                  if (result.value) {

               $.ajax({
                  url: site_url+"Business/update_order_transaction",
                  type: "POST",
                  data: {'transaction_id': transaction_id, 'transaction_status': transaction_status},
                  dataType: 'json',
                  success: function(data) {

                  Swal.fire({
                                title: "success!",
                                text: sweetalert_success,
                                type: "success"
                            }).then(function() {
                                window.location = site_url+'Business/'+success_direct;
                            });          
                  },
                      
                  error: function (response) {
                         
                          Swal.fire({
                                title: "Error!",
                                text: sweetalert_error,
                                type: "error"
                            }).then(function() {
                                window.location = site_url+'Business/'+error_direct;
                            });

                    }
            
                });
                  }
            });

}