$('.delete_image').click(function(e){

e.preventDefault();

var link = $(this).attr('href');

   Swal.fire({
      title: 'Are you sure to delete this image?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
          if (result.value) {
                window.location.href = link;
            }
        });

});