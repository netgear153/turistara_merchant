

function view_transaction_details(transaction_id){

        global_transaction_id = transaction_id ;

        $('#table_for_orders tbody').empty();
        $('#dataModal').modal("show");

        // ajax for customer details
        $.ajax({
                    url: site_url+"Business/fetch_customer_details",
                    type: "POST",
                    data: {'transaction_id': transaction_id},
                    dataType: 'json',
                    success: function(data) {
                                    
                      $('#first_name').text(data.first_name);
                      $('#last_name').text(data.last_name);
                      $('#username').text(data.username);
                      $('#address').text(data.address);
                      $('#contact_number').text(data.contact_number);
                      $('#email').text(data.email);

                      
                      global_user_id = data.user_id ;
                       
                    }
                });


        // ajax for customer payment
        $.ajax({
                    url: site_url+"Business/fetch_payment",
                    type: "POST",
                    data: {'transaction_id': transaction_id},
                    dataType: 'json',
                    success: function(data) {

                      $('#payment_id').text(data.payment_id);              
                      $('#payment_method').text(data.payment_method);
                      $('#date_of_payment').text(data.date_of_payment);
                      $('#payment_status').text(data.payment_status);
                      $('#amount').text(data.amount);
                      $('#delivery_fee').val(data.fee);
                      $('#amount_with_fee').text(data.amount_with_fee);
                      $('#note').text(data.note);

                       global_payment_id = data.payment_id;
                       global_amount = data.amount ;
                       global_delivery_fee = data.fee ;
                       global_amount_with_fee = data.amount_with_fee ;
                       
                    }
                });

        // ajax for delivery settings
        $.ajax({
                    url: site_url+"Business/fetch_setting",
                    type: "POST",
                    dataType: 'json',
                    success: function(data) {

                    if(data.delivery == "Free"){

                     $('#delivery_fee').clone().attr('type','text').val('Free').insertAfter('#delivery_fee').prev().remove();
                     $('#delivery_fee').prop('readonly',true);

                    }
                       
                    }
                });

        // ajax for customer payment
        if(status != ""){
        $.ajax({
                    url: site_url+"Business/fetch_order_confirmation",
                    type: "POST",
                    data: {'transaction_id': transaction_id},
                    dataType: 'json',
                    success: function(data) {

                      $('#message').text(data.message); 

                      $('#message').attr('readonly', true);
                      $('#delivery_fee').attr('readonly', true);   

                     
                    }
                });
          }

        // ajax for orders
        $.ajax({
                    url: site_url+"Business/fetch_order",
                    type: "POST",
                    data: {'transaction_id': transaction_id},
                    dataType: 'json',
                    success: function(data) {

                     $.each(data, function(i, value) {

                      global_order_id.push(value.order_id);

                      //-----creates set of objects to be used in updating payment amount---//
                      //--This is to minimize input hidden, instead it creates a set of objects--//
                      //--And acccesed by a function--//
                      global_quantity_amount[value.order_id] = value.amount;
                      global_updated_amount[value.order_id] = value.amount;
                      global_updated_quantity[value.order_id] = value.quantity;
                      global_cancelled_order[value.order_id] = '';

                      //Display food image
                      if (value.display_image != '') {
                          display_image = '<img src="' + site_url + 'images/food/' + value.display_image + '" class="img-fluid" alt="tbl" >';
                      } else {
                          display_image = '<label class="text-danger">' + 'No Image' + '</label>';
                      }


                      //Display for food name
                      food_name = '<h5>' + value.food_name + '</h5>' +
                          '<span style="display:block;text-overflow: ellipsis;width:500px;overflow: hidden; white-space: nowrap;">' + value.description +
                          '</span>';



                      //Display for food stock availability
                      if (value.stock == 'In Stock') {
                          stock = '<label class="text-success">' + value.stock + '</label>';
                      } else {
                          stock = '<label class="text-danger">' + value.stock + '</label>';
                      }

                      //Displays number of food quantity
                      quantity = '<center><input type="number" value="' + value.quantity + '" data-value="' + value.order_id +
                          '" data-old-quantity="' + value.quantity +
                          '" style="text-align: center; width:  50px; border:none; background: transparent;" pattern="/^-?\d+\.?\d*$/" ' +
                          'oninput="order_quantity(this);" min="1" max="' + value.quantity + '" onkeydown="return false"></center>';

                      //Displays number of food amount
                      amount = '&#8369; <span id="amount_' + value.order_id + '">' + value.amount + '</span>';

                      //Displays number of food amount
                      if (value.status == "") {
                          status = '<button data-text-swap="Cancelled" onclick="cancel_order(this);" class="cancel" value="' + value.order_id +
                              '" >Cancel </button>';
                      } else {
                          //--------- THE PURPOSE OF THIS IS TO ALIGN THE AMOUNT AND TOTAL AMOUNT..:) ---------->
                          status = '<button style="color: transparent; background-color: transparent; border-color: transparent; cursor: default;" disabled >Cancel </button>';
                      }

                      var $tr = $('<tr>').append(
                          $('<td class="pro-list-img">').html(display_image),
                          $('<td>').html(food_name),
                          $('<td>').html(stock),
                          $('<td>').html(quantity),
                          $('<td>').html(amount),
                          $('<td>').html(status)
                      ).appendTo('#table_for_orders tbody');
                  });
           
                    }
                });
         
}