
 $.ajax({
        url: site_url+"Business/fetch_sidebar",
        method: 'POST',
        dataType: 'json',
        success: function(data) {

            var accommodation = data.accommodation;
            var food = data.food;

            if( food == 'none'){
            	$("#li_foods").empty();
            	$("#li_order").empty();

            }

            $("#li_booking").empty();
            
            if( accommodation == 'none'){
            	$("#li_booking").empty();
                $("#li_accomodation_reservation").empty();
            	$("#li_rooms").empty();
            }

        },
    });

//--- UPDATES EVERYTIME FOR EXPIRATION OF RESERVED RESERVATION --//
  $.ajax({
        url: site_url+"Business/update_expire_reservation",
        method: 'POST',
        dataType: 'json',
        success: function(data) {

            console.log(data);
        },
    });

