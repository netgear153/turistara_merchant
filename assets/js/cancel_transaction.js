function cancel_transaction() {

    var transaction_id = global_transaction_id;
    var amount = '0';
    var delivery_fee = '0';
    var amount_with_fee = '0';
    var message = $('#message').val();
    var payment_id = global_payment_id;
    var user_id = global_user_id;
    var transaction_status = 'Cancelled';

    if (message == '') {

        $('#error_message').text('Please explain why you cancelled all orders.');
        return false;

    }

    $('#dataModal').modal('hide');

    Swal.fire({
        title: 'Are you sure to cancel this transaction?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, cancel transaction!'
    }).then((result) => {
        if (result.value) {

            $.ajax({
                url: site_url + "Business/cancel_order",
                type: "POST",
                data: {
                    'transaction_id': transaction_id,
                    'transaction_status': transaction_status,
                    'user_id': user_id,
                    'message': message,
                    'payment_id': payment_id,
                    'amount': amount,
                    'fee': delivery_fee,
                    'amount_with_fee': amount_with_fee,
                },
                dataType: 'json',
                success: function(data) {

                    Swal.fire({
                        title: "success!",
                        text: "The order has been cancelled.",
                        type: "success"
                    }).then(function() {
                        window.location = site_url + 'Business/order';
                    });
                },

                error: function(data) {

                    console.log(data);

                    Swal.fire({
                          title: "Error!",
                          text: "The order could not be cancel. Please try again.",
                          type: "error"
                      }).then(function() {
                          window.location = site_url+'Business/order';
                      });

                }

            });
        }
    });

}