
//Find array value and delete it 
Array.prototype.remove = function(value) {
var idx = this.indexOf(value);
if (idx != -1) {
    return this.splice(idx, 1); // The second parameter is the number of elements to remove.
}
return false;
}

//example :  global_cancelled_order_id.remove(order_id);