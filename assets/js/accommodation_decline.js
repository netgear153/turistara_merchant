
function decline() {

    var transaction_status = 'declined';

    $('#dataModal').modal('hide');

    Swal.fire({
        title: 'Are you sure to decline this reservation?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, decline reservation!'
    }).then((result) => {
        if (result.value) {

            $.ajax({
                url: site_url + "Business/decline_reservation",
                type: "POST",
                data: {
                    'transaction_id': global_transaction_id,
                    'transaction_status': transaction_status
                },
                dataType: 'json',
                success: function(data) {

                    Swal.fire({
                        title: "success!",
                        text: "The transaction has been declined.",
                        type: "success"
                    }).then(function() {
                        window.location = site_url + 'Business/accommodation_declined';
                    });
                },

                error: function(request, status, error) {

                    Swal.fire({
                        title: "Error!",
                        text: "The transaction could not be decline. Please try again.",
                        type: "error"
                    }).then(function() {
                        window.location = site_url + 'Business/accommodation_reservation';
                    });

                }

            });
        }
    });

}