
function reserve() {

    var transaction_status = "reserved";



    var hours_interval = parseFloat($('#hours_interval').val()) || 0;
    var minutes_interval = parseFloat($('#minutes_interval').val()) || 0;
    var seconds_interval = parseFloat($('#seconds_interval').val()) || 0;
    $('#dataModal').modal('hide');

    $.ajax({
        url: site_url + "Business/reserve_reservation",
        type: "POST",
        data: {
            'transaction_id': global_transaction_id,
            'transaction_status': transaction_status,
            'expiry_hours_interval': hours_interval,
            'expiry_minutes_interval': minutes_interval,
            'expiry_seconds_interval': seconds_interval
        },
        dataType: 'json',
        success: function(data) {

           Swal.fire({
                title: "Success!",
                text: "The transaction has been reserved.",
                type: "success"
            }).then(function() {
                window.location = site_url + 'Business/accommodation_reserved';
            });

        },

        error: function(request, status, error) {
            alert(request.responseText);

            Swal.fire({
                title: "Error!",
                text: "The transaction could not be reserve. Please try again.",
                type: "error"
            }).then(function() {
                window.location = site_url + 'Business/accommodation_reservation';
            });

        }

    });
}