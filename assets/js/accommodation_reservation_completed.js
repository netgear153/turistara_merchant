
function complete(transaction_id) {

    var transaction_status = 'completed';

    $('#dataModal').modal('hide');

    Swal.fire({
        title: 'Are you sure that this reservation is successful?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, this reservation is successful!'
    }).then((result) => {
        if (result.value) {

            $.ajax({
                url: site_url + "Business/update_order_transaction",
                type: "POST",
                data: {
                    'transaction_id': transaction_id,
                    'transaction_status': transaction_status
                },
                dataType: 'json',
                success: function(data) {

                    Swal.fire({
                        title: "success!",
                        text: "This reservation is complete.",
                        type: "success"
                    }).then(function() {
                        window.location = site_url + 'Business/accommodation_reservation_completed';
                    });
                },

                error: function(request, status, error) {

                    Swal.fire({
                        title: "Error!",
                        text: "The transaction could not be decline. Please try again.",
                        type: "error"
                    }).then(function() {
                        window.location = site_url + 'Business/accommodation_reservation';
                    });

                }

            });
        }
    });

}