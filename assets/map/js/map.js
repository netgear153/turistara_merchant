function initMap(){var map=new google.maps.Map(document.getElementById('map'),{center:{lat:-33.8688,lng:151.2195},zoom:15});infoWindow=new google.maps.InfoWindow;if(navigator.geolocation){navigator.geolocation.getCurrentPosition(function(position){var pos={lat:position.coords.latitude,lng:position.coords.longitude};map.setCenter(pos);},function(){handleLocationError(true,infoWindow,map.getCenter());});}else{handleLocationError(false,infoWindow,map.getCenter());}var input=document.getElementById('pac-input');var autocomplete=new google.maps.places.Autocomplete(input);autocomplete.bindTo('bounds',map);var infowindow=new google.maps.InfoWindow();var infowindowContent=document.getElementById('infowindow-content');infowindow.setContent(infowindowContent);var marker=new google.maps.Marker({map:map,anchorPoint:new google.maps.Point(0,-29),draggable:true});google.maps.event.addListener(marker,'dragend',function(){var place=autocomplete.getPlace();document.getElementById('business_address').value=place.formatted_address;document.getElementById('latitude').value=marker.getPosition().lat();document.getElementById('longitude').value=marker.getPosition().lng();});autocomplete.addListener('place_changed',function(){infowindow.close();marker.setVisible(false);var place=autocomplete.getPlace();if(!place.geometry){window.alert("No details available for input: '"+place.name+"'");return;}if(place.geometry.viewport){map.fitBounds(place.geometry.viewport);}else{map.setCenter(place.geometry.location);map.setZoom(35);}marker.setPosition(place.geometry.location);marker.setVisible(true);var address='';if(place.address_components){address=[(place.address_components[0]&&place.address_components[0].short_name||''),(place.address_components[1]&&place.address_components[1].short_name||''),(place.address_components[2]&&place.address_components[2].short_name||'')].join(' ');}document.getElementById('latitude').value=place.geometry.location.lat();document.getElementById('longitude').value=place.geometry.location.lng();var len=place.address_components.length;document.getElementById('business_address').value=place.formatted_address;if(len==5){document.getElementById('country').value=place.address_components[(len-1)].long_name;document.getElementById('city').value=place.address_components[1].long_name;document.getElementById('region').value=place.address_components[(len-2)].long_name;}else if(len==4){document.getElementById('country').value=place.address_components[(len-1)].long_name;document.getElementById('city').value=place.address_components[0].long_name;document.getElementById('region').value=place.address_components[(len-2)].long_name;}else if(len==6){document.getElementById('country').value=place.address_components[(len-2)].long_name;document.getElementById('city').value=place.address_components[1].long_name;document.getElementById('region').value=place.address_components[(len-3)].long_name;}input.value="";});}
var locations = [];
var labels = [];
var icons = [];


//   firebase.database().ref('markers').on('child_added', function(markerSnap) {
//       var type = markerSnap.val().type;
//       var option = markerSnap.val().emergency_option;
//       if(type == "Restaurant"){
//         new google.maps.Marker({
//           position: {lat:parseFloat(markerSnap.val().latitude), lng:parseFloat(markerSnap.val().longitude)},
//           map: map,
//           icon: markerIcon[type].icon,
//           title: markerSnap.val().type
//         });
//       }
//   });

function generalMap() {

    firebase.database().ref('markers').on('value', function(markerSnap) {

        markerSnap.forEach((markerItem) => {
            var type = markerItem.val().type;
            var option = markerItem.val().emergency_option;
            if(type == "Restaurant"){
                var obj = {};
                obj.lat = parseFloat(markerItem.val().latitude);
                obj.lng = parseFloat(markerItem.val().longitude);
                locations.push(obj);
                var title = markerItem.val().name;
                var icon = "http://localhost:1000/icons/restaurant.png";
                labels.push(title);
                icons.push(icon);
            } else if(type == "Pizza") {
                var obj = {};
                obj.lat = parseFloat(markerItem.val().latitude);
                obj.lng = parseFloat(markerItem.val().longitude);
                locations.push(obj);
                var title = markerItem.val().name;
                var icon = "http://localhost:1000/icons/pizza.png";
                labels.push(title);
                icons.push(icon);
            } else if(type == "Coffee Shop") {
                var obj = {};
                obj.lat = parseFloat(markerItem.val().latitude);
                obj.lng = parseFloat(markerItem.val().longitude);
                locations.push(obj);
                var title = markerItem.val().name;
                var icon = "http://localhost:1000/icons/coffee.png";
                labels.push(title);
                icons.push(icon);
            } else if(type == "Inn") {
                var obj = {};
                obj.lat = parseFloat(markerItem.val().latitude);
                obj.lng = parseFloat(markerItem.val().longitude);
                locations.push(obj);
                var title = markerItem.val().name;
                var icon = "http://localhost:1000/icons/inn.png";
                labels.push(title);
                icons.push(icon);
            } else if(type == "Hotel") {
                var obj = {};
                obj.lat = parseFloat(markerItem.val().latitude);
                obj.lng = parseFloat(markerItem.val().longitude);
                locations.push(obj);
                var title = markerItem.val().name;
                var icon = "http://localhost:1000/icons/hotel.png";
                labels.push(title);
                icons.push(icon);
            } else if(type == "Hostel") {
                var obj = {};
                obj.lat = parseFloat(markerItem.val().latitude);
                obj.lng = parseFloat(markerItem.val().longitude);
                locations.push(obj);
                var title = markerItem.val().name;
                var icon = "http://localhost:1000/icons/hostel.png";
                labels.push(title);
                icons.push(icon);
            }
        }); 
        

        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 8.2280, lng: 124.2452},
            zoom: 10,
            styles: [
            {
                "featureType": "all",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "weight": "2.00"
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#9c9c9c"
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "landscape.man_made",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 45
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#eeeeee"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#7b7b7b"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#46bcec"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#c8d7d4"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#070707"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            }
        ]
        });
        // var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        var markers = locations.map(function(location, i) {
            return new google.maps.Marker({
            position: location,
            title: labels[i],
            icon: icons[i],
            });
        });


        
        var markerIcon = {
            Restaurant:{
            icon: "http://localhost:1000/icons/restaurant.png"
            }
        }
        var markerCluster = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});

        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29),
            draggable: true
        }); 
    });

}


function map() {
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -33.8688, lng: 151.2195},
    zoom: 13
  });
  var input = document.getElementById('pac-input');


  var autocomplete = new google.maps.places.Autocomplete(input);
  autocomplete.bindTo('bounds', map);

  var infowindow = new google.maps.InfoWindow();
  var infowindowContent = document.getElementById('infowindow-content');
  infowindow.setContent(infowindowContent);

  var marker = new google.maps.Marker({
    map: map,
    anchorPoint: new google.maps.Point(0, -29),
    draggable: true
  }); 

  google.maps.event.addListener(marker, 'dragend', function(){
    var place = autocomplete.getPlace();


    console.log(marker.getPosition().lat() + ", "+marker.getPosition().lng());
    console.log();

    document.getElementById('address').value = place.formatted_address;
    document.getElementById('latitude').value = marker.getPosition().lat();
    document.getElementById('longitude').value = marker.getPosition().lng();

  });


  autocomplete.addListener('place_changed', function() {
    infowindow.close();
    marker.setVisible(false);
    var place = autocomplete.getPlace();
    if (!place.geometry) {
      window.alert("No details available for input: '" + place.name + "'");
      return;
    }

    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(35);  // Why 17? Because it looks good.
    }
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);


    var address = '';
    if (place.address_components) {
      address = [
        (place.address_components[0] && place.address_components[0].short_name || ''),
        (place.address_components[1] && place.address_components[1].short_name || ''),
        (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
    }

    // infowindowContent.children['place-icon'].src = place.icon;
    // infowindowContent.children['place-name'].textContent = place.name;
    // infowindowContent.children['place-address'].textContent = address;
    // infowindow.open(map, marker);



    document.getElementById('latitude').value = place.geometry.location.lat();
    document.getElementById('longitude').value = place.geometry.location.lng();

    var len = place.address_components.length;
        document.getElementById('address').value = place.formatted_address;
    if(len == 5){

        document.getElementById('country').value = place.address_components[(len-1)].long_name;
        document.getElementById('city').value = place.address_components[1].long_name;
        document.getElementById('region').value = place.address_components[(len-2)].long_name;

    } else if(len == 4){
        document.getElementById('country').value = place.address_components[(len-1)].long_name;
        document.getElementById('city').value = place.address_components[0].long_name;
        document.getElementById('region').value = place.address_components[(len-2)].long_name;
    } else if(len == 6){

        document.getElementById('country').value = place.address_components[(len-2)].long_name;
        document.getElementById('city').value = place.address_components[1].long_name;
        document.getElementById('region').value = place.address_components[(len-3)].long_name;

    }
    input.value = "";
  });

}




